using B1.Data;
using B1.Data.RePay;
using B1.Data.Services;
using Blazored.LocalStorage;
//using DevExpress.Blazor;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace B1
{
    public class Startup
    {
        public static IConfiguration zConfiguration;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Startup.zConfiguration = Configuration;

        }

    public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();

            services.AddDbContext<LeadDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnectionPR"), sqlServerOptions => sqlServerOptions.CommandTimeout(20)),
                ServiceLifetime.Scoped);

            services.AddDbContext<RePayContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("RePayConnectionPR")),
                ServiceLifetime.Scoped);

            //services.AddDbContext<RePayContext>(
            //    options => options.UseSqlServer(Configuration.GetConnectionString("OLAConnectionDEV")),
            //    ServiceLifetime.Scoped);

            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();
            
            services.AddSingleton<LoanDataServiceCache>();
            services.AddScoped<LoanDataService>();

            services.AddSingleton<PaymentDataServiceCache>();
            services.AddScoped<PaymentDataService>();

            services.AddAuthentication(IISDefaults.AuthenticationScheme);
            services.AddDevExpressBlazor();

            services.AddBlazoredLocalStorage();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
