﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Quote
    {
        public Quote()
        {
            BillOfSale = new HashSet<BillOfSale>();
            Contact = new HashSet<Contact>();
            Income = new HashSet<Income>();
            IncomeAndExpenditure = new HashSet<IncomeAndExpenditure>();
            QuoteAddress = new HashSet<QuoteAddress>();
            QuoteAppointment = new HashSet<QuoteAppointment>();
            QuoteAuthorisation = new HashSet<QuoteAuthorisation>();
            QuoteDataConfirmation = new HashSet<QuoteDataConfirmation>();
            QuoteDiaryEntry = new HashSet<QuoteDiaryEntry>();
            QuoteFunding = new HashSet<QuoteFunding>();
            QuoteHistory = new HashSet<QuoteHistory>();
            QuoteLink = new HashSet<QuoteLink>();
            QuoteScore = new HashSet<QuoteScore>();
        }

        public long Id { get; set; }
        public long Number { get; set; }
        public int State { get; set; }
        public long? LeadId { get; set; }
        public long? LoanSystemLoanId { get; set; }
        public string CustomerTitle { get; set; }
        public string CustomerName { get; set; }
        public string CustomerSurname { get; set; }
        public DateTime? CustomerDateOfBirth { get; set; }
        public string CustomerAddressLine1 { get; set; }
        public string CustomerAddressLine2 { get; set; }
        public string CustomerAddressLine3 { get; set; }
        public string CustomerTown { get; set; }
        public string CustomerCounty { get; set; }
        public string CustomerPostcode { get; set; }
        public string CustomerHomeownerStatus { get; set; }
        public string CustomerOccupation { get; set; }
        public string VehicleRegNo { get; set; }
        public int? VehicleMileage { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleColour { get; set; }
        public string VehicleChassisNumber { get; set; }
        public int VehicleSpareKeyStatus { get; set; }
        public string VehicleRegistered { get; set; }
        public string VehicleV5documentReference { get; set; }
        public DateTime? VehicleV5issueDate { get; set; }
        public DateTime? VehicleMotexpiryDate { get; set; }
        public DateTime? VehicleTaxExpiryDate { get; set; }
        public long? ArrangingUnderwriterId { get; set; }
        public long? SigningUnderwriterId { get; set; }
        public long? FranchiseId { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? LoanDurationInWeeks { get; set; }
        public decimal? LoanWeeklyPayment { get; set; }
        public int LoanPaymentMethod { get; set; }
        public DateTime? LoanSignedDate { get; set; }
        public string SigningLocation { get; set; }
        public DateTime? SigningDate { get; set; }
        public int CustomerEmploymentStatus { get; set; }
        public string VehicleSpec { get; set; }
        public string SigningMeetingStart { get; set; }
        public string SigningMeetingFinish { get; set; }
        public bool? VehicleLogBookInCustomerName { get; set; }
        public bool? CustomerMarketingOptOut { get; set; }
        public long LoanProductId { get; set; }
        public DateTime? LoanFirstPayment { get; set; }
        public decimal? LoanMonthlyRate { get; set; }
        public int VehicleType { get; set; }
        public int? VehicleMileageOnMot { get; set; }
        public string VehicleMottestNumber { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int BookingMeetingTimeslot { get; set; }
        public string AdviceToAdm { get; set; }
        public int InvitationReceivedMethod { get; set; }
        public string InvitationReceivedFrom { get; set; }
        public string InvitationReceivedComment { get; set; }
        public int AppointmentFailOutcome { get; set; }
        public string AppointmentFailComment { get; set; }
        public string AppointmentFailReason { get; set; }
        public DateTime? PrintingResetAt { get; set; }
        public DateTime? LastWorkedAt { get; set; }
        public DateTime? ContactAgainAt { get; set; }
        public int LoanRepaymentPeriod { get; set; }
        public string ElmsloanAgrNo { get; set; }
        public string Customer2Title { get; set; }
        public string Customer2Name { get; set; }
        public string Customer2Surname { get; set; }
        public DateTime? Customer2DateOfBirth { get; set; }
        public string Customer2HomeownerStatus { get; set; }
        public string Customer2Occupation { get; set; }
        public int Customer2EmploymentStatus { get; set; }
        public bool CustomerDualSignatories { get; set; }
        public int? CustomerCreditScore { get; set; }
        public int? Customer2CreditScore { get; set; }
        public DateTime? CustomerMovedToCurrentAddressAt { get; set; }
        public DateTime? CustomerStartedEmploymentAt { get; set; }
        public int? HouseholdNumberOfAdults { get; set; }
        public int? HouseholdNumberOfChildren { get; set; }
        public DateTime? Customer2MovedToCurrentAddressAt { get; set; }
        public DateTime? Customer2StartedEmploymentAt { get; set; }
        public string GeodemographicsAreaAffluence { get; set; }
        public string GeodemographicsAreaLifestage { get; set; }
        public decimal? CustomerIncomeInclBenefits { get; set; }
        public decimal? NbtfundsRequested { get; set; }
        public bool? NbthomeownerStatus { get; set; }
        public int NbtemployedStatus { get; set; }
        public int? NbttimeAtAddress { get; set; }
        public int? NbttimeInEmployment { get; set; }
        public bool? NbtownCar { get; set; }
        public bool? NbtivabankruptTrustDeed { get; set; }
        public bool? NbtbankAccountDebitCard { get; set; }
        public bool? NbtfundsForSettingUpCpa { get; set; }
        public int NbtnumberOfPaydayLoans { get; set; }
        public DateTime? ScoringSalesResetAt { get; set; }
        public DateTime? ScoringUnderwritingResetAt { get; set; }
        public string CustomerGender { get; set; }
        public string Customer2Gender { get; set; }
        public string CustomerNinumber { get; set; }
        public string Customer2Ninumber { get; set; }
        public string CustomerEmployer { get; set; }
        public string Customer2Employer { get; set; }
        public long? VehicleInsurerId { get; set; }
        public string VehicleInsurancePolicyNumber { get; set; }
        public string BankSortCode { get; set; }
        public string BankAccountNumber { get; set; }
        public string CustomerPayFrequency { get; set; }
        public DateTime? CustomerNextPayDay { get; set; }
        public DateTime? CustomerSubsequentPayDay { get; set; }
        public string Customer2PayFrequency { get; set; }
        public DateTime? Customer2NextPayDay { get; set; }
        public DateTime? Customer2SubsequentPayDay { get; set; }
        public string CustomerNameClean { get; set; }
        public string CustomerFullNameClean { get; set; }

        public virtual Underwriter ArrangingUnderwriter { get; set; }
        public virtual Franchise Franchise { get; set; }
        public virtual Underwriter SigningUnderwriter { get; set; }
        public virtual Insurer VehicleInsurer { get; set; }
        public virtual ICollection<BillOfSale> BillOfSale { get; set; }
        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<Income> Income { get; set; }
        public virtual ICollection<IncomeAndExpenditure> IncomeAndExpenditure { get; set; }
        public virtual ICollection<QuoteAddress> QuoteAddress { get; set; }
        public virtual ICollection<QuoteAppointment> QuoteAppointment { get; set; }
        public virtual ICollection<QuoteAuthorisation> QuoteAuthorisation { get; set; }
        public virtual ICollection<QuoteDataConfirmation> QuoteDataConfirmation { get; set; }
        public virtual ICollection<QuoteDiaryEntry> QuoteDiaryEntry { get; set; }
        public virtual ICollection<QuoteFunding> QuoteFunding { get; set; }
        public virtual ICollection<QuoteHistory> QuoteHistory { get; set; }
        public virtual ICollection<QuoteLink> QuoteLink { get; set; }
        public virtual ICollection<QuoteScore> QuoteScore { get; set; }
    }
}
