﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class PayOut
    {
        public long Id { get; set; }
        public string Number { get; set; }
        public decimal Amount { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
    }
}
