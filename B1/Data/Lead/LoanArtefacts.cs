﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanArtefacts
    {
        public long Id { get; set; }
        public int LoanId { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
