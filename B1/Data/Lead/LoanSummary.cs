﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanSummary
    {
        public int LoanId { get; set; }
        public int? AgrNo { get; set; }
        public int? LoanState { get; set; }
        public int? RepossessedStatus { get; set; }
        public int CarId { get; set; }
        public DateTime? DateSigned { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal FinalPaymentAmount { get; set; }
        public DateTime FinalPaymentDate { get; set; }
        public int FullWeeks { get; set; }
        public decimal WeeklyAmount { get; set; }
        public decimal? TotalPayments { get; set; }
        public double? TotalActions { get; set; }
        public double? TotalRepo { get; set; }
        public double? TotalLegal { get; set; }
        public decimal? TotalPaymentsPartSettl { get; set; }
        public decimal? TotalInterestWrittenOff { get; set; }
    }
}
