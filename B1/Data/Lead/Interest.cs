﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Interest
    {
        public int InterestId { get; set; }
        public int LoanId { get; set; }
        public DateTime? InterestDate { get; set; }
        public decimal? InterestAmount { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public string Reference { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
