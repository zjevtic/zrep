﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class InterestRate
    {
        public long Id { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal Rate { get; set; }
        public long LoanProductId { get; set; }
    }
}
