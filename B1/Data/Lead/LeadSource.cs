﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LeadSource
    {
        public long Id { get; set; }
        public long LeadId { get; set; }
        public int Index { get; set; }
        public string Type { get; set; }
        public long? Code { get; set; }
        public string Name { get; set; }

        public virtual Lead Lead { get; set; }
    }
}
