﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class User
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsLoanAuth { get; set; }
        public bool IsCarCheckOverride { get; set; }
        public bool IsAuthCommDecision { get; set; }
        public bool IsAuthRiskDecision { get; set; }
        public bool IsSalesSupport { get; set; }
        public bool IsComplianceOfficer { get; set; }
        public bool IsBookingTeam { get; set; }
        public bool IsUwteamManager { get; set; }
        public bool IsLeadStatAllowed { get; set; }
        public bool IsTswrittenOff { get; set; }
        public bool IsTsnoEntry { get; set; }
        public bool IsTswarning { get; set; }
        public bool IsTsstop { get; set; }
        public bool IsManager { get; set; }
        public bool IsLegal { get; set; }
        public bool IsFastPay { get; set; }
        public string FullName { get; set; }
        public bool IsAllowedToStartQuoteDiffDate { get; set; }
        public bool IsAllowedToWo { get; set; }
        public bool IsReports { get; set; }
        public bool IsOlareports { get; set; }
        public bool IsQa { get; set; }
        public bool IsItteam { get; set; }
        public bool IsSystem { get; set; }
    }
}
