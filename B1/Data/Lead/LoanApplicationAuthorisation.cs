﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationAuthorisation
    {
        public LoanApplicationAuthorisation()
        {
            LoanApplicationAuthorisationNote = new HashSet<LoanApplicationAuthorisationNote>();
        }

        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public string Reason { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOnBehalf { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
        public virtual ICollection<LoanApplicationAuthorisationNote> LoanApplicationAuthorisationNote { get; set; }
    }
}
