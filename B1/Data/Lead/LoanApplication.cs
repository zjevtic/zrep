﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplication
    {
        public LoanApplication()
        {
            LoanApplicantAddress = new HashSet<LoanApplicantAddress>();
            LoanApplicationAssessment = new HashSet<LoanApplicationAssessment>();
            LoanApplicationAuthorisation = new HashSet<LoanApplicationAuthorisation>();
            LoanApplicationCheck = new HashSet<LoanApplicationCheck>();
            LoanApplicationCheckAlt = new HashSet<LoanApplicationCheckAlt>();
            LoanApplicationHistory = new HashSet<LoanApplicationHistory>();
            LoanApplicationIe = new HashSet<LoanApplicationIe>();
            LoanApplicationIncome = new HashSet<LoanApplicationIncome>();
            LoanApplicationLink = new HashSet<LoanApplicationLink>();
            LoanApplicationMiscDetail = new HashSet<LoanApplicationMiscDetail>();
            LoanApplicationPredictor = new HashSet<LoanApplicationPredictor>();
            LoanApplicationSource = new HashSet<LoanApplicationSource>();
            LoanApplicationZva = new HashSet<LoanApplicationZva>();
        }

        public long Id { get; set; }
        public string Number { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleNames { get; set; }
        public string LastName { get; set; }
        public DateTime? Dob { get; set; }
        public string Ninumber { get; set; }
        public string MaritalStatus { get; set; }
        public int? NoOfDependents { get; set; }
        public string ReasonForLoan { get; set; }
        public bool? AllowMarketing { get; set; }
        public int State { get; set; }
        public string BankSortCode { get; set; }
        public string BankAccountNumber { get; set; }
        public bool? VehicleAvailable { get; set; }
        public string LoanAgrNo { get; set; }
        public string BankVisionSearchId { get; set; }
        public long? ArrangementId { get; set; }
        public int BankVisionRequirement { get; set; }
        public string EquifaxSearchId { get; set; }
        public decimal? RiskScore { get; set; }
        public decimal? MaxLoanAmount { get; set; }
        public int? LoanDurationInWeeks { get; set; }
        public int LoanRepaymentPeriod { get; set; }
        public DateTime? LoanFirstPayment { get; set; }
        public string LoanPaperworkSignature { get; set; }
        public decimal? LoanAmount { get; set; }
        public DateTime? DateSigned { get; set; }
        public string BankAccountName { get; set; }
        public int BankVisionStatus { get; set; }
        public DateTime CreatedAt { get; set; }
        public string GeodemographicsAreaAffluence { get; set; }
        public string GeodemographicsAreaLifestage { get; set; }
        public decimal? Woscore { get; set; }
        public decimal? Plscore { get; set; }
        public decimal? WoPlScore { get; set; }
        public decimal LoanMonthlyRate { get; set; }
        public decimal? AmountRequired { get; set; }
        public bool? RegisteredForOnlineBanking { get; set; }
        public decimal? AdpmaxLoanAmount { get; set; }
        public long? CreditTeamId { get; set; }
        public DateTime? AssignedToCreditTeamAt { get; set; }
        public string Mobile { get; set; }
        public string Landline { get; set; }
        public string WorkPhone { get; set; }
        public string Email { get; set; }

        public virtual CreditTeam CreditTeam { get; set; }
        public virtual ICollection<LoanApplicantAddress> LoanApplicantAddress { get; set; }
        public virtual ICollection<LoanApplicationAssessment> LoanApplicationAssessment { get; set; }
        public virtual ICollection<LoanApplicationAuthorisation> LoanApplicationAuthorisation { get; set; }
        public virtual ICollection<LoanApplicationCheck> LoanApplicationCheck { get; set; }
        public virtual ICollection<LoanApplicationCheckAlt> LoanApplicationCheckAlt { get; set; }
        public virtual ICollection<LoanApplicationHistory> LoanApplicationHistory { get; set; }
        public virtual ICollection<LoanApplicationIe> LoanApplicationIe { get; set; }
        public virtual ICollection<LoanApplicationIncome> LoanApplicationIncome { get; set; }
        public virtual ICollection<LoanApplicationLink> LoanApplicationLink { get; set; }
        public virtual ICollection<LoanApplicationMiscDetail> LoanApplicationMiscDetail { get; set; }
        public virtual ICollection<LoanApplicationPredictor> LoanApplicationPredictor { get; set; }
        public virtual ICollection<LoanApplicationSource> LoanApplicationSource { get; set; }
        public virtual ICollection<LoanApplicationZva> LoanApplicationZva { get; set; }
    }
}
