﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Payments
    {
        public Payments()
        {
            InverseCardPayment = new HashSet<Payments>();
        }

        public int PaymentId { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string ReceiptNo { get; set; }
        public int LoanId { get; set; }
        public decimal? Amount { get; set; }
        public int? CardPaymentId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public decimal AllocatedToSettlement { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public int PaymentMethod { get; set; }

        public virtual Payments CardPayment { get; set; }
        public virtual LoanDetails Loan { get; set; }
        public virtual ICollection<Payments> InverseCardPayment { get; set; }
    }
}
