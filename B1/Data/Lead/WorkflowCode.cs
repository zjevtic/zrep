﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class WorkflowCode
    {
        public long Id { get; set; }
        public long WorkflowId { get; set; }
        public string MethodName { get; set; }
        public string Condition { get; set; }
        public string Action { get; set; }

        public virtual WorkflowVersion Workflow { get; set; }
    }
}
