﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Matrix
    {
        public long Id { get; set; }
        public long MatrixTypeId { get; set; }
        public string PostcodePrefix { get; set; }
        public long UnderwriterId { get; set; }

        public virtual MatrixType MatrixType { get; set; }
        public virtual Underwriter Underwriter { get; set; }
    }
}
