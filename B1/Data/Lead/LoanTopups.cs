﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanTopups
    {
        public int Id { get; set; }
        public int OriginalLoanId { get; set; }
        public int TopupLoanId { get; set; }
        public decimal SettlementAmount { get; set; }

        public virtual LoanDetails OriginalLoan { get; set; }
        public virtual LoanDetails TopupLoan { get; set; }
    }
}
