﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class CustomList
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public bool Archived { get; set; }
    }
}
