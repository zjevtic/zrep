﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class TmpAcc
    {
        public int? AgrNo { get; set; }
        public string Status { get; set; }
    }
}
