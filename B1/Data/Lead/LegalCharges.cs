﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LegalCharges
    {
        public int LchargeId { get; set; }
        public int? LoanId { get; set; }
        public double? Amount { get; set; }
        public DateTime? Date { get; set; }
        public string Comments { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
