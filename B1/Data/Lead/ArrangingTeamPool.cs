﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class ArrangingTeamPool
    {
        public long Id { get; set; }
        public long UnderwriterId { get; set; }
        public bool IsOnline { get; set; }

        public virtual Underwriter Underwriter { get; set; }
    }
}
