﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanFundings
    {
        public long Id { get; set; }
        public int LoanId { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public DateTime FundedAt { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
