﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace B1.Data
{
    public partial class LeadDbContext : DbContext
    {
        public LeadDbContext()
        {
            //this.Database.ExecuteSqlRaw("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
            this.Database.ExecuteSqlRaw("SET TRANSACTION ISOLATION LEVEL READ COMMITTED");
        }
        
        public LeadDbContext(DbContextOptions<LeadDbContext> options)
            : base(options)
        {
            this.Database.ExecuteSqlRaw("SET TRANSACTION ISOLATION LEVEL READ COMMITTED");
        }

        public virtual DbSet<AccountNumber> AccountNumber { get; set; }
        public virtual DbSet<ActionDiary> ActionDiary { get; set; }
        public virtual DbSet<Actions> Actions { get; set; }
        public virtual DbSet<AdditionalAdSource> AdditionalAdSource { get; set; }
        public virtual DbSet<AllActions> AllActions { get; set; }
        public virtual DbSet<ArrangingTeamPool> ArrangingTeamPool { get; set; }
        public virtual DbSet<Attachment> Attachment { get; set; }
        public virtual DbSet<BillOfSale> BillOfSale { get; set; }
        public virtual DbSet<Campaign> Campaign { get; set; }
        public virtual DbSet<CarCheck> CarCheck { get; set; }
        public virtual DbSet<Cars> Cars { get; set; }
        public virtual DbSet<CompanyDetail> CompanyDetail { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<Contacts> Contacts { get; set; }
        public virtual DbSet<CpwcfplusLog> CpwcfplusLog { get; set; }
        public virtual DbSet<CreditTeam> CreditTeam { get; set; }
        public virtual DbSet<CustomList> CustomList { get; set; }
        public virtual DbSet<CustomerCheck> CustomerCheck { get; set; }
        public virtual DbSet<CustomerCheckNote> CustomerCheckNote { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<DebtBuyers> DebtBuyers { get; set; }
        public virtual DbSet<DoNotUseOla> DoNotUseOla { get; set; }
        public virtual DbSet<DtaMv0> DtaMv0 { get; set; }
        public virtual DbSet<EntityUpdatedColumnLog> EntityUpdatedColumnLog { get; set; }
        public virtual DbSet<FileRegistration> FileRegistration { get; set; }
        public virtual DbSet<FileRegistrationNote> FileRegistrationNote { get; set; }
        public virtual DbSet<Franchise> Franchise { get; set; }
        public virtual DbSet<Gdprconsent> Gdprconsent { get; set; }
        public virtual DbSet<Income> Income { get; set; }
        public virtual DbSet<IncomeAndExpenditure> IncomeAndExpenditure { get; set; }
        public virtual DbSet<Insurer> Insurer { get; set; }
        public virtual DbSet<Interest> Interest { get; set; }
        public virtual DbSet<InterestRate> InterestRate { get; set; }
        public virtual DbSet<Lead> Lead { get; set; }
        public virtual DbSet<LeadDistribution> LeadDistribution { get; set; }
        public virtual DbSet<LeadSource> LeadSource { get; set; }
        public virtual DbSet<LeadUpdate> LeadUpdate { get; set; }
        public virtual DbSet<LegalCharges> LegalCharges { get; set; }
        public virtual DbSet<LetterQueue> LetterQueue { get; set; }
        public virtual DbSet<LoanApplicantAddress> LoanApplicantAddress { get; set; }
        public virtual DbSet<LoanApplication> LoanApplication { get; set; }
        public virtual DbSet<LoanApplicationAssessment> LoanApplicationAssessment { get; set; }
        public virtual DbSet<LoanApplicationAuthorisation> LoanApplicationAuthorisation { get; set; }
        public virtual DbSet<LoanApplicationAuthorisationNote> LoanApplicationAuthorisationNote { get; set; }
        public virtual DbSet<LoanApplicationCheck> LoanApplicationCheck { get; set; }
        public virtual DbSet<LoanApplicationCheckAlt> LoanApplicationCheckAlt { get; set; }
        public virtual DbSet<LoanApplicationHistory> LoanApplicationHistory { get; set; }
        public virtual DbSet<LoanApplicationIe> LoanApplicationIe { get; set; }
        public virtual DbSet<LoanApplicationIncome> LoanApplicationIncome { get; set; }
        public virtual DbSet<LoanApplicationLink> LoanApplicationLink { get; set; }
        public virtual DbSet<LoanApplicationMiscDetail> LoanApplicationMiscDetail { get; set; }
        public virtual DbSet<LoanApplicationPredictor> LoanApplicationPredictor { get; set; }
        public virtual DbSet<LoanApplicationSource> LoanApplicationSource { get; set; }
        public virtual DbSet<LoanApplicationZva> LoanApplicationZva { get; set; }
        public virtual DbSet<LoanArtefactLogs> LoanArtefactLogs { get; set; }
        public virtual DbSet<LoanArtefacts> LoanArtefacts { get; set; }
        public virtual DbSet<LoanContactTime> LoanContactTime { get; set; }
        public virtual DbSet<LoanDetails> LoanDetails { get; set; }
        public virtual DbSet<LoanFinancialPlan> LoanFinancialPlan { get; set; }
        public virtual DbSet<LoanFundings> LoanFundings { get; set; }
        public virtual DbSet<LoanGuarantor> LoanGuarantor { get; set; }
        public virtual DbSet<LoanIncomes> LoanIncomes { get; set; }
        public virtual DbSet<LoanProduct> LoanProduct { get; set; }
        public virtual DbSet<LoanSale> LoanSale { get; set; }
        public virtual DbSet<LoanSecondSignatory> LoanSecondSignatory { get; set; }
        public virtual DbSet<LoanStateChanges> LoanStateChanges { get; set; }
        public virtual DbSet<LoanStatusChanges> LoanStatusChanges { get; set; }
        public virtual DbSet<LoanSummary> LoanSummary { get; set; }
        public virtual DbSet<LoanTaggingActions> LoanTaggingActions { get; set; }
        public virtual DbSet<LoanTerm> LoanTerm { get; set; }
        public virtual DbSet<LoanTopups> LoanTopups { get; set; }
        public virtual DbSet<MarketingCampaign> MarketingCampaign { get; set; }
        public virtual DbSet<Matrix> Matrix { get; set; }
        public virtual DbSet<MatrixType> MatrixType { get; set; }
        public virtual DbSet<MatrixUsage> MatrixUsage { get; set; }
        public virtual DbSet<Nbtstat> Nbtstat { get; set; }
        public virtual DbSet<Notes> Notes { get; set; }
        public virtual DbSet<OlawcfplusLog> OlawcfplusLog { get; set; }
        public virtual DbSet<OutstandingPdfchange> OutstandingPdfchange { get; set; }
        public virtual DbSet<PartialEarlySettlements> PartialEarlySettlements { get; set; }
        public virtual DbSet<PayOut> PayOut { get; set; }
        public virtual DbSet<PaymentImportLoan> PaymentImportLoan { get; set; }
        public virtual DbSet<PaymentInputTemp> PaymentInputTemp { get; set; }
        public virtual DbSet<PaymentPlans> PaymentPlans { get; set; }
        public virtual DbSet<PaymentPromises> PaymentPromises { get; set; }
        public virtual DbSet<Payments> Payments { get; set; }
        public virtual DbSet<PostcodePrefix> PostcodePrefix { get; set; }
        public virtual DbSet<Quote> Quote { get; set; }
        public virtual DbSet<QuoteAddress> QuoteAddress { get; set; }
        public virtual DbSet<QuoteAppointment> QuoteAppointment { get; set; }
        public virtual DbSet<QuoteAuthorisation> QuoteAuthorisation { get; set; }
        public virtual DbSet<QuoteAuthorisationNote> QuoteAuthorisationNote { get; set; }
        public virtual DbSet<QuoteDataConfirmation> QuoteDataConfirmation { get; set; }
        public virtual DbSet<QuoteDiaryEntry> QuoteDiaryEntry { get; set; }
        public virtual DbSet<QuoteFunding> QuoteFunding { get; set; }
        public virtual DbSet<QuoteHistory> QuoteHistory { get; set; }
        public virtual DbSet<QuoteLink> QuoteLink { get; set; }
        public virtual DbSet<QuoteScore> QuoteScore { get; set; }
        public virtual DbSet<RePayFirstCpaattempt> RePayFirstCpaattempt { get; set; }
        public virtual DbSet<RePayFirstCpaattemptFailed> RePayFirstCpaattemptFailed { get; set; }
        public virtual DbSet<RePayPayment> RePayPayment { get; set; }
        public virtual DbSet<RegionalManager> RegionalManager { get; set; }
        public virtual DbSet<RepossessionCosts> RepossessionCosts { get; set; }
        public virtual DbSet<Setting> Setting { get; set; }
        public virtual DbSet<TemporaryUnderwriter> TemporaryUnderwriter { get; set; }
        public virtual DbSet<Tmp> Tmp { get; set; }
        public virtual DbSet<TmpAcc> TmpAcc { get; set; }
        public virtual DbSet<TmpCallQueue> TmpCallQueue { get; set; }
        public virtual DbSet<TmpEntityUpdatedColumnLog> TmpEntityUpdatedColumnLog { get; set; }
        public virtual DbSet<TmpGdprImport> TmpGdprImport { get; set; }
        public virtual DbSet<TmpGdprImport2> TmpGdprImport2 { get; set; }
        public virtual DbSet<TmpGdprconsent> TmpGdprconsent { get; set; }
        public virtual DbSet<TmpLead> TmpLead { get; set; }
        public virtual DbSet<TmpLetter> TmpLetter { get; set; }
        public virtual DbSet<TmpLoanCust> TmpLoanCust { get; set; }
        public virtual DbSet<TmpPtp> TmpPtp { get; set; }
        public virtual DbSet<TmpPurgeLeads> TmpPurgeLeads { get; set; }
        public virtual DbSet<TmpPurgeLoans> TmpPurgeLoans { get; set; }
        public virtual DbSet<TmpPurgeOla> TmpPurgeOla { get; set; }
        public virtual DbSet<TmpPurgeQuotes> TmpPurgeQuotes { get; set; }
        public virtual DbSet<TmpTbl> TmpTbl { get; set; }
        public virtual DbSet<Underwriter> Underwriter { get; set; }
        public virtual DbSet<UnderwriterChequeBook> UnderwriterChequeBook { get; set; }
        public virtual DbSet<UnderwriterLoanProduct> UnderwriterLoanProduct { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<VeMaSonHold> VeMaSonHold { get; set; }
        public virtual DbSet<VehicleColour> VehicleColour { get; set; }
        public virtual DbSet<WcfplusLog> WcfplusLog { get; set; }
        public virtual DbSet<Withdrawals> Withdrawals { get; set; }
        public virtual DbSet<WorkflowCode> WorkflowCode { get; set; }
        public virtual DbSet<WorkflowError> WorkflowError { get; set; }
        public virtual DbSet<WorkflowVersion> WorkflowVersion { get; set; }
        public virtual DbSet<Xmlaffiliate> Xmlaffiliate { get; set; }
        public virtual DbSet<Xmlapplication> Xmlapplication { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //                optionsBuilder.UseSqlServer("Server=lon-testdb02.logbookloans.local,1433;Initial Catalog=StagingLead;Persist Security Info=False;User ID=Zoran;Password=*fX9fY9D5xdUJMega");

                
                optionsBuilder.UseSqlServer(Startup.zConfiguration.GetConnectionString("DefaultConnectionPR"), sqlServerOptions => sqlServerOptions.CommandTimeout(20));
            }
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountNumber>(entity =>
            {
                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<ActionDiary>(entity =>
            {
                entity.HasIndex(e => e.LoanId)
                    .HasName("NCIX_ActionDiary_LoanId");

                entity.Property(e => e.ActionDiaryId).HasColumnName("ActionDiaryID");

                entity.Property(e => e.ActionDiaryDate).HasColumnType("datetime");

                entity.Property(e => e.ActionDiaryText)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.CompletedBy).HasMaxLength(4000);

                entity.Property(e => e.CompletedComment).HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.ActionDiary)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_ActionDiary");
            });

            modelBuilder.Entity<Actions>(entity =>
            {
                entity.HasKey(e => e.ActionId)
                    .HasName("PK_dbo.Actions");

                entity.HasIndex(e => e.LoanId)
                    .HasName("IX_Actions");

                entity.Property(e => e.ActionId).HasColumnName("ActionID");

                entity.Property(e => e.Action).HasMaxLength(255);

                entity.Property(e => e.ActionDate).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.Actions)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_Action");
            });

            modelBuilder.Entity<AdditionalAdSource>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<AllActions>(entity =>
            {
                entity.HasKey(e => e.Action)
                    .HasName("PK_dbo.[All Actions]");

                entity.ToTable("All Actions");

                entity.Property(e => e.Action).HasMaxLength(255);
            });

            modelBuilder.Entity<ArrangingTeamPool>(entity =>
            {
                entity.HasOne(d => d.Underwriter)
                    .WithMany(p => p.ArrangingTeamPool)
                    .HasForeignKey(d => d.UnderwriterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Underwriter_ArrangingTeamPool");
            });

            modelBuilder.Entity<Attachment>(entity =>
            {
                entity.Property(e => e.AttachedAt).HasColumnType("datetime");

                entity.Property(e => e.AttachedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Loan)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<BillOfSale>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.ChequeIssuedAt).HasColumnType("datetime");

                entity.HasOne(d => d.ChequeUnderwriter)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.ChequeUnderwriterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Underwriter_BillOfSale");

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_BoS");
            });

            modelBuilder.Entity<Campaign>(entity =>
            {
                entity.HasIndex(e => e.LoanId)
                    .HasName("IXLoanID");

                entity.HasIndex(e => new { e.State, e.Name, e.Date })
                    .HasName("IxDate");

                entity.Property(e => e.AgreementNumber)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.AssignedAt).HasColumnType("datetime");

                entity.Property(e => e.Balance).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.CommitmentAmount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.CommitmentDate).HasColumnType("datetime");

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.CompletedBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.CustomerSurname)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DateSigned).HasColumnType("datetime");

                entity.Property(e => e.FirstInstallmentDate).HasColumnType("datetime");

                entity.Property(e => e.Installment).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.LastWorkedDate).HasColumnType("datetime");

                entity.Property(e => e.LatestNoteDate).HasColumnType("datetime");

                entity.Property(e => e.LatestPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.LatestSpokeToCustomerDate).HasColumnType("datetime");

                entity.Property(e => e.LatestStateChangeDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PaymentsMade).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.PmtCmtAmountDue).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.PmtCmtDueAt).HasColumnType("datetime");

                entity.Property(e => e.PmtCmtType)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.RegularArrears).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.TaskComment)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.TaskDueAt).HasColumnType("datetime");

                entity.Property(e => e.TaskGeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.TaskGeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.TotalCharges).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.TotalPayable).HasColumnType("decimal(29, 4)");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.Campaign)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_Campaign");
            });

            modelBuilder.Entity<CarCheck>(entity =>
            {
                entity.Property(e => e.ChassisNumber).HasMaxLength(4000);

                entity.Property(e => e.Colour).HasMaxLength(4000);

                entity.Property(e => e.DamageDiscount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Description).HasMaxLength(4000);

                entity.Property(e => e.Details).HasColumnType("ntext");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Make).HasMaxLength(4000);

                entity.Property(e => e.MileageOnMot).HasColumnName("MileageOnMOT");

                entity.Property(e => e.Model).HasMaxLength(4000);

                entity.Property(e => e.RegNo)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Registered).HasColumnType("datetime");

                entity.Property(e => e.Spec).HasMaxLength(4000);

                entity.Property(e => e.Value).HasColumnType("decimal(29, 4)");
            });

            modelBuilder.Entity<Cars>(entity =>
            {
                entity.HasKey(e => e.CarId)
                    .HasName("PK_dbo.Cars");

                entity.HasIndex(e => new { e.CarId, e.CustomerId, e.RegNo })
                    .HasName("IX_RegNo");

                entity.HasIndex(e => new { e.CarId, e.RegNo, e.CustomerId })
                    .HasName("IX_CustomerId");

                entity.Property(e => e.CarId).HasColumnName("CarID");

                entity.Property(e => e.Anpr)
                    .HasColumnName("ANPR")
                    .HasColumnType("datetime");

                entity.Property(e => e.ChassisNo)
                    .HasColumnName("Chassis no")
                    .HasMaxLength(50);

                entity.Property(e => e.Colour).HasMaxLength(50);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.FirstRegistered).HasColumnType("datetime");

                entity.Property(e => e.Make)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MileageOnMot).HasColumnName("MileageOnMOT");

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MotDate)
                    .HasColumnName("MOT date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MottestNumber)
                    .HasColumnName("MOTTestNumber")
                    .HasMaxLength(4000);

                entity.Property(e => e.PolicyNumber).HasMaxLength(50);

                entity.Property(e => e.RegNo)
                    .IsRequired()
                    .HasColumnName("Reg no")
                    .HasMaxLength(50);

                entity.Property(e => e.Specification).HasMaxLength(50);

                entity.Property(e => e.TaxExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.TradePrice)
                    .HasColumnName("Trade price")
                    .HasColumnType("money");

                entity.Property(e => e.V5docIssueDate)
                    .HasColumnName("V5DocIssueDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.V5docRefNo)
                    .HasColumnName("V5DocRefNo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V5inCustomerName).HasColumnName("V5InCustomerName");

                entity.Property(e => e.V5serialNumber)
                    .HasColumnName("V5SerialNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Customer_Car");
            });

            modelBuilder.Entity<CompanyDetail>(entity =>
            {
                entity.Property(e => e.ConditionFieldName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.ConditionFieldValue)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.FieldName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.HasIndex(e => e.QuoteId)
                    .HasName("IX_Contact");

                entity.HasIndex(e => new { e.Type, e.Value });

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Type).HasMaxLength(200);

                entity.Property(e => e.Value).HasMaxLength(200);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.Contact)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_Contact");
            });

            modelBuilder.Entity<Contacts>(entity =>
            {
                entity.HasIndex(e => e.LoanId)
                    .HasName("IX_Contacts");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_Contact");
            });

            modelBuilder.Entity<CpwcfplusLog>(entity =>
            {
                entity.ToTable("CPWCFPlusLog");

                entity.Property(e => e.CallDuration).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.CalledAt).HasColumnType("datetime");

                entity.Property(e => e.CalledBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.MethodName)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<CreditTeam>(entity =>
            {
                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Mobile)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<CustomList>(entity =>
            {
                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Subcategory)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<CustomerCheck>(entity =>
            {
                entity.Property(e => e.CustomerAddressLine1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerAddressLine2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerAddressLine3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerCounty)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerDateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.CustomerLandline)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerMiddleName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerPostcode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerSurname)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerTitle)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerTown)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Data).HasColumnType("ntext");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Message)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PreviousAddresses)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CustomerCheckNote>(entity =>
            {
                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Concern)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Details)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.CustomerCheck)
                    .WithMany(p => p.CustomerCheckNote)
                    .HasForeignKey(d => d.CustomerCheckId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CustomerCheck_CustomerCheckNote");
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .HasName("PK_dbo.Customers");

                entity.HasIndex(e => e.LastName)
                    .HasName("IX_CustomerLastName");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.AddressLine1).HasMaxLength(4000);

                entity.Property(e => e.AddressLine2).HasMaxLength(4000);

                entity.Property(e => e.AddressLine3).HasMaxLength(4000);

                entity.Property(e => e.County).HasMaxLength(50);

                entity.Property(e => e.DateMoved).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.FirstName).HasMaxLength(30);

                entity.Property(e => e.HomeOwner)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.Ninumber)
                    .HasColumnName("NINumber")
                    .HasMaxLength(20);

                entity.Property(e => e.PostalCode).HasMaxLength(20);

                entity.Property(e => e.Title).HasMaxLength(10);

                entity.Property(e => e.Town).HasMaxLength(50);
            });

            modelBuilder.Entity<DebtBuyers>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PhoneNumber).HasMaxLength(4000);
            });

            modelBuilder.Entity<DoNotUseOla>(entity =>
            {
                entity.ToTable("DoNotUseOLA");

                entity.HasIndex(e => e.Dob);

                entity.HasIndex(e => e.Email);

                entity.HasIndex(e => e.Mobile);

                entity.HasIndex(e => e.Name);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.Property(e => e.Mobile).HasMaxLength(200);

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<DtaMv0>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("_dta_mv_0");

                entity.Property(e => e.Col1).HasColumnName("_col_1");

                entity.Property(e => e.Col2).HasColumnName("_col_2");
            });

            modelBuilder.Entity<EntityUpdatedColumnLog>(entity =>
            {
                entity.Property(e => e.EntityColumn)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.EntityType)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.NewValue).HasMaxLength(4000);

                entity.Property(e => e.OldValue).HasMaxLength(4000);
            });

            modelBuilder.Entity<FileRegistration>(entity =>
            {
                entity.Property(e => e.BillOfSaleLateRegistrationReason).HasMaxLength(4000);

                entity.Property(e => e.BillOfSaleRegDate).HasColumnType("datetime");

                entity.Property(e => e.BillOfSaleRegNo).HasMaxLength(4000);

                entity.Property(e => e.DateReceivedInHeadOffice).HasColumnType("datetime");

                entity.Property(e => e.DateRegistrationCompleted).HasColumnType("datetime");

                entity.Property(e => e.OtherNotes).HasMaxLength(4000);

                entity.Property(e => e.PhotoIdavailable).HasColumnName("PhotoIDAvailable");

                entity.Property(e => e.Qamember)
                    .HasColumnName("QAMember")
                    .HasMaxLength(4000);

                entity.Property(e => e.SigningAgentName).HasMaxLength(4000);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.FileRegistration)
                    .HasForeignKey(d => d.LoanId)
                    .HasConstraintName("Loan_Detail_FileRegistration");
            });

            modelBuilder.Entity<FileRegistrationNote>(entity =>
            {
                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.Subcategory)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.FileRegistration)
                    .WithMany(p => p.FileRegistrationNote)
                    .HasForeignKey(d => d.FileRegistrationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FileRegistration_FileRegistrationNote");
            });

            modelBuilder.Entity<Franchise>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.CorporateName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanCriteria)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Gdprconsent>(entity =>
            {
                entity.ToTable("GDPRConsent");

                entity.Property(e => e.EntityType)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Income>(entity =>
            {
                entity.Property(e => e.Customer)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.EmployerAddress).HasMaxLength(4000);

                entity.Property(e => e.EmployerName).HasMaxLength(4000);

                entity.Property(e => e.EmployerPhone).HasMaxLength(4000);

                entity.Property(e => e.Frequency)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PayDate)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PayMethod)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.Income)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_Income");
            });

            modelBuilder.Entity<IncomeAndExpenditure>(entity =>
            {
                entity.HasIndex(e => e.QuoteId)
                    .HasName("IDX_IncomeAndExpenditure_QuoteId");

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.Group)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Item)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Value).HasColumnType("decimal(29, 4)");

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.IncomeAndExpenditure)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_IncomeAndExpenditure");
            });

            modelBuilder.Entity<Insurer>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Interest>(entity =>
            {
                entity.HasIndex(e => e.InterestDate)
                    .HasName("IX_Interest_1");

                entity.HasIndex(e => e.LoanId)
                    .HasName("IX_Interest");

                entity.Property(e => e.InterestId).HasColumnName("InterestID");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.InterestAmount).HasColumnType("money");

                entity.Property(e => e.InterestDate).HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.Interest)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_Interest");
            });

            modelBuilder.Entity<InterestRate>(entity =>
            {
                entity.Property(e => e.LoanAmount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Rate).HasColumnType("decimal(29, 4)");
            });

            modelBuilder.Entity<Lead>(entity =>
            {
                entity.HasIndex(e => e.ContactAgainAt)
                    .HasName("Ix_ContatAgainAt");

                entity.HasIndex(e => e.DateOfBirth)
                    .HasName("IX_Lead_Dob");

                entity.HasIndex(e => e.Email);

                entity.HasIndex(e => e.GeneratedAt);

                entity.HasIndex(e => e.Mobile);

                entity.HasIndex(e => new { e.AssignedAt, e.ArrangingUnderwriterId })
                    .HasName("Ix_Underwriter");

                entity.HasIndex(e => new { e.Id, e.MarketingOptOut, e.Name, e.Surname, e.GeneratedAt })
                    .HasName("ix_GeneratedAt_Includes");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.AddressLine1).HasMaxLength(4000);

                entity.Property(e => e.AddressLine2).HasMaxLength(4000);

                entity.Property(e => e.AddressLine3).HasMaxLength(4000);

                entity.Property(e => e.Advice).HasMaxLength(4000);

                entity.Property(e => e.AssignedAt).HasColumnType("datetime");

                entity.Property(e => e.ChassisNumber).HasMaxLength(4000);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Colour).HasMaxLength(4000);

                entity.Property(e => e.ContactAgainAt).HasColumnType("datetime");

                entity.Property(e => e.County).HasMaxLength(4000);

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.HomeownerStatus).HasMaxLength(4000);

                entity.Property(e => e.InitialApplicationAmount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.InsuranceType).HasMaxLength(4000);

                entity.Property(e => e.Landline).HasMaxLength(4000);

                entity.Property(e => e.Make).HasMaxLength(4000);

                entity.Property(e => e.MarketingLastSmssend)
                    .HasColumnName("MarketingLastSMSSend")
                    .HasColumnType("datetime");

                entity.Property(e => e.Mobile).HasMaxLength(200);

                entity.Property(e => e.Model).HasMaxLength(4000);

                entity.Property(e => e.Motd).HasColumnName("MOTd");

                entity.Property(e => e.Name).HasMaxLength(4000);

                entity.Property(e => e.Postcode).HasMaxLength(4000);

                entity.Property(e => e.RegNo).HasMaxLength(4000);

                entity.Property(e => e.Registered).HasMaxLength(4000);

                entity.Property(e => e.Surname).HasMaxLength(4000);

                entity.Property(e => e.Title).HasMaxLength(4000);

                entity.Property(e => e.Town).HasMaxLength(4000);

                entity.Property(e => e.V5inCustomerName).HasColumnName("V5InCustomerName");

                entity.Property(e => e.ValuationInfo).HasMaxLength(4000);

                entity.Property(e => e.Value).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.WorkPhone).HasMaxLength(4000);

                entity.HasOne(d => d.ArrangingUnderwriter)
                    .WithMany(p => p.LeadArrangingUnderwriter)
                    .HasForeignKey(d => d.ArrangingUnderwriterId)
                    .HasConstraintName("Underwriter_Lead");

                entity.HasOne(d => d.Franchise)
                    .WithMany(p => p.Lead)
                    .HasForeignKey(d => d.FranchiseId)
                    .HasConstraintName("Franchise_Lead");

                entity.HasOne(d => d.Insurer)
                    .WithMany(p => p.Lead)
                    .HasForeignKey(d => d.InsurerId)
                    .HasConstraintName("Insurer_Lead");

                entity.HasOne(d => d.SigningUnderwriter)
                    .WithMany(p => p.LeadSigningUnderwriter)
                    .HasForeignKey(d => d.SigningUnderwriterId)
                    .HasConstraintName("Underwriter_Lead1");
            });

            modelBuilder.Entity<LeadDistribution>(entity =>
            {
                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy).HasMaxLength(4000);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.LeadDistribution)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Lead_LeadDistribution");

                entity.HasOne(d => d.Underwriter)
                    .WithMany(p => p.LeadDistribution)
                    .HasForeignKey(d => d.UnderwriterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Underwriter_LeadDistribution");
            });

            modelBuilder.Entity<LeadSource>(entity =>
            {
                entity.HasIndex(e => new { e.LeadId, e.Index })
                    .HasName("Ix_lEADiDiNDEX");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.LeadSource)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Lead_LeadSource");
            });

            modelBuilder.Entity<LeadUpdate>(entity =>
            {
                entity.HasIndex(e => e.LeadId)
                    .HasName("IxLead_LeadUpdate");

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.ContactAgainAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanAmount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.RejectionReason).HasMaxLength(4000);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.LeadUpdate)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Lead_LeadUpdate");
            });

            modelBuilder.Entity<LegalCharges>(entity =>
            {
                entity.HasKey(e => e.LchargeId)
                    .HasName("PK_dbo.LegalCharges");

                entity.HasIndex(e => e.Date)
                    .HasName("IX_LegalCharges_1");

                entity.HasIndex(e => e.LoanId)
                    .HasName("IX_LegalCharges");

                entity.Property(e => e.LchargeId).HasColumnName("LChargeID");

                entity.Property(e => e.Comments).HasColumnType("ntext");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LegalCharges)
                    .HasForeignKey(d => d.LoanId)
                    .HasConstraintName("Loan_Detail_LegalCharge");
            });

            modelBuilder.Entity<LetterQueue>(entity =>
            {
                entity.HasKey(e => e.LetterId)
                    .HasName("PK_dbo.LetterQueue");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DocumentData)
                    .IsRequired()
                    .HasColumnType("image");

                entity.Property(e => e.DocumentReference).HasColumnType("image");

                entity.Property(e => e.DocumentType)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LetterRunDate).HasColumnType("datetime");

                entity.Property(e => e.RequestedBy)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LoanApplicantAddress>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.AddressLine1).HasMaxLength(4000);

                entity.Property(e => e.AddressLine2).HasMaxLength(4000);

                entity.Property(e => e.AddressLine3).HasMaxLength(4000);

                entity.Property(e => e.Country).HasMaxLength(4000);

                entity.Property(e => e.County).HasMaxLength(4000);

                entity.Property(e => e.Postcode).HasMaxLength(4000);

                entity.Property(e => e.Town).HasMaxLength(4000);

                entity.Property(e => e.Ukresident).HasColumnName("UKResident");

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicantAddress)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicantAddress");
            });

            modelBuilder.Entity<LoanApplication>(entity =>
            {
                entity.HasIndex(e => e.CreatedAt);

                entity.HasIndex(e => e.Dob);

                entity.HasIndex(e => e.Email);

                entity.HasIndex(e => e.Mobile)
                    .HasName("IX_Mobile");

                entity.HasIndex(e => new { e.CreatedAt, e.Number })
                    .HasName("NCIX_LoanApplication_Number");

                entity.HasIndex(e => new { e.CreatedAt, e.CreditTeamId, e.State })
                    .HasName("ix_CreditTeamId_State_Includes");

                entity.HasIndex(e => new { e.AllowMarketing, e.FirstName, e.Id, e.LastName, e.CreatedAt })
                    .HasName("ix_CreatedAt_Includes");

                entity.Property(e => e.AdpmaxLoanAmount)
                    .HasColumnName("ADPMaxLoanAmount")
                    .HasColumnType("decimal(29, 4)");

                entity.Property(e => e.AmountRequired).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.AssignedToCreditTeamAt).HasColumnType("datetime");

                entity.Property(e => e.BankAccountName).HasMaxLength(4000);

                entity.Property(e => e.BankAccountNumber).HasMaxLength(4000);

                entity.Property(e => e.BankSortCode).HasMaxLength(4000);

                entity.Property(e => e.BankVisionSearchId).HasMaxLength(4000);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DateSigned).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.EquifaxSearchId).HasMaxLength(4000);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeodemographicsAreaAffluence).HasMaxLength(4000);

                entity.Property(e => e.GeodemographicsAreaLifestage).HasMaxLength(4000);

                entity.Property(e => e.Landline)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanAgrNo).HasMaxLength(4000);

                entity.Property(e => e.LoanAmount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.LoanFirstPayment).HasColumnType("datetime");

                entity.Property(e => e.LoanMonthlyRate).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.LoanPaperworkSignature).HasMaxLength(4000);

                entity.Property(e => e.MaritalStatus).HasMaxLength(4000);

                entity.Property(e => e.MaxLoanAmount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.MiddleNames).HasMaxLength(4000);

                entity.Property(e => e.Mobile)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Ninumber)
                    .HasColumnName("NINumber")
                    .HasMaxLength(4000);

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Plscore)
                    .HasColumnName("PLScore")
                    .HasColumnType("decimal(29, 4)");

                entity.Property(e => e.ReasonForLoan).HasMaxLength(4000);

                entity.Property(e => e.RiskScore).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Title).HasMaxLength(4000);

                entity.Property(e => e.WoPlScore)
                    .HasColumnName("WO_PL_Score")
                    .HasColumnType("decimal(29, 4)");

                entity.Property(e => e.WorkPhone)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Woscore)
                    .HasColumnName("WOScore")
                    .HasColumnType("decimal(29, 4)");

                entity.HasOne(d => d.CreditTeam)
                    .WithMany(p => p.LoanApplication)
                    .HasForeignKey(d => d.CreditTeamId)
                    .HasConstraintName("CreditTeam_LoanApplication");
            });

            modelBuilder.Entity<LoanApplicationAssessment>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationAssessment)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationAssessment");
            });

            modelBuilder.Entity<LoanApplicationAuthorisation>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedOnBehalf)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Subcategory)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationAuthorisation)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationAuthorisation");
            });

            modelBuilder.Entity<LoanApplicationAuthorisationNote>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationAuthorisationId)
                    .HasName("IX_LoanApplicationAuthorisationId");

                entity.Property(e => e.AdditionalRequirements)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplicationAuthorisation)
                    .WithMany(p => p.LoanApplicationAuthorisationNote)
                    .HasForeignKey(d => d.LoanApplicationAuthorisationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("QuoteAuthorisation_QuoteAuthorisationNote1");
            });

            modelBuilder.Entity<LoanApplicationCheck>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationCheck)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationCheck");
            });

            modelBuilder.Entity<LoanApplicationCheckAlt>(entity =>
            {
                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationCheckAlt)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationCheckAlt");
            });

            modelBuilder.Entity<LoanApplicationHistory>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationHistory)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationHistory");
            });

            modelBuilder.Entity<LoanApplicationIe>(entity =>
            {
                entity.ToTable("LoanApplicationIE");

                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.Group)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Item)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Value).HasColumnType("decimal(29, 4)");

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationIe)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationIE");
            });

            modelBuilder.Entity<LoanApplicationIncome>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.BenefitType).HasMaxLength(4000);

                entity.Property(e => e.EmploymentAddressCountry).HasMaxLength(4000);

                entity.Property(e => e.EmploymentAddressCounty).HasMaxLength(4000);

                entity.Property(e => e.EmploymentAddressLine1).HasMaxLength(4000);

                entity.Property(e => e.EmploymentAddressLine2).HasMaxLength(4000);

                entity.Property(e => e.EmploymentAddressLine3).HasMaxLength(4000);

                entity.Property(e => e.EmploymentAddressPostcode).HasMaxLength(4000);

                entity.Property(e => e.EmploymentAddressTown).HasMaxLength(4000);

                entity.Property(e => e.EmploymentEmployerName).HasMaxLength(4000);

                entity.Property(e => e.EmploymentJobTitle).HasMaxLength(4000);

                entity.Property(e => e.EmploymentLastPayDate).HasColumnType("datetime");

                entity.Property(e => e.EmploymentNextPayDate).HasColumnType("datetime");

                entity.Property(e => e.EmploymentStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationIncome)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationIncome");
            });

            modelBuilder.Entity<LoanApplicationLink>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.LoanNumber)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationLink)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationLink");
            });

            modelBuilder.Entity<LoanApplicationMiscDetail>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationMiscDetail)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationMiscDetail");
            });

            modelBuilder.Entity<LoanApplicationPredictor>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationPredictor)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationPredictor");
            });

            modelBuilder.Entity<LoanApplicationSource>(entity =>
            {
                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationSource)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationSource");
            });

            modelBuilder.Entity<LoanApplicationZva>(entity =>
            {
                entity.ToTable("LoanApplicationZVA");

                entity.HasIndex(e => e.LoanApplicationId)
                    .HasName("IX_LoanApplicationId");

                entity.Property(e => e.Authcode)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.AvsaddressMatch)
                    .IsRequired()
                    .HasColumnName("AVSAddressMatch")
                    .HasMaxLength(4000);

                entity.Property(e => e.AvspostcodeMatch)
                    .IsRequired()
                    .HasColumnName("AVSPostcodeMatch")
                    .HasMaxLength(4000);

                entity.Property(e => e.Cvnmatch)
                    .IsRequired()
                    .HasColumnName("CVNMatch")
                    .HasMaxLength(4000);

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Mask)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.ResultCode)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Srd)
                    .HasColumnName("SRD")
                    .HasMaxLength(100);

                entity.Property(e => e.TokenId)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.TransId)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.LoanApplication)
                    .WithMany(p => p.LoanApplicationZva)
                    .HasForeignKey(d => d.LoanApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanApplication_LoanApplicationZVA");
            });

            modelBuilder.Entity<LoanArtefactLogs>(entity =>
            {
                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanArtefactLogs)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanArtefactLog");
            });

            modelBuilder.Entity<LoanArtefacts>(entity =>
            {
                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanArtefacts)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanArtefact");
            });

            modelBuilder.Entity<LoanContactTime>(entity =>
            {
                entity.Property(e => e.Finish).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Start).HasColumnType("decimal(29, 4)");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanContactTime)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanContactTime");
            });

            modelBuilder.Entity<LoanDetails>(entity =>
            {
                entity.HasKey(e => e.LoanId)
                    .HasName("PK_dbo.[Loan Details]");

                entity.ToTable("Loan Details");

                entity.HasIndex(e => e.BelongsHere)
                    .HasName("Loan_Belongs");

                entity.HasIndex(e => e.CarId)
                    .HasName("IX_Loan Details");

                entity.HasIndex(e => e.DateSigned)
                    .HasName("IX_Loan Details_1");

                entity.HasIndex(e => e.Number)
                    .HasName("IX_Loan_Number")
                    .IsUnique();

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.AdvertisingSource).HasMaxLength(255);

                entity.Property(e => e.AmountLoaned)
                    .HasColumnName("Amount loaned")
                    .HasColumnType("money");

                entity.Property(e => e.Apr).HasColumnName("APR");

                entity.Property(e => e.CarId).HasColumnName("CarID");

                entity.Property(e => e.CustomerOccupation).HasMaxLength(50);

                entity.Property(e => e.CustomerType)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DateSigned)
                    .HasColumnName("Date signed")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsurancePolicyNumber).HasMaxLength(4000);

                entity.Property(e => e.InsuranceType).HasMaxLength(4000);

                entity.Property(e => e.InsurerName).HasMaxLength(50);

                entity.Property(e => e.NetSaleProceeds).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Nsaprinted)
                    .HasColumnName("NSAPrinted")
                    .HasColumnType("datetime");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.NwNumber)
                    .HasColumnName("NW number")
                    .HasMaxLength(50);

                entity.Property(e => e.NwlastNumber).HasColumnName("NWLastNumber");

                entity.Property(e => e.NwnumberSecond).HasColumnName("NWNumberSecond");

                entity.Property(e => e.NwnumberSlips).HasColumnName("NWNumberSlips");

                entity.Property(e => e.OdlettersStopped)
                    .HasColumnName("ODLettersStopped")
                    .HasColumnType("datetime");

                entity.Property(e => e.Password).HasMaxLength(4000);

                entity.Property(e => e.ProductName).HasMaxLength(4000);

                entity.Property(e => e.Representative).HasMaxLength(255);

                entity.Property(e => e.Representative1).HasMaxLength(255);

                entity.Property(e => e.SettlementDate)
                    .HasColumnName("Settlement date")
                    .HasColumnType("datetime");

                entity.Property(e => e.SettlementType).HasMaxLength(255);

                entity.Property(e => e.SignedLocation)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SoldForAtAuction).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Stonumber)
                    .HasColumnName("STONumber")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.LoanDetails)
                    //.WithOne(p => p.LoanDetails)
                    .HasForeignKey(d => d.CarId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Car_Loan_Detail");
            });

            modelBuilder.Entity<LoanFinancialPlan>(entity =>
            {
                entity.HasKey(e => e.LoanId)
                    .HasName("PK_dbo.LoanFinancialPlan");

                entity.Property(e => e.LoanId).ValueGeneratedNever();

                entity.Property(e => e.FinalPaymentAmount).HasColumnType("money");

                entity.Property(e => e.FinalPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.FirstPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.WeeklyAmount).HasColumnType("money");

                entity.HasOne(d => d.Loan)
                    .WithOne(p => p.LoanFinancialPlan)
                    .HasForeignKey<LoanFinancialPlan>(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanFinancialPlan");
            });

            modelBuilder.Entity<LoanFundings>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.FundedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanFundings)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanFunding");
            });

            modelBuilder.Entity<LoanGuarantor>(entity =>
            {
                entity.HasKey(e => e.LoanId)
                    .HasName("PK_dbo.LoanGuarantor");

                entity.Property(e => e.LoanId).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.County).HasMaxLength(50);

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName).HasMaxLength(30);

                entity.Property(e => e.HomeOwner)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.PhoneDaytime).HasMaxLength(50);

                entity.Property(e => e.PhoneEvening).HasMaxLength(50);

                entity.Property(e => e.PhoneMobile).HasMaxLength(50);

                entity.Property(e => e.PostalCode).HasMaxLength(20);

                entity.Property(e => e.Title).HasMaxLength(10);

                entity.Property(e => e.Town).HasMaxLength(50);

                entity.HasOne(d => d.Loan)
                    .WithOne(p => p.LoanGuarantor)
                    .HasForeignKey<LoanGuarantor>(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanGuarantor");
            });

            modelBuilder.Entity<LoanIncomes>(entity =>
            {
                entity.Property(e => e.Customer)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.EmployerAddress).HasMaxLength(4000);

                entity.Property(e => e.EmployerName).HasMaxLength(4000);

                entity.Property(e => e.EmployerPhone).HasMaxLength(4000);

                entity.Property(e => e.Frequency)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PayDate)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PayMethod)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanIncomes)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanIncome");
            });

            modelBuilder.Entity<LoanProduct>(entity =>
            {
                entity.Property(e => e.AuthThreshold).HasColumnType("money");

                entity.Property(e => e.DmsdocumentName)
                    .IsRequired()
                    .HasColumnName("DMSDocumentName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsAvailableInEw).HasColumnName("IsAvailableInEW");

                entity.Property(e => e.MaxAmount).HasColumnType("money");

                entity.Property(e => e.MinAmount).HasColumnType("money");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<LoanSale>(entity =>
            {
                entity.Property(e => e.AccountSoldAt).HasColumnType("datetime");

                entity.HasOne(d => d.DebtBuyer)
                    .WithMany(p => p.LoanSale)
                    .HasForeignKey(d => d.DebtBuyerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DebtBuyer_LoanSale");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanSale)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanSale");
            });

            modelBuilder.Entity<LoanSecondSignatory>(entity =>
            {
                entity.HasKey(e => e.LoanId)
                    .HasName("PK_dbo.LoanSecondSignatory");

                entity.Property(e => e.LoanId).ValueGeneratedNever();

                entity.Property(e => e.AddressLine1).HasMaxLength(4000);

                entity.Property(e => e.AddressLine2).HasMaxLength(4000);

                entity.Property(e => e.AddressLine3).HasMaxLength(4000);

                entity.Property(e => e.County).HasMaxLength(50);

                entity.Property(e => e.DateMoved).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.FirstName).HasMaxLength(30);

                entity.Property(e => e.HomeOwner)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.Ninumber)
                    .HasColumnName("NINumber")
                    .HasMaxLength(20);

                entity.Property(e => e.PostalCode).HasMaxLength(20);

                entity.Property(e => e.Title).HasMaxLength(10);

                entity.Property(e => e.Town).HasMaxLength(50);

                entity.HasOne(d => d.Loan)
                    .WithOne(p => p.LoanSecondSignatory)
                    .HasForeignKey<LoanSecondSignatory>(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanSecondSignatory");
            });

            modelBuilder.Entity<LoanStateChanges>(entity =>
            {
                entity.HasKey(e => e.RecordGuid)
                    .HasName("PK_dbo.LoanStateChanges");

                entity.HasIndex(e => e.DateOfChange)
                    .HasName("IX_LoanStateDate");

                entity.Property(e => e.RecordGuid).ValueGeneratedNever();

                entity.Property(e => e.DateOfChange).HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.NewState)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OriginalState)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanStateChanges)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanStateChange");
            });

            modelBuilder.Entity<LoanStatusChanges>(entity =>
            {
                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.NewStatus)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.OriginalStatus)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanStatusChanges)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanStatusChange");
            });

            modelBuilder.Entity<LoanSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("LoanSummary");

                entity.Property(e => e.CarId).HasColumnName("CarID");

                entity.Property(e => e.DateSigned)
                    .HasColumnName("Date signed")
                    .HasColumnType("datetime");

                entity.Property(e => e.FinalPaymentAmount).HasColumnType("money");

                entity.Property(e => e.FinalPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.LoanAmount).HasColumnType("money");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.TotalInterestWrittenOff).HasColumnType("money");

                entity.Property(e => e.TotalPayments).HasColumnType("money");

                entity.Property(e => e.TotalPaymentsPartSettl).HasColumnType("money");

                entity.Property(e => e.WeeklyAmount).HasColumnType("money");
            });

            modelBuilder.Entity<LoanTaggingActions>(entity =>
            {
                entity.HasIndex(e => e.LoanId)
                    .HasName("IxLoanId");

                entity.HasIndex(e => e.TagName)
                    .HasName("IX_TagName");

                entity.Property(e => e.AppliedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DateApplied).HasColumnType("datetime");

                entity.Property(e => e.TagName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.LoanTaggingActions)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanTaggingAction");
            });

            modelBuilder.Entity<LoanTerm>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<LoanTopups>(entity =>
            {
                entity.Property(e => e.SettlementAmount).HasColumnType("decimal(29, 4)");

                entity.HasOne(d => d.OriginalLoan)
                    .WithMany(p => p.LoanTopupsOriginalLoan)
                    .HasForeignKey(d => d.OriginalLoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanTopup1");

                entity.HasOne(d => d.TopupLoan)
                    .WithMany(p => p.LoanTopupsTopupLoan)
                    .HasForeignKey(d => d.TopupLoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_LoanTopup");
            });

            modelBuilder.Entity<MarketingCampaign>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Email).HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Mobile).HasMaxLength(4000);

                entity.Property(e => e.Type).HasMaxLength(4000);
            });

            modelBuilder.Entity<Matrix>(entity =>
            {
                entity.HasIndex(e => e.MatrixTypeId)
                    .HasName("IxMatrixType_Matrix");

                entity.HasIndex(e => e.UnderwriterId)
                    .HasName("IxUnderwriter_Matrix");

                entity.Property(e => e.PostcodePrefix)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.MatrixType)
                    .WithMany(p => p.Matrix)
                    .HasForeignKey(d => d.MatrixTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MatrixType_Matrix");

                entity.HasOne(d => d.Underwriter)
                    .WithMany(p => p.Matrix)
                    .HasForeignKey(d => d.UnderwriterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Underwriter_Matrix");
            });

            modelBuilder.Entity<MatrixType>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<MatrixUsage>(entity =>
            {
                entity.HasIndex(e => e.ArrangingUwmatrixTypeId)
                    .HasName("IxMatrixType_MatrixUsage");

                entity.HasIndex(e => e.SigningUwmatrixTypeId)
                    .HasName("IxMatrixType_MatrixUsage1");

                entity.Property(e => e.AdSource)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.ArrangingUwmatrixTypeId).HasColumnName("ArrangingUWMatrixTypeId");

                entity.Property(e => e.SigningUwmatrixTypeId).HasColumnName("SigningUWMatrixTypeId");

                entity.HasOne(d => d.ArrangingUwmatrixType)
                    .WithMany(p => p.MatrixUsageArrangingUwmatrixType)
                    .HasForeignKey(d => d.ArrangingUwmatrixTypeId)
                    .HasConstraintName("MatrixType_MatrixUsage");

                entity.HasOne(d => d.SigningUwmatrixType)
                    .WithMany(p => p.MatrixUsageSigningUwmatrixType)
                    .HasForeignKey(d => d.SigningUwmatrixTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MatrixType_MatrixUsage1");
            });

            modelBuilder.Entity<Nbtstat>(entity =>
            {
                entity.ToTable("NBTStat");

                entity.HasIndex(e => new { e.Event, e.GeneratedAt })
                    .HasName("IX_Event");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Nbtuser)
                    .IsRequired()
                    .HasColumnName("NBTUser")
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Notes>(entity =>
            {
                entity.HasKey(e => e.NoteId)
                    .HasName("PK_dbo.Notes");

                entity.HasIndex(e => e.LoanId)
                    .HasName("IX_Notes");

                entity.Property(e => e.NoteId).HasColumnName("NoteID");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Text).HasColumnType("ntext");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.Notes)
                    .HasForeignKey(d => d.LoanId)
                    .HasConstraintName("Loan_Detail_Note");
            });

            modelBuilder.Entity<OlawcfplusLog>(entity =>
            {
                entity.ToTable("OLAWCFPlusLog");

                entity.Property(e => e.CalledAt).HasColumnType("datetime");

                entity.Property(e => e.CalledBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Host)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.MethodName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Parameters)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<OutstandingPdfchange>(entity =>
            {
                entity.ToTable("OutstandingPDFChange");

                entity.Property(e => e.ChangedAt).HasColumnType("datetime");

                entity.Property(e => e.Loan)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<PartialEarlySettlements>(entity =>
            {
                entity.HasKey(e => e.OfferId)
                    .HasName("PK_dbo.PartialEarlySettlements");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.OfferDate).HasColumnType("datetime");

                entity.Property(e => e.SettlementAmount).HasColumnType("money");
            });

            modelBuilder.Entity<PayOut>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<PaymentImportLoan>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PaymentImportLoan");

                entity.Property(e => e.AgreementNo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.FirstName).HasMaxLength(30);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.NwNumber)
                    .HasColumnName("NW number")
                    .HasMaxLength(50);

                entity.Property(e => e.NwlastNumber).HasColumnName("NWLastNumber");

                entity.Property(e => e.NwnumberSecond).HasColumnName("NWNumberSecond");

                entity.Property(e => e.NwnumberSlips).HasColumnName("NWNumberSlips");

                entity.Property(e => e.RegNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Stonumber)
                    .HasColumnName("STONumber")
                    .HasMaxLength(100);

                entity.Property(e => e.Surname).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(10);

                entity.Property(e => e.WeeklyAmount).HasColumnType("money");
            });

            modelBuilder.Entity<PaymentInputTemp>(entity =>
            {
                entity.HasKey(e => e.PaymentId)
                    .HasName("PK_dbo.PaymentInputTemp");

                entity.Property(e => e.PaymentId)
                    .HasColumnName("PaymentID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Bank)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Database)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Detail1)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Detail2)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Detail3)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Detail4)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Detail5)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ImportDate).HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.MsreplTranVersion).HasColumnName("msrepl_tran_version");

                entity.Property(e => e.NwnumberRange)
                    .HasColumnName("NWnumberRange")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Payment).HasColumnType("money");

                entity.Property(e => e.PaymentDate).HasColumnType("datetime");

                entity.Property(e => e.PaymentType)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Stonumber)
                    .HasColumnName("STONumber")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PaymentPlans>(entity =>
            {
                entity.HasIndex(e => e.LoanId)
                    .HasName("IX_LoanId");

                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.FinishAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LastUpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.StartAt).HasColumnType("datetime");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.PaymentPlans)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_PaymentPlan");
            });

            modelBuilder.Entity<PaymentPromises>(entity =>
            {
                entity.HasIndex(e => new { e.LoanId, e.State })
                    .HasName("IX_LoaidIdStatus");

                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.DueAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LastUpdatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.PaymentPromises)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_PaymentPromise");
            });

            modelBuilder.Entity<Payments>(entity =>
            {
                entity.HasKey(e => e.PaymentId)
                    .HasName("PK_dbo.Payments");

                entity.HasIndex(e => e.LoanId)
                    .HasName("IX_Payments");

                entity.HasIndex(e => e.PaymentDate)
                    .HasName("IX_Payments_1");

                entity.Property(e => e.PaymentId).HasColumnName("PaymentID");

                entity.Property(e => e.AllocatedToSettlement).HasColumnType("money");

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.CardPaymentId).HasColumnName("CardPaymentID");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.PaymentDate).HasColumnType("datetime");

                entity.Property(e => e.ReceiptNo)
                    .HasColumnName("Receipt No")
                    .HasMaxLength(255);

                entity.Property(e => e.TransactionDate).HasColumnType("datetime");

                entity.HasOne(d => d.CardPayment)
                    .WithMany(p => p.InverseCardPayment)
                    .HasForeignKey(d => d.CardPaymentId)
                    .HasConstraintName("Payment_Payment");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_Payment");
            });

            modelBuilder.Entity<PostcodePrefix>(entity =>
            {
                entity.Property(e => e.Prefix)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Quote>(entity =>
            {
                entity.HasIndex(e => e.ArrangingUnderwriterId)
                    .HasName("IX_ArrangingUnderwriterId");

                entity.HasIndex(e => e.Customer2DateOfBirth);

                entity.HasIndex(e => e.CustomerDateOfBirth);

                entity.HasIndex(e => e.CustomerFullNameClean);

                entity.HasIndex(e => e.LastWorkedAt);

                entity.HasIndex(e => e.LeadId)
                    .HasName("IX_LeadId");

                entity.HasIndex(e => e.Number)
                    .HasName("IX_Number");

                entity.Property(e => e.AdviceToAdm)
                    .HasColumnName("AdviceToADM")
                    .HasMaxLength(4000);

                entity.Property(e => e.AppointmentFailComment).HasMaxLength(4000);

                entity.Property(e => e.AppointmentFailReason).HasMaxLength(4000);

                entity.Property(e => e.BankAccountNumber).HasMaxLength(10);

                entity.Property(e => e.BankSortCode).HasMaxLength(10);

                entity.Property(e => e.ContactAgainAt).HasColumnType("datetime");

                entity.Property(e => e.Customer2DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.Customer2Employer).HasMaxLength(50);

                entity.Property(e => e.Customer2Gender).HasMaxLength(20);

                entity.Property(e => e.Customer2HomeownerStatus).HasMaxLength(4000);

                entity.Property(e => e.Customer2MovedToCurrentAddressAt).HasColumnType("datetime");

                entity.Property(e => e.Customer2Name).HasMaxLength(4000);

                entity.Property(e => e.Customer2NextPayDay).HasColumnType("datetime");

                entity.Property(e => e.Customer2Ninumber)
                    .HasColumnName("Customer2NINumber")
                    .HasMaxLength(20);

                entity.Property(e => e.Customer2Occupation).HasMaxLength(4000);

                entity.Property(e => e.Customer2PayFrequency).HasMaxLength(100);

                entity.Property(e => e.Customer2StartedEmploymentAt).HasColumnType("datetime");

                entity.Property(e => e.Customer2SubsequentPayDay).HasColumnType("datetime");

                entity.Property(e => e.Customer2Surname).HasMaxLength(4000);

                entity.Property(e => e.Customer2Title).HasMaxLength(4000);

                entity.Property(e => e.CustomerAddressLine1).HasMaxLength(4000);

                entity.Property(e => e.CustomerAddressLine2).HasMaxLength(4000);

                entity.Property(e => e.CustomerAddressLine3).HasMaxLength(4000);

                entity.Property(e => e.CustomerCounty).HasMaxLength(4000);

                entity.Property(e => e.CustomerDateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.CustomerEmployer).HasMaxLength(50);

                entity.Property(e => e.CustomerFullNameClean)
                    .HasMaxLength(4000)
                    .HasComputedColumnSql("(upper((ltrim(rtrim(coalesce([CustomerName],'')))+'  ')+ltrim(rtrim(coalesce([CustomerSurname],'')))))");

                entity.Property(e => e.CustomerGender).HasMaxLength(20);

                entity.Property(e => e.CustomerHomeownerStatus).HasMaxLength(4000);

                entity.Property(e => e.CustomerIncomeInclBenefits).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.CustomerMovedToCurrentAddressAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.CustomerNameClean)
                    .HasMaxLength(4000)
                    .HasComputedColumnSql("(ltrim(rtrim(coalesce([CustomerName],''))))");

                entity.Property(e => e.CustomerNextPayDay).HasColumnType("datetime");

                entity.Property(e => e.CustomerNinumber)
                    .HasColumnName("CustomerNINumber")
                    .HasMaxLength(20);

                entity.Property(e => e.CustomerOccupation).HasMaxLength(4000);

                entity.Property(e => e.CustomerPayFrequency).HasMaxLength(100);

                entity.Property(e => e.CustomerPostcode).HasMaxLength(4000);

                entity.Property(e => e.CustomerStartedEmploymentAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerSubsequentPayDay).HasColumnType("datetime");

                entity.Property(e => e.CustomerSurname)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.CustomerTitle).HasMaxLength(4000);

                entity.Property(e => e.CustomerTown).HasMaxLength(4000);

                entity.Property(e => e.ElmsloanAgrNo)
                    .HasColumnName("ELMSLoanAgrNo")
                    .HasMaxLength(4000);

                entity.Property(e => e.GeodemographicsAreaAffluence).HasMaxLength(4000);

                entity.Property(e => e.GeodemographicsAreaLifestage).HasMaxLength(4000);

                entity.Property(e => e.InvitationReceivedComment).HasMaxLength(4000);

                entity.Property(e => e.InvitationReceivedFrom).HasMaxLength(4000);

                entity.Property(e => e.LastWorkedAt).HasColumnType("datetime");

                entity.Property(e => e.LoanAmount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.LoanFirstPayment).HasColumnType("datetime");

                entity.Property(e => e.LoanMonthlyRate).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.LoanSignedDate).HasColumnType("datetime");

                entity.Property(e => e.LoanWeeklyPayment).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.NbtbankAccountDebitCard).HasColumnName("NBTBankAccountDebitCard");

                entity.Property(e => e.NbtemployedStatus).HasColumnName("NBTEmployedStatus");

                entity.Property(e => e.NbtfundsForSettingUpCpa).HasColumnName("NBTFundsForSettingUpCPA");

                entity.Property(e => e.NbtfundsRequested)
                    .HasColumnName("NBTFundsRequested")
                    .HasColumnType("decimal(29, 4)");

                entity.Property(e => e.NbthomeownerStatus).HasColumnName("NBTHomeownerStatus");

                entity.Property(e => e.NbtivabankruptTrustDeed).HasColumnName("NBTIVABankruptTrustDeed");

                entity.Property(e => e.NbtnumberOfPaydayLoans).HasColumnName("NBTNumberOfPaydayLoans");

                entity.Property(e => e.NbtownCar).HasColumnName("NBTOwnCar");

                entity.Property(e => e.NbttimeAtAddress).HasColumnName("NBTTimeAtAddress");

                entity.Property(e => e.NbttimeInEmployment).HasColumnName("NBTTimeInEmployment");

                entity.Property(e => e.PrintingResetAt).HasColumnType("datetime");

                entity.Property(e => e.ScoringSalesResetAt).HasColumnType("datetime");

                entity.Property(e => e.ScoringUnderwritingResetAt).HasColumnType("datetime");

                entity.Property(e => e.SigningDate).HasColumnType("datetime");

                entity.Property(e => e.SigningLocation).HasMaxLength(4000);

                entity.Property(e => e.SigningMeetingFinish).HasMaxLength(10);

                entity.Property(e => e.SigningMeetingStart).HasMaxLength(10);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.VehicleChassisNumber).HasMaxLength(4000);

                entity.Property(e => e.VehicleColour).HasMaxLength(4000);

                entity.Property(e => e.VehicleInsurancePolicyNumber).HasMaxLength(50);

                entity.Property(e => e.VehicleMake).HasMaxLength(4000);

                entity.Property(e => e.VehicleMileageOnMot).HasColumnName("VehicleMileageOnMOT");

                entity.Property(e => e.VehicleModel).HasMaxLength(4000);

                entity.Property(e => e.VehicleMotexpiryDate)
                    .HasColumnName("VehicleMOTExpiryDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.VehicleMottestNumber)
                    .HasColumnName("VehicleMOTTestNumber")
                    .HasMaxLength(4000);

                entity.Property(e => e.VehicleRegNo).HasMaxLength(4000);

                entity.Property(e => e.VehicleRegistered).HasMaxLength(4000);

                entity.Property(e => e.VehicleSpec).HasMaxLength(4000);

                entity.Property(e => e.VehicleTaxExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.VehicleV5documentReference)
                    .HasColumnName("VehicleV5DocumentReference")
                    .HasMaxLength(4000);

                entity.Property(e => e.VehicleV5issueDate)
                    .HasColumnName("VehicleV5IssueDate")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.ArrangingUnderwriter)
                    .WithMany(p => p.QuoteArrangingUnderwriter)
                    .HasForeignKey(d => d.ArrangingUnderwriterId)
                    .HasConstraintName("Underwriter_Quote");

                entity.HasOne(d => d.Franchise)
                    .WithMany(p => p.Quote)
                    .HasForeignKey(d => d.FranchiseId)
                    .HasConstraintName("Franchise_Quote");

                entity.HasOne(d => d.SigningUnderwriter)
                    .WithMany(p => p.QuoteSigningUnderwriter)
                    .HasForeignKey(d => d.SigningUnderwriterId)
                    .HasConstraintName("Underwriter_Quote1");

                entity.HasOne(d => d.VehicleInsurer)
                    .WithMany(p => p.Quote)
                    .HasForeignKey(d => d.VehicleInsurerId)
                    .HasConstraintName("Insurer_Quote");
            });

            modelBuilder.Entity<QuoteAddress>(entity =>
            {
                entity.Property(e => e.AddressLine1).HasMaxLength(4000);

                entity.Property(e => e.AddressLine2).HasMaxLength(4000);

                entity.Property(e => e.AddressLine3).HasMaxLength(4000);

                entity.Property(e => e.County).HasMaxLength(4000);

                entity.Property(e => e.Postcode).HasMaxLength(4000);

                entity.Property(e => e.Town).HasMaxLength(4000);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteAddress)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteAddress");
            });

            modelBuilder.Entity<QuoteAppointment>(entity =>
            {
                entity.HasIndex(e => e.QuoteId)
                    .HasName("IxQuoteId");

                entity.HasIndex(e => new { e.Id, e.QuoteId, e.Status, e.AppointmentAt })
                    .HasName("IxAppointmentAt");

                entity.Property(e => e.AppointmentAt).HasColumnType("datetime");

                entity.Property(e => e.BookedAt).HasColumnType("datetime");

                entity.Property(e => e.BookedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.CourtesyCallMadeAt).HasColumnType("datetime");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.StatusUpdatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteAppointment)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteAppointment");
            });

            modelBuilder.Entity<QuoteAuthorisation>(entity =>
            {
                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedOnBehalf)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Subcategory)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteAuthorisation)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteAuthorisation");
            });

            modelBuilder.Entity<QuoteAuthorisationNote>(entity =>
            {
                entity.Property(e => e.AdditionalRequirements)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.QuoteAuthorisation)
                    .WithMany(p => p.QuoteAuthorisationNote)
                    .HasForeignKey(d => d.QuoteAuthorisationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("QuoteAuthorisation_QuoteAuthorisationNote");
            });

            modelBuilder.Entity<QuoteDataConfirmation>(entity =>
            {
                entity.Property(e => e.FieldGroup)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteDataConfirmation)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteDataConfirmation");
            });

            modelBuilder.Entity<QuoteDiaryEntry>(entity =>
            {
                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.CompletedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.CompletedComment)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.DueAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteDiaryEntry)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteDiaryEntry");
            });

            modelBuilder.Entity<QuoteFunding>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.FundedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteFunding)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteFunding");
            });

            modelBuilder.Entity<QuoteHistory>(entity =>
            {
                entity.HasIndex(e => e.QuoteId)
                    .HasName("IX_QuoteId");

                entity.Property(e => e.Comment).HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteHistory)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteHistory");
            });

            modelBuilder.Entity<QuoteLink>(entity =>
            {
                entity.Property(e => e.LoanNumber)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteLink)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteLink");
            });

            modelBuilder.Entity<QuoteScore>(entity =>
            {
                entity.HasIndex(e => e.QuoteId)
                    .HasName("IX_QuoteId");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.MaxLoanAmount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Score).HasColumnType("decimal(29, 4)");

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteScore)
                    .HasForeignKey(d => d.QuoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Quote_QuoteScore");
            });

            modelBuilder.Entity<RePayFirstCpaattempt>(entity =>
            {
                entity.ToTable("RePayFirstCPAAttempt");

                entity.HasIndex(e => e.LoanId);

                entity.Property(e => e.FirstCpaattemptPaymentDate)
                    .HasColumnName("FirstCPAAttemptPaymentDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.RePayFirstCpaattempt)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_RePayFirstCPAAttemptFailed");
            });

            modelBuilder.Entity<RePayFirstCpaattemptFailed>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("RePayFirstCPAAttemptFailed");

                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");
            });

            modelBuilder.Entity<RePayPayment>(entity =>
            {
                entity.HasIndex(e => e.LoanId);

                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.RePayPayment)
                    .HasForeignKey(d => d.LoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Loan_Detail_RePayPayment");
            });

            modelBuilder.Entity<RegionalManager>(entity =>
            {
                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile)
                    .IsRequired()
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Username).HasMaxLength(4000);
            });

            modelBuilder.Entity<RepossessionCosts>(entity =>
            {
                entity.HasKey(e => e.RcostId)
                    .HasName("PK_dbo.RepossessionCosts");

                entity.HasIndex(e => e.Date)
                    .HasName("IX_RepossessionCosts_1");

                entity.HasIndex(e => e.LoanId)
                    .HasName("IX_RepossessionCosts");

                entity.Property(e => e.RcostId).HasColumnName("RCostID");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.HasOne(d => d.Loan)
                    .WithMany(p => p.RepossessionCosts)
                    .HasForeignKey(d => d.LoanId)
                    .HasConstraintName("Loan_Detail_RepossessionCost");
            });

            modelBuilder.Entity<Setting>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<TemporaryUnderwriter>(entity =>
            {
                entity.HasIndex(e => e.ActingUnderwriterId)
                    .HasName("IxUnderwriter_TemporaryUnderwriter1");

                entity.HasIndex(e => e.ReplacedUnderwriterId)
                    .HasName("IxUnderwriter_TemporaryUnderwriter");

                entity.HasOne(d => d.ActingUnderwriter)
                    .WithMany(p => p.TemporaryUnderwriterActingUnderwriter)
                    .HasForeignKey(d => d.ActingUnderwriterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Underwriter_TemporaryUnderwriter1");

                entity.HasOne(d => d.ReplacedUnderwriter)
                    .WithMany(p => p.TemporaryUnderwriterReplacedUnderwriter)
                    .HasForeignKey(d => d.ReplacedUnderwriterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Underwriter_TemporaryUnderwriter");
            });

            modelBuilder.Entity<Tmp>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<TmpAcc>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TmpACC");

                entity.Property(e => e.Status)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TmpCallQueue>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<TmpEntityUpdatedColumnLog>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tmpEntityUpdatedColumnLog");

                entity.Property(e => e.EntityColumn)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.EntityType)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.NewValue).HasMaxLength(4000);

                entity.Property(e => e.OldValue).HasMaxLength(4000);
            });

            modelBuilder.Entity<TmpGdprImport>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tmp_GDPR_import");

                entity.Property(e => e.EmailValue).HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.Olaid).HasColumnName("OLAid");

                entity.Property(e => e.PhoneValue).HasMaxLength(4000);

                entity.Property(e => e.Sms).HasColumnName("SMS");

                entity.Property(e => e.Smsvalue)
                    .HasColumnName("SMSValue")
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<TmpGdprImport2>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tmp_GDPR_import2");

                entity.Property(e => e.Olaid).HasColumnName("OLAid");

                entity.Property(e => e.Sms).HasColumnName("SMS");
            });

            modelBuilder.Entity<TmpGdprconsent>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tmpGDPRConsent");

                entity.Property(e => e.EntityType)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<TmpLead>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Code)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TmpLetter>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tmpLetter");

                entity.Property(e => e.Data)
                    .HasColumnName("DATA")
                    .IsUnicode(false);

                entity.Property(e => e.DisplayType)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Dte).HasColumnType("datetime");
            });

            modelBuilder.Entity<TmpLoanCust>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Customer)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LoanId).HasColumnName("LoanID");

                entity.Property(e => e.SettledDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TmpPtp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tmpPTP");

                entity.Property(e => e.Agr)
                    .HasColumnName("agr")
                    .HasMaxLength(200);

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.Dueat)
                    .HasColumnName("dueat")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<TmpPurgeLeads>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<TmpPurgeLoans>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.LoanId).HasColumnName("LoanID");
            });

            modelBuilder.Entity<TmpPurgeOla>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TmpPurgeOLA");

                entity.Property(e => e.Olaid).HasColumnName("OLAId");
            });

            modelBuilder.Entity<TmpPurgeQuotes>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<TmpTbl>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Data).HasColumnName("data");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Underwriter>(entity =>
            {
                entity.HasIndex(e => e.FranchiseId)
                    .HasName("IxFranchise_Underwriter");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Landline).HasMaxLength(4000);

                entity.Property(e => e.Mobile)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.SolicitorAddress).HasMaxLength(4000);

                entity.Property(e => e.SolicitorCompany).HasMaxLength(4000);

                entity.Property(e => e.Username).HasMaxLength(4000);

                entity.HasOne(d => d.Franchise)
                    .WithMany(p => p.Underwriter)
                    .HasForeignKey(d => d.FranchiseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Franchise_Underwriter");

                entity.HasOne(d => d.RegionalManager)
                    .WithMany(p => p.Underwriter)
                    .HasForeignKey(d => d.RegionalManagerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("RegionalManager_Underwriter");
            });

            modelBuilder.Entity<UnderwriterChequeBook>(entity =>
            {
                entity.HasOne(d => d.Underwriter)
                    .WithMany(p => p.UnderwriterChequeBook)
                    .HasForeignKey(d => d.UnderwriterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Underwriter_UnderwriterChequeBook");
            });

            modelBuilder.Entity<UnderwriterLoanProduct>(entity =>
            {
                entity.HasOne(d => d.LoanProduct)
                    .WithMany(p => p.UnderwriterLoanProduct)
                    .HasForeignKey(d => d.LoanProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanProduct_UnderwriterLoanProduct");

                entity.HasOne(d => d.Underwriter)
                    .WithMany(p => p.UnderwriterLoanProduct)
                    .HasForeignKey(d => d.UnderwriterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Underwriter_UnderwriterLoanProduct");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.IsAllowedToWo).HasColumnName("IsAllowedToWO");

                entity.Property(e => e.IsItteam).HasColumnName("IsITTeam");

                entity.Property(e => e.IsOlareports).HasColumnName("IsOLAReports");

                entity.Property(e => e.IsQa).HasColumnName("IsQA");

                entity.Property(e => e.IsTsnoEntry).HasColumnName("IsTSNoEntry");

                entity.Property(e => e.IsTsstop).HasColumnName("IsTSStop");

                entity.Property(e => e.IsTswarning).HasColumnName("IsTSWarning");

                entity.Property(e => e.IsTswrittenOff).HasColumnName("IsTSWrittenOff");

                entity.Property(e => e.IsUwteamManager).HasColumnName("IsUWTeamManager");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<VeMaSonHold>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VeMaSOnHold");

                entity.Property(e => e.AgreementNumber)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<VehicleColour>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<WcfplusLog>(entity =>
            {
                entity.ToTable("WCFPlusLog");

                entity.Property(e => e.CalledAt).HasColumnType("datetime");

                entity.Property(e => e.CalledBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.MethodName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Parameters)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Withdrawals>(entity =>
            {
                entity.Property(e => e.AmountDue).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.DueDate).HasColumnType("datetime");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<WorkflowCode>(entity =>
            {
                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Condition)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.MethodName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Workflow)
                    .WithMany(p => p.WorkflowCode)
                    .HasForeignKey(d => d.WorkflowId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("WorkflowVersion_WorkflowCode");
            });

            modelBuilder.Entity<WorkflowError>(entity =>
            {
                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<WorkflowVersion>(entity =>
            {
                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Xmlaffiliate>(entity =>
            {
                entity.ToTable("XMLAffiliate");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Xmlapplication>(entity =>
            {
                entity.ToTable("XMLApplication");

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Input)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.LeadCode)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
