﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class ActionDiary
    {
        public int ActionDiaryId { get; set; }
        public int LoanId { get; set; }
        public DateTime ActionDiaryDate { get; set; }
        public string ActionDiaryText { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime? CompletedAt { get; set; }
        public string CompletedBy { get; set; }
        public string CompletedComment { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
