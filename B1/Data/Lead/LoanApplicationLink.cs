﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationLink
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public string LoanNumber { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
