﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteDiaryEntry
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public string Type { get; set; }
        public DateTime DueAt { get; set; }
        public string Comment { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime? CompletedAt { get; set; }
        public string CompletedBy { get; set; }
        public string CompletedComment { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
