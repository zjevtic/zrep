﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Actions
    {
        public int ActionId { get; set; }
        public int LoanId { get; set; }
        public DateTime? ActionDate { get; set; }
        public string Action { get; set; }
        public double? ActionCharge { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
