﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Attachment
    {
        public long Id { get; set; }
        public string Loan { get; set; }
        public string AttachedBy { get; set; }
        public DateTime AttachedAt { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
        public string Comment { get; set; }
    }
}
