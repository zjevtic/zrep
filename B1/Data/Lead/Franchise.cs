﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Franchise
    {
        public Franchise()
        {
            Lead = new HashSet<Lead>();
            Quote = new HashSet<Quote>();
            Underwriter = new HashSet<Underwriter>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string CorporateName { get; set; }
        public string Code { get; set; }
        public string LoanCriteria { get; set; }

        public virtual ICollection<Lead> Lead { get; set; }
        public virtual ICollection<Quote> Quote { get; set; }
        public virtual ICollection<Underwriter> Underwriter { get; set; }
    }
}
