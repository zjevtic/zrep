﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteAuthorisationNote
    {
        public long Id { get; set; }
        public long QuoteAuthorisationId { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public int Decision { get; set; }
        public string Comment { get; set; }
        public string AdditionalRequirements { get; set; }

        public virtual QuoteAuthorisation QuoteAuthorisation { get; set; }
    }
}
