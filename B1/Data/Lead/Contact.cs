﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Contact
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public string Type { get; set; }
        public bool Inactive { get; set; }
        public string Value { get; set; }
        public string Reason { get; set; }
        public int SignatoryNumber { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
