﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class TmpGdprImport2
    {
        public double? Olaid { get; set; }
        public bool? Email { get; set; }
        public bool? Sms { get; set; }
        public bool? Phone { get; set; }
    }
}
