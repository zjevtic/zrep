﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class MatrixUsage
    {
        public long Id { get; set; }
        public string AdSource { get; set; }
        public long? ArrangingUwmatrixTypeId { get; set; }
        public long SigningUwmatrixTypeId { get; set; }

        public virtual MatrixType ArrangingUwmatrixType { get; set; }
        public virtual MatrixType SigningUwmatrixType { get; set; }
    }
}
