﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanTerm
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int DurationInWeeks { get; set; }
    }
}
