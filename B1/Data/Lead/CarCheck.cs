﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class CarCheck
    {
        public long Id { get; set; }
        public int Type { get; set; }
        public string RegNo { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public bool IsAutomatic { get; set; }
        public bool IsPassed { get; set; }
        public decimal? DamageDiscount { get; set; }
        public int? MileageOnMot { get; set; }
        public int? Mileage { get; set; }
        public decimal? Value { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Colour { get; set; }
        public string ChassisNumber { get; set; }
        public DateTime? Registered { get; set; }
        public string Spec { get; set; }
    }
}
