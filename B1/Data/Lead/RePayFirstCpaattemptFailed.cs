﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class RePayFirstCpaattemptFailed
    {
        public long Id { get; set; }
        public int LoanId { get; set; }
        public decimal Amount { get; set; }
        public int Status { get; set; }
        public DateTime Date { get; set; }
        public long? ArrangementPlanId { get; set; }
        public string GeneratedBy { get; set; }
    }
}
