﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanTaggingActions
    {
        public int Id { get; set; }
        public string TagName { get; set; }
        public byte[] ActionDetains { get; set; }
        public DateTime DateApplied { get; set; }
        public string AppliedBy { get; set; }
        public int LoanId { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
