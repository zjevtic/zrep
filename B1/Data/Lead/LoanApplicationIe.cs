﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationIe
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public string Group { get; set; }
        public string Item { get; set; }
        public int Sign { get; set; }
        public decimal Value { get; set; }
        public string Comment { get; set; }
        public bool Evidence { get; set; }
        public int Period { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
