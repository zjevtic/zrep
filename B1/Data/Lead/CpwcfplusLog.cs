﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class CpwcfplusLog
    {
        public long Id { get; set; }
        public string MethodName { get; set; }
        public DateTime CalledAt { get; set; }
        public string CalledBy { get; set; }
        public Guid? SessionId { get; set; }
        public decimal CallDuration { get; set; }
    }
}
