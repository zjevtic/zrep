﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class PaymentImportLoan
    {
        public int LoanId { get; set; }
        public string AgreementNo { get; set; }
        public decimal WeeklyAmount { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string NwNumber { get; set; }
        public int? NwnumberSlips { get; set; }
        public int? NwlastNumber { get; set; }
        public int? NwnumberSecond { get; set; }
        public string Stonumber { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public int? State { get; set; }
        public string RegNo { get; set; }
    }
}
