﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class CustomerCheck
    {
        public CustomerCheck()
        {
            CustomerCheckNote = new HashSet<CustomerCheckNote>();
        }

        public long Id { get; set; }
        public string Number { get; set; }
        public string CustomerName { get; set; }
        public string CustomerSurname { get; set; }
        public DateTime? CustomerDateOfBirth { get; set; }
        public string CustomerAddressLine1 { get; set; }
        public string CustomerAddressLine2 { get; set; }
        public string CustomerAddressLine3 { get; set; }
        public string CustomerTown { get; set; }
        public string CustomerCounty { get; set; }
        public string CustomerPostcode { get; set; }
        public bool ResultsObtained { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public string Data { get; set; }
        public int Signatory { get; set; }
        public string Message { get; set; }
        public int Type { get; set; }
        public string CustomerTitle { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerLandline { get; set; }
        public string PreviousAddresses { get; set; }

        public virtual ICollection<CustomerCheckNote> CustomerCheckNote { get; set; }
    }
}
