﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationHistory
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public int Event { get; set; }
        public string Comment { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
