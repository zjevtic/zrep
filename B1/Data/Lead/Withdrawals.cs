﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Withdrawals
    {
        public int Id { get; set; }
        public int LoanId { get; set; }
        public DateTime DueDate { get; set; }
        public decimal AmountDue { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
    }
}
