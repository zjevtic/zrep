﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class EntityUpdatedColumnLog
    {
        public long Id { get; set; }
        public string EntityType { get; set; }
        public long EntityId { get; set; }
        public string EntityColumn { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
    }
}
