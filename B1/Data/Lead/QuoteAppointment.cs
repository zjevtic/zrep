﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteAppointment
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public DateTime BookedAt { get; set; }
        public string BookedBy { get; set; }
        public int Status { get; set; }
        public DateTime AppointmentAt { get; set; }
        public DateTime? CourtesyCallMadeAt { get; set; }
        public string Reason { get; set; }
        public string Comment { get; set; }
        public DateTime StatusUpdatedAt { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
