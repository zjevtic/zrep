﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Xmlaffiliate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool IsOwnWebsite { get; set; }
        public bool IsDeDupe { get; set; }
    }
}
