﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Insurer
    {
        public Insurer()
        {
            Lead = new HashSet<Lead>();
            Quote = new HashSet<Quote>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool Archived { get; set; }

        public virtual ICollection<Lead> Lead { get; set; }
        public virtual ICollection<Quote> Quote { get; set; }
    }
}
