﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteLink
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public int SignatoryNumber { get; set; }
        public string LoanNumber { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
