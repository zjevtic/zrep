﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Cars
    {
        public Cars()
        {
            LoanDetails = new HashSet<LoanDetails>();
        }

        public int CarId { get; set; }
        public int CustomerId { get; set; }
        public string RegNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Specification { get; set; }
        public string ChassisNo { get; set; }
        public string Colour { get; set; }
        public DateTime? FirstRegistered { get; set; }
        public int? Mileage { get; set; }
        public DateTime? MotDate { get; set; }
        public decimal? TradePrice { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? TaxExpiryDate { get; set; }
        public DateTime? Anpr { get; set; }
        public string V5serialNumber { get; set; }
        public string V5docRefNo { get; set; }
        public DateTime? V5docIssueDate { get; set; }
        public int VehicleType { get; set; }
        public bool? V5inCustomerName { get; set; }
        public int? MileageOnMot { get; set; }
        public string MottestNumber { get; set; }

        public virtual Customers Customer { get; set; }
        public virtual ICollection<LoanDetails> LoanDetails { get; set; }
    }
}
