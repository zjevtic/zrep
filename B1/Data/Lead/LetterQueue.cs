﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LetterQueue
    {
        public int LetterId { get; set; }
        public string DocumentType { get; set; }
        public DateTime DateCreated { get; set; }
        public byte[] DocumentReference { get; set; }
        public string RequestedBy { get; set; }
        public int? RelatedObjectId { get; set; }
        public byte[] DocumentData { get; set; }
        public DateTime? LetterRunDate { get; set; }
        public int Generated { get; set; }
    }
}
