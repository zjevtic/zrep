﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Campaign
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int LoanId { get; set; }
        public string AgreementNumber { get; set; }
        public DateTime Date { get; set; }
        public int State { get; set; }
        public decimal TotalPayable { get; set; }
        public decimal RegularArrears { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalCharges { get; set; }
        public DateTime? LatestPaymentDate { get; set; }
        public DateTime? LatestNoteDate { get; set; }
        public DateTime? LatestSpokeToCustomerDate { get; set; }
        public DateTime FirstInstallmentDate { get; set; }
        public decimal Installment { get; set; }
        public int NumberOfPmtCmtFailed { get; set; }
        public DateTime DateSigned { get; set; }
        public decimal PaymentsMade { get; set; }
        public int NumberOfPmtCmtHonoured { get; set; }
        public string CustomerName { get; set; }
        public string CustomerSurname { get; set; }
        public string PmtCmtType { get; set; }
        public DateTime? PmtCmtDueAt { get; set; }
        public decimal? PmtCmtAmountDue { get; set; }
        public DateTime? AssignedAt { get; set; }
        public DateTime? TaskDueAt { get; set; }
        public string TaskComment { get; set; }
        public string TaskGeneratedBy { get; set; }
        public DateTime? TaskGeneratedAt { get; set; }
        public DateTime LatestStateChangeDate { get; set; }
        public DateTime LastWorkedDate { get; set; }
        public decimal Balance { get; set; }
        public DateTime? CommitmentDate { get; set; }
        public decimal? CommitmentAmount { get; set; }
        public int DaysPastDue { get; set; }
        public DateTime? CompletedAt { get; set; }
        public string CompletedBy { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
