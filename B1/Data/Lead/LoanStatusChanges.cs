﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanStatusChanges
    {
        public int Id { get; set; }
        public int LoanId { get; set; }
        public string OriginalStatus { get; set; }
        public string NewStatus { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
