﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class AccountNumber
    {
        public long Id { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
    }
}
