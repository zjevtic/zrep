﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class TmpTbl
    {
        public long Id { get; set; }
        public int? Data { get; set; }
    }
}
