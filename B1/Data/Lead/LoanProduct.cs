﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanProduct
    {
        public LoanProduct()
        {
            UnderwriterLoanProduct = new HashSet<UnderwriterLoanProduct>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsAvailableInEw { get; set; }
        public bool IsAvailableInS { get; set; }
        public int MinTermInMonths { get; set; }
        public int MaxTermInMonths { get; set; }
        public int AuthTerm { get; set; }
        public bool IsSecuredOnVehicle { get; set; }
        public bool IsForHomeowners { get; set; }
        public bool IsBalloonRepayment { get; set; }
        public bool Archived { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
        public bool IsBusinessLoan { get; set; }
        public string DmsdocumentName { get; set; }
        public decimal AuthThreshold { get; set; }

        public virtual ICollection<UnderwriterLoanProduct> UnderwriterLoanProduct { get; set; }
    }
}
