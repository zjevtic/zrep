﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class WorkflowError
    {
        public long Id { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public string Message { get; set; }
    }
}
