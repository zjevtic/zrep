﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationMiscDetail
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
