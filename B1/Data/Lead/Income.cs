﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Income
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public string Customer { get; set; }
        public string Type { get; set; }
        public string Frequency { get; set; }
        public string PayDate { get; set; }
        public string PayMethod { get; set; }
        public DateTime? StartDate { get; set; }
        public string EmployerName { get; set; }
        public string EmployerAddress { get; set; }
        public string EmployerPhone { get; set; }
        public bool IsMain { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
