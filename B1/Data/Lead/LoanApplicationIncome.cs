﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationIncome
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public int Source { get; set; }
        public int Frequency { get; set; }
        public int PaymentType { get; set; }
        public decimal Amount { get; set; }
        public string EmploymentJobTitle { get; set; }
        public string EmploymentEmployerName { get; set; }
        public string EmploymentAddressLine1 { get; set; }
        public string EmploymentAddressLine2 { get; set; }
        public string EmploymentAddressLine3 { get; set; }
        public string EmploymentAddressTown { get; set; }
        public string EmploymentAddressCounty { get; set; }
        public string EmploymentAddressPostcode { get; set; }
        public string EmploymentAddressCountry { get; set; }
        public DateTime? EmploymentStartDate { get; set; }
        public DateTime? EmploymentLastPayDate { get; set; }
        public DateTime? EmploymentNextPayDate { get; set; }
        public int? EmploymentStatus { get; set; }
        public string BenefitType { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
