﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Lead
    {
        public Lead()
        {
            LeadDistribution = new HashSet<LeadDistribution>();
            LeadSource = new HashSet<LeadSource>();
            LeadUpdate = new HashSet<LeadUpdate>();
        }

        public long Id { get; set; }
        public string Code { get; set; }
        public int System { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Mobile { get; set; }
        public string Landline { get; set; }
        public string WorkPhone { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string RegNo { get; set; }
        public int? Mileage { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Colour { get; set; }
        public decimal? Value { get; set; }
        public string Registered { get; set; }
        public bool? FreeOfFinance { get; set; }
        public bool? Taxed { get; set; }
        public bool? Insured { get; set; }
        public bool? Motd { get; set; }
        public bool? V5inCustomerName { get; set; }
        public long? InsurerId { get; set; }
        public string HomeownerStatus { get; set; }
        public string Advice { get; set; }
        public string InsuranceType { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string ValuationInfo { get; set; }
        public long? ArrangingUnderwriterId { get; set; }
        public long? SigningUnderwriterId { get; set; }
        public long? FranchiseId { get; set; }
        public bool DeadCallCentreLead { get; set; }
        public decimal? InitialApplicationAmount { get; set; }
        public bool? MarketingOptOut { get; set; }
        public string ChassisNumber { get; set; }
        public long? LoanSystemLoanId { get; set; }
        public int CallAttempt { get; set; }
        public string Action { get; set; }
        public bool Important { get; set; }
        public DateTime? ContactAgainAt { get; set; }
        public DateTime? AssignedAt { get; set; }
        public int MarketingCounter { get; set; }
        public DateTime? MarketingLastSmssend { get; set; }

        public virtual Underwriter ArrangingUnderwriter { get; set; }
        public virtual Franchise Franchise { get; set; }
        public virtual Insurer Insurer { get; set; }
        public virtual Underwriter SigningUnderwriter { get; set; }
        public virtual ICollection<LeadDistribution> LeadDistribution { get; set; }
        public virtual ICollection<LeadSource> LeadSource { get; set; }
        public virtual ICollection<LeadUpdate> LeadUpdate { get; set; }
    }
}
