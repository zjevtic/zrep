﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanContactTime
    {
        public long Id { get; set; }
        public int LoanId { get; set; }
        public decimal Start { get; set; }
        public decimal Finish { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
