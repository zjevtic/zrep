﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteAddress
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public bool Signatory1 { get; set; }
        public bool Signatory2 { get; set; }
        public bool IsCurrent { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
