﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanSale
    {
        public long Id { get; set; }
        public int LoanId { get; set; }
        public long DebtBuyerId { get; set; }
        public DateTime AccountSoldAt { get; set; }

        public virtual DebtBuyers DebtBuyer { get; set; }
        public virtual LoanDetails Loan { get; set; }
    }
}
