﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class PaymentInputTemp
    {
        public Guid PaymentId { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PaymentType { get; set; }
        public decimal? Payment { get; set; }
        public string Detail1 { get; set; }
        public string Detail2 { get; set; }
        public string Detail3 { get; set; }
        public string Detail4 { get; set; }
        public string Detail5 { get; set; }
        public int? LoanId { get; set; }
        public int? AgreementNumber { get; set; }
        public string CustomerName { get; set; }
        public int? PaymentNumber { get; set; }
        public string Stonumber { get; set; }
        public string NwnumberRange { get; set; }
        public int? Status { get; set; }
        public string Database { get; set; }
        public bool IsDuplicate { get; set; }
        public DateTime? ImportDate { get; set; }
        public string StateName { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public bool ManualChange { get; set; }
        public string UserName { get; set; }
        public string Bank { get; set; }
    }
}
