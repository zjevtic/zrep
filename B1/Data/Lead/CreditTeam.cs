﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class CreditTeam
    {
        public CreditTeam()
        {
            LoanApplication = new HashSet<LoanApplication>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool Archived { get; set; }
        public string Username { get; set; }
        public bool IsAdmin { get; set; }

        public virtual ICollection<LoanApplication> LoanApplication { get; set; }
    }
}
