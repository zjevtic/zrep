﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteScore
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public string Model { get; set; }
        public decimal Score { get; set; }
        public decimal MaxLoanAmount { get; set; }
        public int Decision { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
