﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class RePayFirstCpaattempt
    {
        public long Id { get; set; }
        public int LoanId { get; set; }
        public DateTime FirstCpaattemptPaymentDate { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
