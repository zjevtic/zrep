﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class CompanyDetail
    {
        public long Id { get; set; }
        public string FieldName { get; set; }
        public string ConditionFieldName { get; set; }
        public string ConditionFieldValue { get; set; }
        public string Value { get; set; }
    }
}
