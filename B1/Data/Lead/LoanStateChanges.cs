﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanStateChanges
    {
        public Guid RecordGuid { get; set; }
        public int LoanId { get; set; }
        public string OriginalState { get; set; }
        public string NewState { get; set; }
        public DateTime DateOfChange { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
