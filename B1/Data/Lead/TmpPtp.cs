﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class TmpPtp
    {
        public double? Id { get; set; }
        public string Agr { get; set; }
        public DateTime? Dueat { get; set; }
        public decimal? Amount { get; set; }
    }
}
