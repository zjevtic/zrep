﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanSecondSignatory
    {
        public int LoanId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string PostalCode { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string HomeOwner { get; set; }
        public bool Optout { get; set; }
        public bool SameAddress { get; set; }
        public bool UseForCorrAddress { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public DateTime? DateMoved { get; set; }
        public string Ninumber { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
