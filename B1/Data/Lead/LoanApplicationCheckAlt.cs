﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationCheckAlt
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public int Type { get; set; }
        public int Decision { get; set; }
        public byte[] Data { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
