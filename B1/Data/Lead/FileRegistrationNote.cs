﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class FileRegistrationNote
    {
        public long Id { get; set; }
        public long FileRegistrationId { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public string Comment { get; set; }

        public virtual FileRegistration FileRegistration { get; set; }
    }
}
