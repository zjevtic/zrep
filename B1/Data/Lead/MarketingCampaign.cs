﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class MarketingCampaign
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
        public string Mobile { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
    }
}
