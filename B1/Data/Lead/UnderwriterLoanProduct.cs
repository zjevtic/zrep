﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class UnderwriterLoanProduct
    {
        public long Id { get; set; }
        public long UnderwriterId { get; set; }
        public long LoanProductId { get; set; }

        public virtual LoanProduct LoanProduct { get; set; }
        public virtual Underwriter Underwriter { get; set; }
    }
}
