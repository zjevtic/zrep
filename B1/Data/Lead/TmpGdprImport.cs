﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class TmpGdprImport
    {
        public double? Olaid { get; set; }
        public bool? Email { get; set; }
        public bool? Sms { get; set; }
        public bool? Phone { get; set; }
        public string EmailValue { get; set; }
        public string Smsvalue { get; set; }
        public string PhoneValue { get; set; }
        public DateTime? GeneratedAt { get; set; }
    }
}
