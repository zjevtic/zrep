﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class DebtBuyers
    {
        public DebtBuyers()
        {
            LoanSale = new HashSet<LoanSale>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsShareMember { get; set; }
        public bool IsInsightMember { get; set; }

        public virtual ICollection<LoanSale> LoanSale { get; set; }
    }
}
