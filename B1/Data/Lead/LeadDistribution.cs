﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LeadDistribution
    {
        public long Id { get; set; }
        public long UnderwriterId { get; set; }
        public long LeadId { get; set; }
        public DateTime GeneratedAt { get; set; }
        public int Event { get; set; }
        public string GeneratedBy { get; set; }
        public string Comment { get; set; }

        public virtual Lead Lead { get; set; }
        public virtual Underwriter Underwriter { get; set; }
    }
}
