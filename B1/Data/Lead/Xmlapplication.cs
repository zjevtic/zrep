﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Xmlapplication
    {
        public long Id { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string Input { get; set; }
        public string LeadCode { get; set; }
        public string Message { get; set; }
        public bool Valid { get; set; }
        public bool Accepted { get; set; }
    }
}
