﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationPredictor
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
