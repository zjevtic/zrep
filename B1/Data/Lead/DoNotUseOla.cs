﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class DoNotUseOla
    {
        public long Id { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
