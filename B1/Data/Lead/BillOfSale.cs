﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class BillOfSale
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public long Number { get; set; }
        public decimal Amount { get; set; }
        public long ChequeUnderwriterId { get; set; }
        public DateTime ChequeIssuedAt { get; set; }

        public virtual Underwriter ChequeUnderwriter { get; set; }
        public virtual Quote Quote { get; set; }
    }
}
