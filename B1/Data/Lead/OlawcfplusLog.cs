﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class OlawcfplusLog
    {
        public long Id { get; set; }
        public string ServiceName { get; set; }
        public string MethodName { get; set; }
        public DateTime CalledAt { get; set; }
        public string CalledBy { get; set; }
        public string Parameters { get; set; }
        public string Host { get; set; }
    }
}
