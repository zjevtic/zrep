﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteHistory
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public int Event { get; set; }
        public string Comment { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
