﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationZva
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public string Mask { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string TokenId { get; set; }
        public string Authcode { get; set; }
        public string ResultCode { get; set; }
        public string TransId { get; set; }
        public string AvsaddressMatch { get; set; }
        public string AvspostcodeMatch { get; set; }
        public string Cvnmatch { get; set; }
        public string Srd { get; set; }
        public int ProcessedBy { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
