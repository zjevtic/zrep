﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class TemporaryUnderwriter
    {
        public long Id { get; set; }
        public long ReplacedUnderwriterId { get; set; }
        public long ActingUnderwriterId { get; set; }

        public virtual Underwriter ActingUnderwriter { get; set; }
        public virtual Underwriter ReplacedUnderwriter { get; set; }
    }
}
