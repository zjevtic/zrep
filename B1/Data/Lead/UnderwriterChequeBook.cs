﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class UnderwriterChequeBook
    {
        public long Id { get; set; }
        public long UnderwriterId { get; set; }
        public long RangeStart { get; set; }
        public long RangeFinish { get; set; }

        public virtual Underwriter Underwriter { get; set; }
    }
}
