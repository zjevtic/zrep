﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class RepossessionCosts
    {
        public int RcostId { get; set; }
        public int? LoanId { get; set; }
        public double? Amount { get; set; }
        public DateTime? Date { get; set; }
        public string Title { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
