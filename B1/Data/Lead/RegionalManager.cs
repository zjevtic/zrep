﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class RegionalManager
    {
        public RegionalManager()
        {
            Underwriter = new HashSet<Underwriter>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Underwriter> Underwriter { get; set; }
    }
}
