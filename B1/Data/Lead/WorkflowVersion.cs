﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class WorkflowVersion
    {
        public WorkflowVersion()
        {
            WorkflowCode = new HashSet<WorkflowCode>();
        }

        public long Id { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }

        public virtual ICollection<WorkflowCode> WorkflowCode { get; set; }
    }
}
