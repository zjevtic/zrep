﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanGuarantor
    {
        public int LoanId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string PostalCode { get; set; }
        public string PhoneDaytime { get; set; }
        public string PhoneEvening { get; set; }
        public string PhoneMobile { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Email { get; set; }
        public string HomeOwner { get; set; }
        public bool Optout { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
