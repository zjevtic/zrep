﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class CustomerCheckNote
    {
        public long Id { get; set; }
        public long CustomerCheckId { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public int Outcome { get; set; }
        public string Category { get; set; }
        public string Concern { get; set; }
        public string Details { get; set; }

        public virtual CustomerCheck CustomerCheck { get; set; }
    }
}
