﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteFunding
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public DateTime FundedAt { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
