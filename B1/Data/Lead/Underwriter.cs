﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Underwriter
    {
        public Underwriter()
        {
            ArrangingTeamPool = new HashSet<ArrangingTeamPool>();
            BillOfSale = new HashSet<BillOfSale>();
            LeadArrangingUnderwriter = new HashSet<Lead>();
            LeadDistribution = new HashSet<LeadDistribution>();
            LeadSigningUnderwriter = new HashSet<Lead>();
            Matrix = new HashSet<Matrix>();
            QuoteArrangingUnderwriter = new HashSet<Quote>();
            QuoteSigningUnderwriter = new HashSet<Quote>();
            TemporaryUnderwriterActingUnderwriter = new HashSet<TemporaryUnderwriter>();
            TemporaryUnderwriterReplacedUnderwriter = new HashSet<TemporaryUnderwriter>();
            UnderwriterChequeBook = new HashSet<UnderwriterChequeBook>();
            UnderwriterLoanProduct = new HashSet<UnderwriterLoanProduct>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Landline { get; set; }
        public string Email { get; set; }
        public long FranchiseId { get; set; }
        public bool Archived { get; set; }
        public string Username { get; set; }
        public long RegionalManagerId { get; set; }
        public string SolicitorCompany { get; set; }
        public string SolicitorAddress { get; set; }
        public long EmployeeNumber { get; set; }
        public bool Arranging { get; set; }

        public virtual Franchise Franchise { get; set; }
        public virtual RegionalManager RegionalManager { get; set; }
        public virtual ICollection<ArrangingTeamPool> ArrangingTeamPool { get; set; }
        public virtual ICollection<BillOfSale> BillOfSale { get; set; }
        public virtual ICollection<Lead> LeadArrangingUnderwriter { get; set; }
        public virtual ICollection<LeadDistribution> LeadDistribution { get; set; }
        public virtual ICollection<Lead> LeadSigningUnderwriter { get; set; }
        public virtual ICollection<Matrix> Matrix { get; set; }
        public virtual ICollection<Quote> QuoteArrangingUnderwriter { get; set; }
        public virtual ICollection<Quote> QuoteSigningUnderwriter { get; set; }
        public virtual ICollection<TemporaryUnderwriter> TemporaryUnderwriterActingUnderwriter { get; set; }
        public virtual ICollection<TemporaryUnderwriter> TemporaryUnderwriterReplacedUnderwriter { get; set; }
        public virtual ICollection<UnderwriterChequeBook> UnderwriterChequeBook { get; set; }
        public virtual ICollection<UnderwriterLoanProduct> UnderwriterLoanProduct { get; set; }
    }
}
