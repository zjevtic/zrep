﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Contacts
    {
        public int Id { get; set; }
        public int LoanId { get; set; }
        public string Type { get; set; }
        public bool Inactive { get; set; }
        public string Value { get; set; }
        public string Reason { get; set; }
        public int SignatoryNumber { get; set; }
        public bool PrimaryMobile { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
