﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteAuthorisation
    {
        public QuoteAuthorisation()
        {
            QuoteAuthorisationNote = new HashSet<QuoteAuthorisationNote>();
        }

        public long Id { get; set; }
        public long QuoteId { get; set; }
        public string Reason { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOnBehalf { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }

        public virtual Quote Quote { get; set; }
        public virtual ICollection<QuoteAuthorisationNote> QuoteAuthorisationNote { get; set; }
    }
}
