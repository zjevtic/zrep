﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicationSource
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public int Index { get; set; }
        public string Type { get; set; }
        public long? Code { get; set; }
        public string Name { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
