﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class OutstandingPdfchange
    {
        public long Id { get; set; }
        public string Loan { get; set; }
        public DateTime ChangedAt { get; set; }
    }
}
