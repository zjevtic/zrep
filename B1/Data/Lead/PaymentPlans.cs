﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class PaymentPlans
    {
        public int Id { get; set; }
        public int LoanId { get; set; }
        public DateTime StartAt { get; set; }
        public decimal Amount { get; set; }
        public int Type { get; set; }
        public int? RepaymentPeriod { get; set; }
        public int Duration { get; set; }
        public DateTime FinishAt { get; set; }
        public int State { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime? LastUpdatedAt { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
