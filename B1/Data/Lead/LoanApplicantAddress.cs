﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanApplicantAddress
    {
        public long Id { get; set; }
        public long LoanApplicationId { get; set; }
        public bool Ukresident { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public int Index { get; set; }
        public int TimeAtAddressMonths { get; set; }

        public virtual LoanApplication LoanApplication { get; set; }
    }
}
