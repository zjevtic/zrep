﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanFinancialPlan
    {
        public int LoanId { get; set; }
        public int FullWeeks { get; set; }
        public decimal WeeklyAmount { get; set; }
        public decimal FinalPaymentAmount { get; set; }
        public DateTime FinalPaymentDate { get; set; }
        public DateTime FirstPaymentDate { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
