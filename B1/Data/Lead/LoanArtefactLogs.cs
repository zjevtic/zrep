﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanArtefactLogs
    {
        public long Id { get; set; }
        public int LoanId { get; set; }
        public int Type { get; set; }
        public int OldStatus { get; set; }
        public int NewStatus { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public string Comment { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
