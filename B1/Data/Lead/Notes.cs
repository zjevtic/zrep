﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Notes
    {
        public int NoteId { get; set; }
        public int? LoanId { get; set; }
        public string Text { get; set; }
        public DateTime? Date { get; set; }
        public bool? SpeakWithCustomer { get; set; }
        public bool Important { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
