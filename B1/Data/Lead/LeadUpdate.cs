﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LeadUpdate
    {
        public long Id { get; set; }
        public long LeadId { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime GeneratedAt { get; set; }
        public decimal? LoanAmount { get; set; }
        public string RejectionReason { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public DateTime? ContactAgainAt { get; set; }
        public long? QuoteNumber { get; set; }

        public virtual Lead Lead { get; set; }
    }
}
