﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class LoanDetails
    {
        public LoanDetails()
        {
            ActionDiary = new HashSet<ActionDiary>();
            Actions = new HashSet<Actions>();
            Campaign = new HashSet<Campaign>();
            Contacts = new HashSet<Contacts>();
            FileRegistration = new HashSet<FileRegistration>();
            Interest = new HashSet<Interest>();
            LegalCharges = new HashSet<LegalCharges>();
            LoanArtefactLogs = new HashSet<LoanArtefactLogs>();
            LoanArtefacts = new HashSet<LoanArtefacts>();
            LoanContactTime = new HashSet<LoanContactTime>();
            LoanFundings = new HashSet<LoanFundings>();
            LoanIncomes = new HashSet<LoanIncomes>();
            LoanSale = new HashSet<LoanSale>();
            LoanStateChanges = new HashSet<LoanStateChanges>();
            LoanStatusChanges = new HashSet<LoanStatusChanges>();
            LoanTaggingActions = new HashSet<LoanTaggingActions>();
            LoanTopupsOriginalLoan = new HashSet<LoanTopups>();
            LoanTopupsTopupLoan = new HashSet<LoanTopups>();
            Notes = new HashSet<Notes>();
            PaymentPlans = new HashSet<PaymentPlans>();
            PaymentPromises = new HashSet<PaymentPromises>();
            Payments = new HashSet<Payments>();
            RePayFirstCpaattempt = new HashSet<RePayFirstCpaattempt>();
            RePayPayment = new HashSet<RePayPayment>();
            RepossessionCosts = new HashSet<RepossessionCosts>();
        }

        public int CarId { get; set; }
        public int LoanId { get; set; }
        public DateTime? DateSigned { get; set; }
        public decimal? AmountLoaned { get; set; }
        public double? InterestRate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public string NwNumber { get; set; }
        public string Representative { get; set; }
        public string CustomerOccupation { get; set; }
        public string AdvertisingSource { get; set; }
        public string SettlementType { get; set; }
        public double? Apr { get; set; }
        public int? NwnumberSlips { get; set; }
        public int? NwlastNumber { get; set; }
        public int? NwnumberSecond { get; set; }
        public DateTime? OdlettersStopped { get; set; }
        public string Stonumber { get; set; }
        public DateTime? Nsaprinted { get; set; }
        public string SignedLocation { get; set; }
        public string InsurerName { get; set; }
        public string Representative1 { get; set; }
        public bool? RefusedMarketing { get; set; }
        public bool? BelongsHere { get; set; }
        public bool? IsScotland { get; set; }
        public int? RepaymentPeriod { get; set; }
        public int? LoanProduct { get; set; }
        public bool? IsTransferred { get; set; }
        public string Password { get; set; }
        public string ProductName { get; set; }
        public int DurationInWeeks { get; set; }
        public string InsuranceType { get; set; }
        public string InsurancePolicyNumber { get; set; }
        public decimal? SoldForAtAuction { get; set; }
        public decimal? NetSaleProceeds { get; set; }
        public string Status { get; set; }
        public int? State { get; set; }
        public string Number { get; set; }
        public string CustomerType { get; set; }
        public int? ProfileCode { get; set; }

        public virtual Cars Car { get; set; }
        public virtual LoanFinancialPlan LoanFinancialPlan { get; set; }
        public virtual LoanGuarantor LoanGuarantor { get; set; }
        public virtual LoanSecondSignatory LoanSecondSignatory { get; set; }
        public virtual ICollection<ActionDiary> ActionDiary { get; set; }
        public virtual ICollection<Actions> Actions { get; set; }
        public virtual ICollection<Campaign> Campaign { get; set; }
        public virtual ICollection<Contacts> Contacts { get; set; }
        public virtual ICollection<FileRegistration> FileRegistration { get; set; }
        public virtual ICollection<Interest> Interest { get; set; }
        public virtual ICollection<LegalCharges> LegalCharges { get; set; }
        public virtual ICollection<LoanArtefactLogs> LoanArtefactLogs { get; set; }
        public virtual ICollection<LoanArtefacts> LoanArtefacts { get; set; }
        public virtual ICollection<LoanContactTime> LoanContactTime { get; set; }
        public virtual ICollection<LoanFundings> LoanFundings { get; set; }
        public virtual ICollection<LoanIncomes> LoanIncomes { get; set; }
        public virtual ICollection<LoanSale> LoanSale { get; set; }
        public virtual ICollection<LoanStateChanges> LoanStateChanges { get; set; }
        public virtual ICollection<LoanStatusChanges> LoanStatusChanges { get; set; }
        public virtual ICollection<LoanTaggingActions> LoanTaggingActions { get; set; }
        public virtual ICollection<LoanTopups> LoanTopupsOriginalLoan { get; set; }
        public virtual ICollection<LoanTopups> LoanTopupsTopupLoan { get; set; }
        public virtual ICollection<Notes> Notes { get; set; }
        public virtual ICollection<PaymentPlans> PaymentPlans { get; set; }
        public virtual ICollection<PaymentPromises> PaymentPromises { get; set; }
        public virtual ICollection<Payments> Payments { get; set; }
        public virtual ICollection<RePayFirstCpaattempt> RePayFirstCpaattempt { get; set; }
        public virtual ICollection<RePayPayment> RePayPayment { get; set; }
        public virtual ICollection<RepossessionCosts> RepossessionCosts { get; set; }
    }
}
