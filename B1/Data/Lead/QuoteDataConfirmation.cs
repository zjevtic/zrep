﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class QuoteDataConfirmation
    {
        public long Id { get; set; }
        public long QuoteId { get; set; }
        public string FieldGroup { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
