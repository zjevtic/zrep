﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class FileRegistration
    {
        public FileRegistration()
        {
            FileRegistrationNote = new HashSet<FileRegistrationNote>();
        }

        public long Id { get; set; }
        public DateTime? DateReceivedInHeadOffice { get; set; }
        public bool? V5 { get; set; }
        public bool? SpareKey { get; set; }
        public DateTime? DateRegistrationCompleted { get; set; }
        public bool? EnforceabilityIssue { get; set; }
        public bool? AffordabilityIssue { get; set; }
        public DateTime? BillOfSaleRegDate { get; set; }
        public string BillOfSaleRegNo { get; set; }
        public int? LoanId { get; set; }
        public string SigningAgentName { get; set; }
        public bool? VehiclePhotoAvailable { get; set; }
        public bool? PhotoIdavailable { get; set; }
        public bool? BillOfSaleLateRegistration { get; set; }
        public string BillOfSaleLateRegistrationReason { get; set; }
        public string OtherNotes { get; set; }
        public string Qamember { get; set; }

        public virtual LoanDetails Loan { get; set; }
        public virtual ICollection<FileRegistrationNote> FileRegistrationNote { get; set; }
    }
}
