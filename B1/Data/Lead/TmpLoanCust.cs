﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class TmpLoanCust
    {
        public int? LoanId { get; set; }
        public string Customer { get; set; }
        public int? State { get; set; }
        public DateTime? SettledDate { get; set; }
    }
}
