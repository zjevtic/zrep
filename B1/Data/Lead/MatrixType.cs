﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class MatrixType
    {
        public MatrixType()
        {
            Matrix = new HashSet<Matrix>();
            MatrixUsageArrangingUwmatrixType = new HashSet<MatrixUsage>();
            MatrixUsageSigningUwmatrixType = new HashSet<MatrixUsage>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Matrix> Matrix { get; set; }
        public virtual ICollection<MatrixUsage> MatrixUsageArrangingUwmatrixType { get; set; }
        public virtual ICollection<MatrixUsage> MatrixUsageSigningUwmatrixType { get; set; }
    }
}
