﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Gdprconsent
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool Consent { get; set; }
        public string EntityType { get; set; }
        public long EntityId { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
    }
}
