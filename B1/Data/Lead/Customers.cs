﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Customers
    {
        public Customers()
        {
            Cars = new HashSet<Cars>();
        }

        public int CustomerId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string PostalCode { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string HomeOwner { get; set; }
        public DateTime? DateMoved { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Ninumber { get; set; }

        public virtual ICollection<Cars> Cars { get; set; }
    }
}
