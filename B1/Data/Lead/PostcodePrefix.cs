﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class PostcodePrefix
    {
        public long Id { get; set; }
        public string Prefix { get; set; }
    }
}
