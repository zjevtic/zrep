﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class TmpLetter
    {
        public int? LoanId { get; set; }
        public string DisplayType { get; set; }
        public string Data { get; set; }
        public DateTime? Dte { get; set; }
    }
}
