﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class PartialEarlySettlements
    {
        public int OfferId { get; set; }
        public int LoanId { get; set; }
        public decimal SettlementAmount { get; set; }
        public DateTime OfferDate { get; set; }
        public string CreatedBy { get; set; }
        public bool OfferClosed { get; set; }
    }
}
