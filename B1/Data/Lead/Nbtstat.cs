﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class Nbtstat
    {
        public long Id { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public int Event { get; set; }
        public string Nbtuser { get; set; }
    }
}
