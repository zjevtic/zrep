﻿using System;
using System.Collections.Generic;

namespace B1.Data
{
    public partial class PaymentPromises
    {
        public int Id { get; set; }
        public int LoanId { get; set; }
        public DateTime DueAt { get; set; }
        public decimal Amount { get; set; }
        public int State { get; set; }
        public DateTime GeneratedAt { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime? LastUpdatedAt { get; set; }

        public virtual LoanDetails Loan { get; set; }
    }
}
