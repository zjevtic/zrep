﻿using System;

namespace B1.Data.ViewModels
{
    public class LoanChartViewModel
    {

        public DateTime TimeStamp { get; set; }
        public int LoansCount { get; set; }
        public decimal LoansAmountOut { get; set; }
        public decimal LoansAmountIn { get; internal set; }

        public decimal MA { get; internal set; }
        public decimal MACount { get; internal set; }
    }
    public class LoanTimelineChartViewModel
    {

        public DateTime Timestamp { get; set; }
        public string Source { get; internal set; }
        public decimal LoanSum { get; set; }
        public decimal LoanCount { get; set; }
        public decimal MASum { get; internal set; }
        public decimal MACount { get; internal set; }
        

    }

    public class LoanChartPerSizeViewModel
    {

        public decimal Size { get; set; }
        //public decimal LoansAmount { get; set; }
        public int LoansCount { get; set; }
        public decimal LoansAmountOut { get; internal set; }
        public decimal LoansAmountIn { get; internal set; }
        public string Place { get; internal set; }
        
        public string Source { get; internal set; }

        public decimal? SumComp1 { get; internal set; }
        public decimal? CountComp1 { get; internal set; }
        public decimal? SumComp2 { get; internal set; }
        public decimal? CountComp2 { get; internal set; }

    }
    public class LoanChartPerWeekViewModel
    {
        public int Year { get; set; }
        public int Week { get; set; }
        //public decimal LoansAmount { get; set; }
        public DateTime TimeStamp { get; set; }
        public int LoansCount { get; set; }
        public decimal LoansAmountOut { get; internal set; }
        public decimal LoansAmountIn { get; internal set; }
    }

}