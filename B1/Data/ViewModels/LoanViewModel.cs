﻿using System;
using System.Globalization;

namespace B1.Data.ViewModels
{
    public class LoanViewModel
    {
        
        public LoanViewModel()
        {
        }

        public int LoanId { get; set; }
        public int LoanProduct { get; set; }
        public string ProductName { get; set; }
        public int State { get; set; }
        public DateTime DateActivated { get; set; }
        public decimal AmountLoaned { get; set; }
        public double InterestRate { get; set; }
        public string Colour { get; set; }
        public string Make { get; set; }

        public string Model { get; set; }
        public string LastName { get; set; }

        
        public string FirstName {get; set; }

        public decimal? PaySum { get; internal set; }
        public int PayCount { get; internal set; }
        public decimal TotalPayabale => this.AmountLoaned + this.AmountLoaned * this.Duration * Convert.ToDecimal(this.InterestRate);
        public decimal Duration { get; internal set; }
        public decimal? CarValue { get; internal set; }
        public string Postcode { get; internal set; }
        public DateTime? CarYear { get; internal set; }
    }
}