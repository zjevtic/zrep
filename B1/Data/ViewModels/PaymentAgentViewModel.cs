﻿using System;

namespace B1.Data.Services
{
    public class PaymentAgentViewModel
    {
        public DateTime TimeStamp { get; set; }
        public string Agent { get; set; }
        
        public int Count { get; set; }
        public decimal Sum { get; set; }
    }
}