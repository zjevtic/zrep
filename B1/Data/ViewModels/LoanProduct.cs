﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace B1.Data.ViewModels
{
    public class LoanType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
