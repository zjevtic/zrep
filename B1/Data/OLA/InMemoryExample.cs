﻿using System;
using System.Collections.Generic;

namespace B1.Data.OLA
{
    public partial class InMemoryExample
    {
        public int OrderId { get; set; }
        public int ItemNumber { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
