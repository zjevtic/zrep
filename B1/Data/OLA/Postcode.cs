﻿using System;
using System.Collections.Generic;

namespace B1.Data.OLA
{
    public partial class Postcode
    {
        public int Id { get; set; }
        public int? Number { get; set; }
        public string P1 { get; set; }
        public string Postcode1 { get; set; }
        public string Area { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
    }
}
