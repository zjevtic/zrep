﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class UserDepartment
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Department { get; set; }
    }
}
