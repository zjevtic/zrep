﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class PredefinedBinrange
    {
        public long Id { get; set; }
        public decimal Start { get; set; }
        public decimal Finish { get; set; }
        public int ActionType { get; set; }
    }
}
