﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class Timeslot
    {
        public Timeslot()
        {
            Bank = new HashSet<Bank>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Bank> Bank { get; set; }
    }
}
