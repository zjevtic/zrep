﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class ArrangementHistory
    {
        public long Id { get; set; }
        public long ArrangementId { get; set; }
        public int OldStatus { get; set; }
        public int NewStatus { get; set; }
        public DateTime Date { get; set; }
        public string User { get; set; }

        public virtual Arrangement Arrangement { get; set; }
    }
}
