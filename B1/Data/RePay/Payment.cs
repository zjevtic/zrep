﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class Payment
    {
        public long Id { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public int Status { get; set; }
        public decimal Amount { get; set; }
        public string OrderId { get; set; }
        public string TransactionId { get; set; }
        public string AuthCode { get; set; }
        public DateTime Date { get; set; }
        public long? ArrangementPlanId { get; set; }
        public string Message { get; set; }
        public string User { get; set; }
        public long? CardId { get; set; }
        public string Srd { get; set; }
        public int ProcessedBy { get; set; }

        public virtual ArrangementPlan ArrangementPlan { get; set; }
        public virtual StoredCard Card { get; set; }
    }
}
