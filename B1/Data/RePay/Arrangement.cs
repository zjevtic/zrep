﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class Arrangement
    {
        public Arrangement()
        {
            ArrangementHistory = new HashSet<ArrangementHistory>();
            ArrangementLog = new HashSet<ArrangementLog>();
            ArrangementPlan = new HashSet<ArrangementPlan>();
        }

        public long Id { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public int Status { get; set; }
        public string Period { get; set; }
        public long CardId { get; set; }
        public string User { get; set; }
        public int? PayDay { get; set; }

        public virtual StoredCard Card { get; set; }
        public virtual ICollection<ArrangementHistory> ArrangementHistory { get; set; }
        public virtual ICollection<ArrangementLog> ArrangementLog { get; set; }
        public virtual ICollection<ArrangementPlan> ArrangementPlan { get; set; }
    }
}
