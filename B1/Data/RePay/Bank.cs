﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class Bank
    {
        public Bank()
        {
            Innrange = new HashSet<Innrange>();
            StoredCard = new HashSet<StoredCard>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public long TimeslotId { get; set; }

        public virtual Timeslot Timeslot { get; set; }
        public virtual ICollection<Innrange> Innrange { get; set; }
        public virtual ICollection<StoredCard> StoredCard { get; set; }
    }
}
