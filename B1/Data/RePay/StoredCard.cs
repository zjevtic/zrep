﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class StoredCard
    {
        public StoredCard()
        {
            Arrangement = new HashSet<Arrangement>();
            Payment = new HashSet<Payment>();
        }

        public long Id { get; set; }
        public string CustomerId { get; set; }
        public byte[] Code { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public long BankId { get; set; }
        public DateTime PprdateOfBirth { get; set; }
        public string Pprsurname { get; set; }
        public string Pprpostcode { get; set; }
        public string PpraccountNumber { get; set; }
        public string TokenId { get; set; }
        public string Mask { get; set; }
        public bool IsHidden { get; set; }
        public string TransactionId { get; set; }
        public string Srd { get; set; }
        public int ProcessedBy { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual ICollection<Arrangement> Arrangement { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
    }
}
