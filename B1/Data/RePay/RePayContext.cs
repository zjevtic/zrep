﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace B1.Data.RePay
{
    public partial class RePayContext : DbContext
    {
        public RePayContext()
        {
            this.Database.ExecuteSqlRaw("SET TRANSACTION ISOLATION LEVEL READ COMMITTED");
        }

        public RePayContext(DbContextOptions<RePayContext> options)
            : base(options)
        {
            this.Database.ExecuteSqlRaw("SET TRANSACTION ISOLATION LEVEL READ COMMITTED");
        }

        public virtual DbSet<Arrangement> Arrangement { get; set; }
        public virtual DbSet<ArrangementHistory> ArrangementHistory { get; set; }
        public virtual DbSet<ArrangementLog> ArrangementLog { get; set; }
        public virtual DbSet<ArrangementPlan> ArrangementPlan { get; set; }
        public virtual DbSet<ArrangementPlanStoppage> ArrangementPlanStoppage { get; set; }
        public virtual DbSet<Bank> Bank { get; set; }
        public virtual DbSet<Binrange> Binrange { get; set; }
        public virtual DbSet<Branch> Branch { get; set; }
        public virtual DbSet<EventLog> EventLog { get; set; }
        public virtual DbSet<Innrange> Innrange { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<PredefinedBinrange> PredefinedBinrange { get; set; }
        public virtual DbSet<StoredCard> StoredCard { get; set; }
        public virtual DbSet<Timeslot> Timeslot { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserDepartment> UserDepartment { get; set; }
        public virtual DbSet<WcfplusLog> WcfplusLog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Startup.zConfiguration.GetConnectionString("RePayConnectionPR"), sqlServerOptions => sqlServerOptions.CommandTimeout(3));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Arrangement>(entity =>
            {
                entity.HasIndex(e => e.CardId)
                    .HasName("IX_CardId");

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.CustomerId)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Period)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Card)
                    .WithMany(p => p.Arrangement)
                    .HasForeignKey(d => d.CardId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("StoredCard_Arrangement");
            });

            modelBuilder.Entity<ArrangementHistory>(entity =>
            {
                entity.HasIndex(e => e.ArrangementId)
                    .HasName("IX_ArrangementId");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Arrangement)
                    .WithMany(p => p.ArrangementHistory)
                    .HasForeignKey(d => d.ArrangementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Arrangement_ArrangementHistory");
            });

            modelBuilder.Entity<ArrangementLog>(entity =>
            {
                entity.HasIndex(e => e.ArrangementId)
                    .HasName("IX_ArrangementId");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Arrangement)
                    .WithMany(p => p.ArrangementLog)
                    .HasForeignKey(d => d.ArrangementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Arrangement_ArrangementLog");
            });

            modelBuilder.Entity<ArrangementPlan>(entity =>
            {
                entity.HasIndex(e => e.ArrangementId)
                    .HasName("IX_ArrangementId");

                entity.HasIndex(e => new { e.Id, e.ArrangementId, e.StartDate, e.FinishDate })
                    .HasName("Missing_IXNC_ArrangementPlan_StartDate_FinishDate_A7531");

                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.FinishDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Arrangement)
                    .WithMany(p => p.ArrangementPlan)
                    .HasForeignKey(d => d.ArrangementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Arrangement_ArrangementPlan");
            });

            modelBuilder.Entity<ArrangementPlanStoppage>(entity =>
            {
                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Bank>(entity =>
            {
                entity.HasIndex(e => e.TimeslotId)
                    .HasName("IX_TimeslotId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Timeslot)
                    .WithMany(p => p.Bank)
                    .HasForeignKey(d => d.TimeslotId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Timeslot_Bank");
            });

            modelBuilder.Entity<Binrange>(entity =>
            {
                entity.ToTable("BINRange");

                entity.Property(e => e.Finish).HasColumnType("decimal(20, 0)");

                entity.Property(e => e.Start).HasColumnType("decimal(20, 0)");
            });

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("Branch");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.AreaManager)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.AreaManagerEmail)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Latitude).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Longitude).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Postcode)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.RegionalManager)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.RegionalManagerEmail)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<EventLog>(entity =>
            {
                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.GeneratedAt).HasColumnType("datetime");

                entity.Property(e => e.GeneratedBy)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<Innrange>(entity =>
            {
                entity.ToTable("INNRange");

                entity.HasIndex(e => e.BankId)
                    .HasName("IX_BankId");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.Innrange)
                    .HasForeignKey(d => d.BankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Bank_INNRange");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.HasIndex(e => e.ArrangementPlanId)
                    .HasName("IX_ArrangementPlanId");

                entity.HasIndex(e => e.CardId)
                    .HasName("IX_CardId");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("IX_CustomerId");

                entity.HasIndex(e => e.Date)
                    .HasName("IX_Date");

                entity.HasIndex(e => new { e.AccountId, e.CardId })
                    .HasName("IX_AccountIdCardId");

                entity.HasIndex(e => new { e.ArrangementPlanId, e.Status })
                    .HasName("Missing_IXNC_Payment_Status_6A087");

                entity.HasIndex(e => new { e.Status, e.Date })
                    .HasName("IX_SatusDate");

                entity.HasIndex(e => new { e.AccountId, e.Date, e.ArrangementPlanId })
                    .HasName("Missing_IXNC_Payment_ArrangementPlanId_C0153");

                entity.Property(e => e.AccountId).HasMaxLength(50);

                entity.Property(e => e.Amount).HasColumnType("decimal(29, 4)");

                entity.Property(e => e.AuthCode).HasMaxLength(4000);

                entity.Property(e => e.CustomerId).HasMaxLength(50);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Message).HasMaxLength(4000);

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Srd)
                    .HasColumnName("SRD")
                    .HasMaxLength(100);

                entity.Property(e => e.TransactionId).HasMaxLength(4000);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.HasOne(d => d.ArrangementPlan)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.ArrangementPlanId)
                    .HasConstraintName("ArrangementPlan_Payment");

                entity.HasOne(d => d.Card)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.CardId)
                    .HasConstraintName("StoredCard_Payment");
            });

            modelBuilder.Entity<PredefinedBinrange>(entity =>
            {
                entity.ToTable("PredefinedBINRange");

                entity.HasIndex(e => new { e.Id, e.ActionType, e.Start, e.Finish })
                    .HasName("Missing_IXNC_PredefinedBINRange_Start_Finish_79010");

                entity.Property(e => e.Finish).HasColumnType("decimal(20, 0)");

                entity.Property(e => e.Start).HasColumnType("decimal(20, 0)");
            });

            modelBuilder.Entity<StoredCard>(entity =>
            {
                entity.HasIndex(e => e.BankId)
                    .HasName("IX_BankId");

                entity.Property(e => e.AddressLine1)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.AddressLine2).HasMaxLength(4000);

                entity.Property(e => e.AddressLine3).HasMaxLength(4000);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(8000);

                entity.Property(e => e.County).HasMaxLength(4000);

                entity.Property(e => e.CustomerId)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Mask)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Postcode)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.PpraccountNumber)
                    .IsRequired()
                    .HasColumnName("PPRAccountNumber")
                    .HasMaxLength(4000);

                entity.Property(e => e.PprdateOfBirth)
                    .HasColumnName("PPRDateOfBirth")
                    .HasColumnType("datetime");

                entity.Property(e => e.Pprpostcode)
                    .IsRequired()
                    .HasColumnName("PPRPostcode")
                    .HasMaxLength(4000);

                entity.Property(e => e.Pprsurname)
                    .IsRequired()
                    .HasColumnName("PPRSurname")
                    .HasMaxLength(4000);

                entity.Property(e => e.Srd)
                    .HasColumnName("SRD")
                    .HasMaxLength(100);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.TokenId).HasMaxLength(4000);

                entity.Property(e => e.Town)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.TransactionId).HasMaxLength(100);

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.StoredCard)
                    .HasForeignKey(d => d.BankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Bank_StoredCard");
            });

            modelBuilder.Entity<Timeslot>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("User");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<UserDepartment>(entity =>
            {
                entity.Property(e => e.Department)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<WcfplusLog>(entity =>
            {
                entity.ToTable("WCFPlusLog");

                entity.Property(e => e.CalledAt).HasColumnType("datetime");

                entity.Property(e => e.CalledBy)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.MethodName)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.Parameters)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
