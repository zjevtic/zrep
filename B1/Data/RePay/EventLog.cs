﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class EventLog
    {
        public long Id { get; set; }
        public string Comment { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime GeneratedAt { get; set; }
    }
}
