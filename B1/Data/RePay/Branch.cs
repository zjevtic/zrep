﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class Branch
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string Phone { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Email { get; set; }
        public string AreaManager { get; set; }
        public string AreaManagerEmail { get; set; }
        public string RegionalManager { get; set; }
        public string RegionalManagerEmail { get; set; }
    }
}
