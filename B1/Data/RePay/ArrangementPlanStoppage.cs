﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class ArrangementPlanStoppage
    {
        public long Id { get; set; }
        public long ArrangementPlanId { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime GeneratedAt { get; set; }
    }
}
