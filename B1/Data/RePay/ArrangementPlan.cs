﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class ArrangementPlan
    {
        public ArrangementPlan()
        {
            Payment = new HashSet<Payment>();
        }

        public long Id { get; set; }
        public long ArrangementId { get; set; }
        public int PaymentNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public decimal Amount { get; set; }

        public virtual Arrangement Arrangement { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
    }
}
