﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class WcfplusLog
    {
        public long Id { get; set; }
        public string ServiceName { get; set; }
        public string MethodName { get; set; }
        public DateTime CalledAt { get; set; }
        public string CalledBy { get; set; }
        public string Parameters { get; set; }
    }
}
