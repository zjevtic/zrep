﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class User
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}
