﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class Innrange
    {
        public long Id { get; set; }
        public string Number { get; set; }
        public long BankId { get; set; }

        public virtual Bank Bank { get; set; }
    }
}
