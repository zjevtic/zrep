﻿using System;
using System.Collections.Generic;

namespace B1.Data.RePay
{
    public partial class ArrangementLog
    {
        public long Id { get; set; }
        public long ArrangementId { get; set; }
        public DateTime Date { get; set; }
        public string User { get; set; }
        public string Type { get; set; }
        public string Comment { get; set; }

        public virtual Arrangement Arrangement { get; set; }
    }
}
