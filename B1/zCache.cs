﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace zCache
{
    public interface IMyObjects
    {
        int Id { get; set; }
    }
    public sealed class MyObjects : ObjectsCache
    {
        static readonly MyObjects _instance = new MyObjects();
        public static MyObjects Cache
        {
            get { return _instance; }
        }
        private MyObjects()
        : base()
        {
            // Initialize members here.
        }

    }
    public class ObjectsCache
    {
        private readonly object locker = new object();
        private readonly SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        //private Dictionary<Type, object> _dict = new Dictionary<Type, object>();
        private ConcurrentDictionary<Type, object> _dict = new ConcurrentDictionary<Type, object>();
        private List<Tuple<object, DateTime?>> _expiration = new List<Tuple<object, DateTime?>>();
        private System.Timers.Timer tim = new System.Timers.Timer(60000);
        public ObjectsCache()
        {
            // Initialize members here.
            tim.AutoReset = false;
            tim.Elapsed += Tim_Elapsed;
            tim.Start();
        }
        private void Tim_Elapsed(object sender, ElapsedEventArgs e)
        {
            RemoveExpired();
            tim.Start();
        }
        public void AddObject<T>(T value, DateTime? expiration = null) where T : class
        {
            lock (locker)
            {

                if (!_dict.ContainsKey(typeof(T)))
                    _dict.TryAdd(typeof(T), new List<T>());
                ((List<T>)_dict[value.GetType()]).Add(value);
                if (expiration != null) _expiration.Add(new Tuple<object, DateTime?>(value, expiration));

            }

        }


        public void AddObjects<T>(IEnumerable<T> value, DateTime? expiration = null) where T : class
        {

            foreach (var newItem in value)
            {
                this.AddObject(newItem, expiration);
            }

        }
        public bool RemoveObject<T>(T value) where T : class
        {
            if (_dict.ContainsKey(value.GetType()))
            {
                ((List<T>)_dict[typeof(T)]).Remove(value);
            }
            return true;
        }
        public bool RemoveObjects<T>() where T : class
        {
            object outvalue;
            return _dict.TryRemove(typeof(T), out outvalue);
        }
        private void RemoveExpired()
        {
            semaphoreSlim.Wait();
            DateTime tNow = DateTime.Now;
            var expiredObjects = _expiration.Where(t => t.Item2 < tNow).Select(t => t.Item1).ToList();
            foreach (dynamic expired in expiredObjects)
            {
                RemoveObject(expired);
            }
            _expiration.RemoveAll(t => t.Item2 < tNow);
            semaphoreSlim.Release();
        }
        public T GetObject<T>(int id) where T : class, IMyObjects, new()
        {
            return GetObjects<T>().FirstOrDefault(o => o.Id == id);
        }
        public List<T> GetObjects<T>() where T : class, new()
        {
            if (!_dict.ContainsKey(typeof(T))) _dict.TryAdd(typeof(T), new List<T>());
            return _dict[typeof(T)] as List<T>;
        }
        public List<T> GetObjects<T>(Predicate<T> FindData) where T : class, new()
        {
            if (!_dict.ContainsKey(typeof(T))) _dict.TryAdd(typeof(T), new List<T>());
            return (_dict[typeof(T)] as List<T>).FindAll(FindData);
        }
        public List<T> GetObjects<T>(Predicate<T> FindData, Action IfNoDataFound) where T : class, new()
        {
            semaphoreSlim.Wait();

            if (!_dict.ContainsKey(typeof(T))) _dict.TryAdd(typeof(T), new List<T>());
            var data = (_dict[typeof(T)] as List<T>).FindAll(FindData);
            if (!data.Any())
            {
                IfNoDataFound();
                data = (_dict[typeof(T)] as List<T>).FindAll(FindData);
            }
            semaphoreSlim.Release();
            return data;

        }

        public async Task<List<T>> GetObjectsAsync<T>(Predicate<T> FindData, Func<Task> IfNoDataFound) where T : class, new()
        {
            await semaphoreSlim.WaitAsync();

            if (!_dict.ContainsKey(typeof(T))) _dict.TryAdd(typeof(T), new List<T>());
            var data = (_dict[typeof(T)] as List<T>).FindAll(FindData);
            if (!data.Any())
            {
                await IfNoDataFound();
                data = (_dict[typeof(T)] as List<T>).FindAll(FindData);
            }

            semaphoreSlim.Release();

            return data;

        }
        public void ClearAll()
        {
            _dict.Clear();
        }

    }
    //public sealed class MyObjects
    //{
    // private readonly object locker = new object();
    // static readonly MyObjects _instance = new MyObjects();
    // //private Dictionary<Type, object> _dict = new Dictionary<Type, object>();
    // private ConcurrentDictionary<Type, object> _dict = new ConcurrentDictionary<Type, object>();
    // public static MyObjects Cache
    // {
    // get { return _instance; }
    // }
    // private MyObjects()
    // {
    // // Initialize members here.
    // }
    // public void AddObject<T>(T value) where T : class
    // {
    // lock (locker)
    // {
    // if (!_dict.ContainsKey(typeof(T)))
    // _dict.TryAdd(typeof(T), new List<T>());
    // ((List<T>)_dict[value.GetType()]).Add(value);
    // }
    // }
    // public void AddObjects<T>(IEnumerable<T> value) where T : class
    // {
    // foreach (var newItem in value)
    // {
    // this.AddObject(newItem);
    // }
    // }
    // public bool RemoveObject<T>(T value) where T : class
    // {
    // if (_dict.ContainsKey(typeof(T)))
    // {
    // ((List<T>)_dict[typeof(T)]).Remove(value);
    // }
    // return true;
    // }
    // public bool RemoveObjects<T>() where T : class
    // {
    // object outvalue;
    // return _dict.TryRemove(typeof(T), out outvalue);
    // }
    // public T GetObject<T>(int id) where T : class, IMyObjects, new()
    // {
    // return GetObjects<T>().FirstOrDefault(o => o.Id == id);
    // }
    // public List<T> GetObjects<T>() where T : class, new()
    // {
    // if (!_dict.ContainsKey(typeof(T))) _dict.TryAdd(typeof(T), new List<T>());
    // return _dict[typeof(T)] as List<T>;
    // }
    //}
}
