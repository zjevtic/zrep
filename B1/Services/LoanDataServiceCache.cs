using B1.Data.OLA;
using B1.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using zCache;

namespace B1.Data.Services

{
    public class LoanDataServiceCache
    {

        private DateTime startUpdateTime = DateTime.Now.AddDays(-8);
        private long lastFundedId = 0;
        private DateTime loanApplicationslastUpdateTime;
        private System.Timers.Timer timer;

        public event EventHandler NewLoansAdded;
        public event EventHandler NewLoanApplicationsAdded;

        public LoanDataServiceCache()
        {

            Loans = new List<Loan>();
            LoanApplications = new List<LoanApplication>();

            Task.Run(() =>
            {
                loanApplicationslastUpdateTime = DateTime.Now.AddDays(-8);
                RefreshLoanApplications(loanApplicationslastUpdateTime);


                if (LoanApplications.Any()) RefreshLoans(startUpdateTime, lastFundedId);



                //LoanAvgPerHour = GetLoanAvgPerHour(1);


                timer = new System.Timers.Timer();
                timer.Elapsed += (s, ev) =>
                {
                    if (DateTime.Now.Second == 50) if (LoanApplications.Any()) RefreshLoans(startUpdateTime, lastFundedId);
                    if (DateTime.Now.Second == 5) RefreshLoanApplications(loanApplicationslastUpdateTime);
                };
                timer.Interval = 1000;
                timer.Start();
            });

        }

        public List<LoanChartViewModel> GetLoanAvgPerHour(int firstDay, int lastDay)
        {
            try
            {
                using (var _context = new LeadDbContext())
                {
                    return (from ld in _context.LoanDetails
                            join lsc in
                            (
                            from lx in _context.LoanStateChanges
                            where lx.NewState == "Active"
                            group lx by lx.LoanId into g
                            select new { LoanId = g.Key, DateOfChange = g.Min(o => o.DateOfChange) }
                            )
                            on ld.LoanId equals lsc.LoanId

                            where lsc.DateOfChange >= DateTime.Today.AddDays(-firstDay) && lsc.DateOfChange < DateTime.Today.AddDays(-lastDay)
                            //group ld by new DateTime(lsc.DateOfChange.Year, lsc.DateOfChange.Month, lsc.DateOfChange.Day, lsc.DateOfChange.Hour, 0, 0) into g
                            group ld by lsc.DateOfChange.Hour into g
                            orderby g.Key
                            select new LoanChartViewModel { TimeStamp = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, g.Key, 0, 0), LoansCount = g.Count() / (firstDay - lastDay), LoansAmountOut = g.Sum(o => o.AmountLoaned ?? 0m) / (firstDay - lastDay) }
                      )
                      .AsNoTracking()
                      .ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }


        }
        private void RefreshLoans(DateTime startDate, long fundingId)
        {
            List<Loan> recentLoans;
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;

            var getP1s = MyObjects.Cache.GetObjects<Location>(s => true,
                () =>
                {

                    using (var _context = new OLA2Context())
                    {
                        var dbquery = (from pc in _context.Postcode
                                       group pc by pc.P1 into g
                                       select new Location()
                                       {
                                           P1 = g.Key,
                                           Area = g.Max(p => p.Area),
                                           Region = g.Max(p => p.Region)

                                       }).ToList();

                        MyObjects.Cache.AddObjects(dbquery);
                    }
                });


            try
            {
                using (var _context = new LeadDbContext())
                {

                    recentLoans = (from ld in _context.LoanDetails

                                       //where ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange) > startDate.AddMinutes(-5)
                                   where ld.LoanFundings.Any(f => f.FundedAt > startDate && f.Id > fundingId && f.Type == 1)

                                   select new Loan
                                   {
                                       //StartTime = ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange),
                                       StartTime = ld.LoanFundings.FirstOrDefault().FundedAt,

                                       Size = ld.AmountLoaned ?? 0m,
                                       Postcode = ld.Car.Customer.PostalCode.ToUpper().RemoveWhitespace(),
                                       Source = "Loans 2 Go", //_context.LoanApplication.FirstOrDefault(la => la.Number == ld.Number).LoanApplicationSource.FirstOrDefault(las => las.Index == 1).Name ?? "Other",
                                       Source2 = "Loans 2 Go", //_context.LoanApplication.FirstOrDefault(la => la.Number == ld.Number).LoanApplicationSource.FirstOrDefault(las => las.Index == 2).Name ?? "Other",
                                       //Application = _context.LoanApplication.AsNoTracking().FirstOrDefault(la => la.Number == ld.Number),
                                       Number = ld.Number,
                                       FundingId = ld.LoanFundings.FirstOrDefault().Id
                                   })

                                  .AsNoTracking()
                                  .ToList();



                    recentLoans.ForEach(l => l.Area = getP1s.FirstOrDefault(p => l.Postcode.StartsWith(p.P1))?.Area.Trim());
                    recentLoans.ForEach(l => l.Region = getP1s.FirstOrDefault(p => l.Postcode.StartsWith(p.P1))?.Region.Trim());

                    recentLoans.Where(l => l.Area == null).ToList().ForEach(l => l.Area = "Other");
                    recentLoans.Where(l => l.Region == null).ToList().ForEach(l => l.Region = "Other");

                    foreach (var l in recentLoans)
                    {
                        var s1 = this.LoanApplications.FirstOrDefault(la => la.Number == l.Number)?.Source;
                        if (!String.IsNullOrEmpty(s1)) l.Source = s1;

                        var s2 = this.LoanApplications.FirstOrDefault(la => la.Number == l.Number)?.Source2;
                        if (!String.IsNullOrEmpty(s2)) l.Source2 = s2;

                        //var s1 = _context.LoanApplication.AsNoTracking().FirstOrDefault(la => la.Number == l.Number);

                        //if (s1 != null)
                        //{
                        //    var ss1 = _context.LoanApplicationSource.AsNoTracking().FirstOrDefault(las => las.LoanApplicationId == s1.Id && las.Index == 1);
                        //    if (ss1 != null && ss1.Name != null)
                        //        l.Source = ss1.Name;

                        //    var ss2 = _context.LoanApplicationSource.AsNoTracking().FirstOrDefault(las => las.LoanApplicationId == s1.Id && las.Index == 2);
                        //    if (ss2 != null && ss2.Name != null)
                        //        l.Source2 = ss2.Name;
                        //}
                    }
                    //recentLoans.ForEach(l => l.Source = (_context.LoanApplication.FirstOrDefault(la => la.Number == l.Number)?.LoanApplicationSource.FirstOrDefault(las => las.Index == 1).Name) ?? "Other");
                    //recentLoans.ForEach(l => l.Source2 = (_context.LoanApplication.FirstOrDefault(la => la.Number == l.Number)?.LoanApplicationSource.FirstOrDefault(las => las.Index == 2).Name) ?? "Other");
                }

                LoansOut = Loans.RemoveAll(l => l.StartTime < DateTime.Now.AddDays(-8));

                recentLoans.RemoveAll(l => Loans.Select(lx => lx.Number).Contains(l.Number));

                if (recentLoans.Any())
                {
                    LoansIn = recentLoans.Count;
                    Loans.AddRange(recentLoans);
                    lastFundedId = Loans.Max(l => l.FundingId);

                    NewLoansAdded?.Invoke(this, EventArgs.Empty);
                }
                else
                {
                    LoansIn = 0;
                }

                //MyObjects.Cache.AddObject(new EventLogItem(EventLevelType.Information, MethodBase.GetCurrentMethod(), String.Format("{0} - {1}", LoansIn, LoansOut)));

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                MyObjects.Cache.AddObject(new EventLogItem(EventLevelType.Error, MethodBase.GetCurrentMethod(), ex.Message));

            }



        }
        private void RefreshLoanApplications(DateTime startDate)
        {
            List<LoanApplication> recentLoanApllications = new List<LoanApplication>();
            DateTime now = DateTime.Now;
            DateTime nowTime = now.AddSeconds(-now.Second).AddMilliseconds(-now.Millisecond);
            try
            {

                using (var _context = new LeadDbContext())
                {

                    //var query = from la in _context.LoanApplication

                    //            where (la.CreatedAt > startDate
                    //                && la.CreatedAt < nowTime)

                    //            select new LoanApplication
                    //            {
                    //                Id = la.Id,
                    //                Number = la.Number,
                    //                CreatedAt = la.CreatedAt,
                    //                Source = la.LoanApplicationSource.FirstOrDefault(s => s.Index == 1).Name,
                    //                Source2 = la.LoanApplicationSource.FirstOrDefault(s => s.Index == 2).Name
                    //            };

                    var query = _context.LoanApplication
                                .Include(la => la.LoanApplicationSource)
                                .Where(la => la.CreatedAt > startDate && la.CreatedAt < nowTime);


                    var retry = true;
                    while (retry)
                    {
                        try
                        {
                            var recentLoanApllicationsQuery = query.AsNoTracking().ToList();
                            foreach (var la in recentLoanApllicationsQuery)
                            {
                                recentLoanApllications.Add(
                                    new LoanApplication
                                    {
                                        Id = la.Id,
                                        Number = la.Number,
                                        CreatedAt = la.CreatedAt,
                                        Source = la.LoanApplicationSource.FirstOrDefault(s => s.Index == 1 && !String.IsNullOrEmpty(s.Name))?.Name ?? "Loans 2 Go",
                                        Source2 = la.LoanApplicationSource.FirstOrDefault(s => s.Index == 2)?.Name
                                    });
                            }
                            retry = false;
                        }
                        catch (Exception ex)
                        {
                            MyObjects.Cache.AddObject(new EventLogItem(EventLevelType.Error, MethodBase.GetCurrentMethod(), ex.Message));
                            Thread.Sleep(3000);
                        }
                    }


                }
                lock (_lock)
                {
                    foreach (var la in recentLoanApllications)
                    {
                        if (la.Source.Contains("_"))
                        {
                            la.Source2 = la.Source.Substring(la.Source.IndexOf("_") + 1);
                            la.Source = la.Source.Substring(0, la.Source.IndexOf("_"));
                        }
                    }
                    LoanApplicationsOut = LoanApplications.RemoveAll(l => l.CreatedAt < DateTime.Now.AddDays(-8));

                    if (recentLoanApllications.Any())
                    {
                        loanApplicationslastUpdateTime = recentLoanApllications.Max(l => l.CreatedAt);
                        LoanApplicationsIn = recentLoanApllications.Count();
                        LoanApplications.AddRange(recentLoanApllications);

                        NewLoanApplicationsAdded?.Invoke(this, EventArgs.Empty);
                    }
                    else
                    {
                        LoanApplicationsIn = 0;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MyObjects.Cache.AddObject(new EventLogItem(EventLevelType.Error, MethodBase.GetCurrentMethod(), ex.Message));
            }

        }

        public List<Loan> Loans { get; private set; }
        public List<LoanChartViewModel> LoanAvgPerHour { get; private set; }
        public int LoansIn { get; private set; }
        public bool HasNewLoans
        {
            get { return LoansIn > 0; }
        }
        public int LoansOut { get; private set; }

        public List<LoanApplication> LoanApplications { get; private set; }
        public int LoanApplicationsIn { get; private set; }
        public int LoanApplicationsOut { get; private set; }

        public void Dispose()
        {
            timer.Stop();
            timer = null;
        }

        private readonly object _lock = new object();
        public List<LoanApplication> LoanApllicationsDataClone
        {
            get
            {
                List<LoanApplication> newList = new List<LoanApplication>();

                lock (_lock)
                {
                    LoanApplications.ForEach(x => newList.Add(x));
                }
                return newList;
            }
        }

    }
    public class Loan
    {
        public string Number { get; set; }

        public DateTime StartTime { get; set; }
        public decimal Size { get; set; }
        public string Postcode { get; set; }
        public string Area { get; set; }
        public string Region { get; set; }
        public string Source { get; set; }
        public string Source2 { get; set; }

        public Data.LoanApplication Application { get; set; }
        public long FundingId { get; internal set; }
    }
    [Serializable]
    public class LoanApplication
    {
        public long Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Source { get; set; }
        public string Source2 { get; internal set; }
        public string Number { get; internal set; }
    }

}


