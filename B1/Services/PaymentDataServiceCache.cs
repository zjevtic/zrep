using B1.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using B1.Data.RePay;
using System.Reflection;
using zCache;

namespace B1.Data.Services

{
    public class PaymentDataServiceCache
    {

        private DateTime lastUpdateTime;
        private System.Timers.Timer timer;

        public PaymentDataServiceCache()
        {


            Payments = new List<Payment>();

            lastUpdateTime = DateTime.Now.AddHours(-25);


            RefreshPayments(lastUpdateTime);


            timer = new System.Timers.Timer();
            timer.Elapsed += (s, ev) =>
            {
                if (DateTime.Now.Second == 45) RefreshPayments(lastUpdateTime);
            };
            timer.Interval = 1000;
            timer.Start();

        }


        private void RefreshPayments(DateTime startDate)
        {
            List<Payment> recentPayments;

            try
            {
                using (var _context = new RePayContext())
                {
                    recentPayments = _context.Payment
                                    .Where(p => p.Date > startDate && p.Status == 1 && p.Amount > 0.1m)
                                    .AsNoTracking()
                                    .ToList();
                }

                PaymentsOut = Payments.RemoveAll(l => l.Date < DateTime.Now.AddHours(-25));

                if (recentPayments.Any())
                {
                    lastUpdateTime = recentPayments.Max(l => l.Date);
                    PaymentsIn = recentPayments.Count;
                    Payments.AddRange(recentPayments);
                }
                else
                {
                    PaymentsIn = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                MyObjects.Cache.AddObject(new EventLogItem(EventLevelType.Error, MethodBase.GetCurrentMethod(), ex.Message));
            }


        }
        public List<Payment> Payments { get; }
        public int PaymentsIn { get; private set; }

        public int PaymentsOut { get; private set; }

        public void Dispose()
        {
            timer.Stop();
            timer = null;
        }

    }

}


