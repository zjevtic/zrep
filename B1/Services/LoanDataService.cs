using B1.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using zCache;
using B1.Data.OLA;
using System.Reflection;
using System.Collections.ObjectModel;

namespace B1.Data.Services

{
    public class LoanDataService
    {
        private LeadDbContext _context;


        public IEnumerable<LoanViewModel> Loans;

        public List<(int, int)> Intervals;
        public List<int> Resolutions;
        public List<LoanApplicationStateType> LoanApplicationStates;
        //public enum LoanApplicationStateType
        //{
        //    Rejected = 0,
        //    Referred = 1,
        //    Accepted = 2,
        //    Reviewed = 5,
        //    Active = 9,
        //    Invalid = 88,
        //    NonPurchased = 95,
        //    Pending = 99
        //}

        public LoanDataService(LeadDbContext context)
        {
            _context = context;

            Intervals = new List<(int, int)>
            {
                (1, 1),
                (2, 1),
                (4, 2),
                (8, 5),
                (12, 10),
                (1 * 24, 15),
                (3 * 24, 30),
                (7 * 24, 60),

            };

            Resolutions = new List<int> { 1, 2, 5, 10, 15, 30, 60 };

            LoanApplicationStates = Enum.GetValues(typeof(LoanApplicationStateType)).Cast<LoanApplicationStateType>().ToList();

        }

        public List<LoanType> LoanTypes = new List<LoanType>() {
            new LoanType(){Id=0, Name="LBL"},
            new LoanType(){Id=1, Name="HP"},
            new LoanType(){Id=2, Name="SPL"}
        };

        public List<LoanState> LoanStates = new List<LoanState>() {
            new LoanState(){Id=2, Name="Active"},
            new LoanState(){Id=11, Name="PreRecoveries"},
            new LoanState(){Id=12, Name="PreLitigation"},
            new LoanState(){Id=21, Name="Recoveries"},
            new LoanState(){Id=22, Name="Auction"},
            new LoanState(){Id=31, Name="PreOutsource"},
            new LoanState(){Id=32, Name="Outsource"},
            new LoanState(){Id=33, Name="PreSale"},
            new LoanState(){Id=41, Name="Litigation"},
            new LoanState(){Id=50, Name="Completed"},
            new LoanState(){Id=51, Name="Withdrawn"},
            new LoanState(){Id=53, Name="DebtSold"}
        };


        //Loans activated
        public async Task<IEnumerable<Loan>> GetLoans(DateTime startDate, DateTime endDate)
        {
            List<Loan> recentLoans = null;
            try
            {
                var getP1s = MyObjects.Cache.GetObjects<Location>(s => true,
                    () =>
                    {

                        using (var _context = new OLA2Context())
                        {
                            var dbquery = (from pc in _context.Postcode
                                           group pc by pc.P1 into g
                                           select new Location()
                                           {
                                               P1 = g.Key,
                                               Area = g.Max(p => p.Area),
                                               Region = g.Max(p => p.Region)

                                           }).OrderBy(x => x.P1).ToList();

                            MyObjects.Cache.AddObjects(dbquery);
                        }
                    });

                var query = (from ld in _context.LoanDetails

                             where ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange) >= startDate
                                && ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange) < endDate

                             select new Loan
                             {
                                 StartTime = ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange),
                                 Size = ld.AmountLoaned ?? 0m,
                                 Postcode = ld.Car.Customer.PostalCode.ToUpper().RemoveWhitespace(),
                                 Source = _context.LoanApplication.FirstOrDefault(la => la.Number == ld.Number).LoanApplicationSource.FirstOrDefault(las => las.Index == 1 && !String.IsNullOrEmpty(las.Name)).Name ?? "Loans 2 Go",
                                 Source2 = _context.LoanApplication.FirstOrDefault(la => la.Number == ld.Number).LoanApplicationSource.FirstOrDefault(las => las.Index == 2).Name ?? "Other"
                             }
                             ).AsNoTracking();

                recentLoans = await query.ToListAsync();


                foreach (Loan l in recentLoans)
                {
                    var p = getP1s.FirstOrDefault(p => l.Postcode.StartsWith(p.P1));

                    l.Area = p != null ? p.Area.Trim() : "Other";
                    l.Region = p != null ? p.Region.Trim() : "Other";
                }
                //recentLoans.ForEach(l => l.Area = getP1s.FirstOrDefault(p => l.Postcode.StartsWith(p.P1))?.Area.Trim());
                //recentLoans.ForEach(l => l.Region = getP1s.FirstOrDefault(p => l.Postcode.StartsWith(p.P1))?.Region.Trim());

                //recentLoans.Where(l => l.Area == null).ToList().ForEach(l => l.Area = "Other");
                //recentLoans.Where(l => l.Region == null).ToList().ForEach(l => l.Region = "Other");


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return recentLoans;
        }

        public async Task<IEnumerable<LoanViewModel>> GetLoans(DateTime startDate)
        {
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
            var loans = from ld in _context.LoanDetails
                        join lsc in (
                                    from lx in _context.LoanStateChanges
                                    where lx.NewState == "Active"
                                    group lx by lx.LoanId into g
                                    select new { LoanID = g.Key, DateOfChange = g.Min(o => o.DateOfChange) }
                                    )
                                    on ld.LoanId equals lsc.LoanID

                        where lsc.DateOfChange >= startDate// && lsc.NewState == "Active"

                        select new LoanViewModel()
                        {
                            LoanId = ld.LoanId,
                            LoanProduct = ld.LoanProduct ?? 0,
                            ProductName = ld.ProductName,
                            State = ld.State ?? 0,
                            DateActivated = lsc.DateOfChange,
                            AmountLoaned = ld.AmountLoaned ?? 0m,
                            InterestRate = ld.InterestRate ?? 0,
                            Duration = Math.Floor(ld.DurationInWeeks / (52m / 12m)),

                            FirstName = ti.ToTitleCase(ld.Car.Customer.FirstName.ToLowerInvariant()),
                            LastName = ti.ToTitleCase(ld.Car.Customer.LastName.ToLowerInvariant()),

                            Postcode = ld.Car.Customer.PostalCode,

                            Make = ld.Car.Make,
                            Model = ld.Car.Model,
                            CarYear = ld.Car.FirstRegistered,
                            Colour = ld.Car.Colour,
                            CarValue = ld.Car.TradePrice,

                            PaySum = ld.Payments.Sum(p => p.Amount),
                            PayCount = ld.Payments.Count

                        };


            //.ThenInclude(l => l.Customer)


            //return loans.AsQueryable();

            return await loans.ToListAsync();

        }
        public IEnumerable<LoanChartPerSizeViewModel> GetLoansPerLoanSize(int year)
        {
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
            List<LoanViewModel> loansTemp = _context.LoanDetails
                .Select(l => new LoanViewModel()
                {
                    LoanId = l.LoanId,
                    LoanProduct = l.LoanProduct ?? 0,
                    ProductName = l.ProductName,
                    State = l.State ?? 0,
                    DateActivated = l.DateSigned ?? DateTime.Now,
                    AmountLoaned = l.AmountLoaned ?? 0m,
                    InterestRate = l.InterestRate ?? 0,
                    Duration = Math.Floor(l.DurationInWeeks / (52m / 12m)),

                    FirstName = ti.ToTitleCase(l.Car.Customer.FirstName.ToLowerInvariant()),
                    LastName = ti.ToTitleCase(l.Car.Customer.LastName.ToLowerInvariant()),

                    Postcode = l.Car.Customer.PostalCode,

                    Make = l.Car.Make,
                    Model = l.Car.Model,
                    CarYear = l.Car.FirstRegistered,
                    Colour = l.Car.Colour,
                    CarValue = l.Car.TradePrice,

                    PaySum = l.Payments.Sum(p => p.Amount),
                    PayCount = l.Payments.Count

                }
                ).Where(l => l.DateActivated.Year == year && l.State >= 50).ToList();// && l.LastName.StartsWith("VANDER"));


            var loans = loansTemp
                .Where(l => l.LoanProduct == 2 && l.AmountLoaned <= 1000)

                .GroupBy(l => new { size = Math.Floor(l.AmountLoaned / 50m) * 50m })

                .Select(ls => new LoanChartPerSizeViewModel()
                {

                    Size = ls.Key.size,
                    LoansAmountOut = ls.Sum(las => las.AmountLoaned),
                    LoansAmountIn = ls.Sum(las => las.PaySum ?? 0m),
                    LoansCount = ls.Count()
                }).ToList().OrderBy(l => l.Size).ToList();


            //var loans = _context.LoanDetails.GroupBy(l=>l.DateSigned?.TimeStamp, (key,g) = new)
            //    .Select(l => new LoanViewModel()
            //{
            //    LoanId = l.LoanId,
            //    ProductName = l.ProductName,
            //    State = l.State ?? 0,
            //    DateSigned = l.DateSigned ?? DateTime.Now,
            //    AmountLoaned = l.AmountLoaned ?? 0m,
            //    InterestRate = l.InterestRate ?? 0,

            //    FirstName = l.Car.Customer.FirstName,
            //    LastName = l.Car.Customer.LastName,

            //    Make = l.Car.Make,
            //    Model = l.Car.Model,
            //    Colour = l.Car.Colour,

            //})


            //    .Where(l => l.DateSigned > startDate);

            ////.ThenInclude(l => l.Customer)


            return loans;

        }
        public IEnumerable<LoanChartViewModel> GetLoansPerMonth(DateTime startDate)
        {

            //var loansTemp = (from lsc in _context.LoanStateChanges

            //                 where lsc.DateOfChange >= startDate && lsc.NewState == "Active"

            //                 select new LoanViewModel()
            //                 {
            //                     LoanId = lsc.Loan.LoanId,
            //                     LoanProduct = lsc.Loan.LoanProduct ?? 0,
            //                     ProductName = lsc.Loan.ProductName,
            //                     DateActivated = lsc.DateOfChange,
            //                     AmountLoaned = lsc.Loan.AmountLoaned ?? 0m,
            //                 })
            //                 .AsNoTracking()
            //                 .ToList();

            var loansTemp = (from ld in _context.LoanDetails

                             where ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange) >= startDate

                             select new Loan
                             {
                                 StartTime = ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange),
                                 Size = ld.AmountLoaned ?? 0m,
                             })
                             .AsNoTracking()
                             .ToList();

            var loans = loansTemp

                //.Where(l => l.LoanProduct == 2 && l.AmountLoaned <= 1000)

                .GroupBy(l => new { month = new DateTime(l.StartTime.Year, l.StartTime.Month, 1) })

                .Select(ls => new LoanChartViewModel()
                {
                    TimeStamp = ls.Key.month,
                    LoansAmountOut = ls.Sum(las => las.Size),
                    LoansCount = ls.Count()
                })
                .ToList().OrderBy(l => l.TimeStamp).ToList();

            return loans;

        }
        public IEnumerable<LoanChartPerWeekViewModel> GetLoansPerWeek(DateTime startDate)
        {

            var loansTemp = (from ld in _context.LoanDetails

                             where ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange) >= startDate

                             select new Loan
                             {
                                 StartTime = ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange),
                                 Size = ld.AmountLoaned ?? 0m,
                             })
                             .AsNoTracking()
                             .ToList();


            var loans = loansTemp

                //.Where(l => l.LoanProduct == 2 && l.AmountLoaned <= 1000)

                .GroupBy(l => new { Y = l.StartTime.Year, V = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(l.StartTime, CalendarWeekRule.FirstDay, DayOfWeek.Monday) })

                .Select(ls => new LoanChartPerWeekViewModel()
                {
                    Year = ls.Key.Y,
                    Week = ls.Key.V,
                    TimeStamp = ls.Min(las => las.StartTime),
                    LoansAmountOut = ls.Sum(las => las.Size),
                    LoansCount = ls.Count()
                })
                .ToList().OrderBy(l => l.TimeStamp).ToList();

            return loans;

        }
        public IEnumerable<LoanChartViewModel> GetLoansPerDay(DateTime startDate)
        {

            var loansTemp = (from ld in _context.LoanDetails

                             where ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange) >= startDate

                             select new Loan
                             {
                                 StartTime = ld.LoanStateChanges.Where(s => s.NewState == "Active").Min(s => s.DateOfChange),
                                 Size = ld.AmountLoaned ?? 0m,
                             })
                             .AsNoTracking()
                             .ToList();

            var loans = loansTemp

                //.Where(l => l.LoanProduct == 2 && l.AmountLoaned <= 1000)

                .GroupBy(l => new { day = new DateTime(l.StartTime.Year, l.StartTime.Month, l.StartTime.Day) })

                .Select(ls => new LoanChartViewModel()
                {
                    TimeStamp = ls.Key.day,
                    LoansAmountOut = ls.Sum(las => las.Size),
                    LoansCount = ls.Count()
                })
                .ToList().OrderBy(l => l.TimeStamp).ToList();

            if (loans.Last().TimeStamp < DateTime.Today)
                loans.Add(
                            new LoanChartViewModel()
                            {
                                TimeStamp = DateTime.Today,
                                LoansAmountOut = 0m,
                                LoansCount = 0
                            });



            return loans;

        }
        public List<LoanChartPerSizeViewModel> GetLoansPerSize(DateTime startDate, IEnumerable<Loan> cache)
        {

            var loansPerSize = cache

                                .Where(l => l.StartTime >= startDate)

                                .GroupBy(l => Math.Floor(l.Size / 50m) * 50m)

                                .OrderBy(g => g.Key)

                                .Select(g => new LoanChartPerSizeViewModel()
                                {
                                    Size = g.Key,
                                    LoansCount = g.Count(),
                                    LoansAmountOut = g.Sum(o => o.Size)
                                })

                                .ToList();

            return loansPerSize;
        }

        public List<LoanChartPerSizeViewModel> GetLoansPerSource(DateTime startDate, IEnumerable<Loan> cache, DateTime? endDate = null)
        {

            if (endDate == null) endDate = DateTime.Now;
            var loansPerSize = cache

                                .Where(l => l.StartTime >= startDate && l.StartTime < endDate)

                                .GroupBy(l => l.Source.Split('_', StringSplitOptions.None).First())
                                //.GroupBy(l => l.Source)

                                //.OrderBy(g => g.Key)
                                .OrderByDescending(g => g.Sum(o => o.Size))

                                .Select(g => new LoanChartPerSizeViewModel()
                                {
                                    Source = g.Key,
                                    LoansCount = g.Count(),
                                    LoansAmountOut = g.Sum(o => o.Size)
                                })

                                .ToList();

            return loansPerSize;
        }
        public List<LoanChartPerSizeViewModel> GetLoansPerSource2(DateTime startDate, IEnumerable<Loan> cache, string source, DateTime? endDate = null)
        {
            if (endDate == null) endDate = DateTime.Now;
            var loansPerSize = cache

                                .Where(l => l.StartTime >= startDate && l.StartTime <= endDate && l.Source == source)

                                .GroupBy(l => l.Source2)

                                //.OrderBy(g => g.Key)
                                .OrderByDescending(g => g.Sum(o => o.Size))

                                .Select(g => new LoanChartPerSizeViewModel()
                                {
                                    Source = g.Key,
                                    LoansCount = g.Count(),
                                    LoansAmountOut = g.Sum(o => o.Size)
                                })

                                .ToList();

            return loansPerSize;
        }
        public List<LoanTimelineChartViewModel> GetLoansSmartIntervalPerSource(IEnumerable<Loan> cache, (int, int) options)
        {
            int scale = options.Item2;
            DateTime now = DateTime.Now;
            DateTime startTime = now.AddHours(-options.Item1).AddSeconds(-now.Second).AddMilliseconds(-now.Millisecond);
            DateTime nowTime = now.AddSeconds(-now.Second).AddMilliseconds(-now.Millisecond);

            List<LoanTimelineChartViewModel> loansSumPerSource = new List<LoanTimelineChartViewModel>();
            try
            {



                loansSumPerSource = cache
                                        .Where(la => la.StartTime >= startTime
                                                     && la.StartTime < nowTime)
                                        //&& !String.IsNullOrEmpty(la.Source))

                                        .GroupBy(la => new
                                        {
                                            Minute = new DateTime(
                                             la.StartTime.Year,
                                             la.StartTime.Month,
                                             la.StartTime.Day,
                                             la.StartTime.Hour,
                                             zRound(la.StartTime.Minute, scale),
                                             0),
                                            la.Source//.Split('_', StringSplitOptions.None).First()
                                        })

                                        //.OrderBy(g => g.Key.Minute).ThenBy(g => g.Key.Source)

                                        .Select(g =>
                                            new LoanTimelineChartViewModel
                                            {
                                                Timestamp = g.Key.Minute,
                                                Source = g.Key.Source,
                                                LoanSum = g.Sum(l => l.Size),
                                                LoanCount = g.Count()
                                            })

                                        .ToList();


                //loansSumPerSource.RemoveAll(x => x.Timestamp == loansSumPerSource.Min(x => x.Timestamp));
                //loanApllicationsCount.RemoveAll(x => x.Timestamp == loanApllicationsCount.Max(x => x.Timestamp));

                var timestamps = loansSumPerSource.Select(l => l.Timestamp).Distinct().ToList();
                var sources = loansSumPerSource.Select(l => l.Source).Distinct().ToList();
                var missingentries = new List<LoanTimelineChartViewModel>();

                foreach (string src in sources)
                {
                    var scrTimestamps = loansSumPerSource.Where(l => l.Source == src).Select(l => l.Timestamp).Distinct();
                    var missingTimestamps = timestamps.Except(scrTimestamps);
                    foreach (var mts in missingTimestamps)
                    {
                        missingentries.Add(new LoanTimelineChartViewModel() { Source = src, Timestamp = mts, LoanSum = 0, LoanCount = 0 });
                    }
                }

                loansSumPerSource.AddRange(missingentries);

                for (DateTime ts = startTime; ts <= nowTime; ts = ts.AddMinutes(scale))
                {
                    foreach (string src in sources)
                    {
                        if (!loansSumPerSource.Where(l => l.Source == src && l.Timestamp == ts).Any())
                            loansSumPerSource.Add(new LoanTimelineChartViewModel() { Source = src, Timestamp = ts, LoanSum = 0, LoanCount = 0 });

                    }


                }


                //loansSumPerSource.ForEach(l => l.MA = loansSumPerSource.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Average(l => l.LoanSum));
                //loansSumPerSource.ForEach(l => l.Total = loansSumPerSource.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Sum(l => l.LoanSum));

                loansSumPerSource = loansSumPerSource.OrderBy(g => g.Timestamp).ThenBy(g => g.Source).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return loansSumPerSource;

        }
        public List<LoanChartViewModel> GetLoansPerHour(DateTime startDate, IEnumerable<Loan> cache, IEnumerable<LoanChartViewModel> AvgHour)
        {
            var loansPerHour = cache

                                .Where(l => l.StartTime >= startDate)

                                .GroupBy(l => new DateTime(l.StartTime.Year, l.StartTime.Month, l.StartTime.Day, l.StartTime.Hour, 0, 0))

                                .OrderBy(g => g.Key)

                                .Select(g => new LoanChartViewModel()
                                {
                                    TimeStamp = g.Key,
                                    LoansCount = g.Count(),
                                    LoansAmountOut = g.Sum(o => o.Size),
                                    MA = (AvgHour.FirstOrDefault(ah => ah.TimeStamp == g.Key)?.LoansAmountOut) ?? 0m,
                                    MACount = (AvgHour.FirstOrDefault(ah => ah.TimeStamp == g.Key)?.LoansCount) ?? 0m
                                })

                                .ToList();


            //return new ObservableCollection<LoanChartViewModel>(loansPerHour);
            return loansPerHour;
        }

        public List<LoanChartPerSizeViewModel> GetLoansPerPlace(DateTime startDate, IEnumerable<Loan> cache)
        {

            var loansPerSize = cache

                                .Where(l => l.StartTime >= startDate)

                                .GroupBy(l => l.Region)

                                .OrderByDescending(g => g.Sum(o => o.Size))

                                .Select(g => new LoanChartPerSizeViewModel()
                                {
                                    Place = g.Key,
                                    LoansCount = g.Count(),
                                    LoansAmountOut = g.Sum(o => o.Size)
                                })

                                .ToList();

            return loansPerSize;
        }
        public List<LoanChartPerSizeViewModel> GetLoansPerRegion(DateTime startDate, IEnumerable<Loan> cache)
        {



            var loansPerSize = cache

                                .Where(l => l.StartTime >= startDate)

                                .GroupBy(l => l.Area)

                                .OrderByDescending(g => g.Sum(o => o.Size))

                                .Select(g => new LoanChartPerSizeViewModel()
                                {
                                    Place = g.Key,
                                    LoansCount = g.Count(),
                                    LoansAmountOut = g.Sum(o => o.Size)
                                })

                                .ToList();

            return loansPerSize;
        }

        //Loan applications

        public List<LoanStatesViewModel> GetLoanApplicationsInStates(LoanApplicationStateType State, IEnumerable<LoanStatesViewModel> cache)
        {
            var loansApplicationStates = cache

                                .Where(l => l.State == State)

                                //.GroupBy(l => (l.Source + "_").Split('_',StringSplitOptions.None).First())

                                //.OrderBy(g => g.Key)

                                //.Select(g => new LoanStatesViewModel()
                                //{
                                //    State = State,
                                //    Source = g.Key,
                                //    Count = g.Count()
                                //})

                                .ToList();


            return loansApplicationStates;
        }
        public List<LoanStatesViewModel> GetLoanApplicationsAllStates(IEnumerable<LoanStatesViewModel> cache)
        {
            var loansApplicationAllStates = cache

                                //.Where(l => l.State == State)

                                .GroupBy(l => l.State)

                                .OrderBy(g => g.Key.ToString())

                                .Select(g => new LoanStatesViewModel()
                                {
                                    State = g.Key,
                                    Source = "",
                                    Count = g.Sum(s => s.Count)
                                })

                                .ToList();

            var np = loansApplicationAllStates.First(a => a.State == LoanApplicationStateType.NonPurchased);
            np.Count = np.Count / 100;
            np.State = LoanApplicationStateType.NonPurchased;

            return loansApplicationAllStates;
        }
        public List<LoanApplicationsCountViewModel> GetLoanApplicationsPerMinute(IEnumerable<LoanApplication> cache, int minutes)
        {
            List<LoanApplicationsCountViewModel> loanApllicationsCount = new List<LoanApplicationsCountViewModel>();
            try
            {


                loanApllicationsCount = cache.Where(la => la.CreatedAt >= DateTime.Now.AddMinutes(-minutes - 60).AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond) &&
                                                          la.CreatedAt < DateTime.Now.AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond))

                                            .GroupBy(la => new DateTime(
                                                        la.CreatedAt.Year,
                                                        la.CreatedAt.Month,
                                                        la.CreatedAt.Day,
                                                        la.CreatedAt.Hour,
                                                        la.CreatedAt.Minute,
                                                        0)
                                                    )

                                             .OrderBy(g => g.Key)
                                             .Select(g =>
                                                        new LoanApplicationsCountViewModel
                                                        {
                                                            Timestamp = g.Key,
                                                            Count = g.Count()
                                                        }
                                                    )
                                             .ToList();

                loanApllicationsCount.ForEach(l => l.MA = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Average(l => l.Count));
                loanApllicationsCount.ForEach(l => l.Total = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Sum(l => l.Count));


                loanApllicationsCount.RemoveAll(s => s.Timestamp < DateTime.Now.AddMinutes(-minutes - 1));

                //loanApllicationsCount.ForEach(l => l.Total = loanApllicationsCount.Where(x => x.Timestamp >= DateTime.Today && x.Timestamp <= l.Timestamp).Sum(l => l.Count));

                //foreach( var lac in loanApllicationsCount)
                //lac.MA = loanApllicationsCount.Where(x => x.Timestamp < lac.Timestamp).TakeLast(10).Average(l => l.Count);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return loanApllicationsCount;
        }

        public List<LoanApplicationsCountViewModel> GetLoanApplicationsSmartIntervalPerSource(IEnumerable<LoanApplication> cache, (int, int) options)
        {
            var scale = options.Item2;
            List<LoanApplicationsCountViewModel> loanApllicationsCount = new List<LoanApplicationsCountViewModel>();
            try
            {



                loanApllicationsCount = cache
                                        .Where(la => la.CreatedAt >= DateTime.Now.AddMinutes(-options.Item1 * 60).AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond)
                                                     && la.CreatedAt < DateTime.Now.AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond)
                                                     && !String.IsNullOrEmpty(la.Source))

                                        .GroupBy(la => new
                                        {
                                            Minute = new DateTime(
                                             la.CreatedAt.Year,
                                             la.CreatedAt.Month,
                                             la.CreatedAt.Day,
                                             la.CreatedAt.Hour,
                                             zRound(la.CreatedAt.Minute, scale),
                                             0),
                                            Source = la.Source//.Split('_', StringSplitOptions.None).First()
                                        })

                                        //.OrderBy(g => g.Key.Minute).ThenBy(g => g.Key.Source)

                                        .Select(g =>
                                            new LoanApplicationsCountViewModel
                                            {
                                                Timestamp = g.Key.Minute,
                                                Source = g.Key.Source,
                                                Count = g.Count(),


                                            })

                                        .ToList();


                loanApllicationsCount.RemoveAll(x => x.Timestamp == loanApllicationsCount.Min(x => x.Timestamp));
                //loanApllicationsCount.RemoveAll(x => x.Timestamp == loanApllicationsCount.Max(x => x.Timestamp));

                var timestamps = loanApllicationsCount.Select(l => l.Timestamp).Distinct().ToList();
                var sources = loanApllicationsCount.Select(l => l.Source).Distinct().ToList();
                var missingentries = new List<LoanApplicationsCountViewModel>();

                foreach (string src in sources)
                {
                    var scrTimestamps = loanApllicationsCount.Where(l => l.Source == src).Select(l => l.Timestamp).Distinct();
                    var missingTimestamps = timestamps.Except(scrTimestamps);
                    foreach (var mts in missingTimestamps)
                    {
                        missingentries.Add(new LoanApplicationsCountViewModel() { Source = src, Timestamp = mts, Count = 0 });
                    }
                }

                loanApllicationsCount.AddRange(missingentries);
                loanApllicationsCount.ForEach(l => l.MA = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Average(l => l.Count));
                loanApllicationsCount.ForEach(l => l.Total = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Sum(l => l.Count));

                loanApllicationsCount = loanApllicationsCount.OrderBy(g => g.Timestamp).ThenBy(g => g.Source).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return loanApllicationsCount;

        }
        public List<LoanApplicationsCountViewModel> GetLoanApplicationsSmartIntervalPerSource2(IEnumerable<LoanApplication> cache, (int, int) options, string source)
        {
            var scale = options.Item2;
            List<LoanApplicationsCountViewModel> loanApllicationsCount = new List<LoanApplicationsCountViewModel>();
            try
            {



                loanApllicationsCount = cache
                                        .Where(la => la.CreatedAt >= DateTime.Now.AddMinutes(-options.Item1 * 60).AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond)
                                                     && la.CreatedAt < DateTime.Now.AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond)
                                                     && la.Source == source)

                                        .GroupBy(la => new
                                        {
                                            Minute = new DateTime(
                                             la.CreatedAt.Year,
                                             la.CreatedAt.Month,
                                             la.CreatedAt.Day,
                                             la.CreatedAt.Hour,
                                             zRound(la.CreatedAt.Minute, scale),
                                             0),
                                            Source = la.Source2
                                        })

                                        //.OrderBy(g => g.Key.Minute).ThenBy(g => g.Key.Source)

                                        .Select(g =>
                                            new LoanApplicationsCountViewModel
                                            {
                                                Timestamp = g.Key.Minute,
                                                Source = g.Key.Source,
                                                Count = g.Count(),


                                            })

                                        .ToList();


                loanApllicationsCount.RemoveAll(x => x.Timestamp == loanApllicationsCount.Min(x => x.Timestamp));
                //loanApllicationsCount.RemoveAll(x => x.Timestamp == loanApllicationsCount.Max(x => x.Timestamp));

                var timestamps = loanApllicationsCount.Select(l => l.Timestamp).Distinct().ToList();
                var sources = loanApllicationsCount.Select(l => l.Source).Distinct().ToList();
                var missingentries = new List<LoanApplicationsCountViewModel>();

                foreach (string src in sources)
                {
                    var scrTimestamps = loanApllicationsCount.Where(l => l.Source == src).Select(l => l.Timestamp).Distinct();
                    var missingTimestamps = timestamps.Except(scrTimestamps);
                    foreach (var mts in missingTimestamps)
                    {
                        missingentries.Add(new LoanApplicationsCountViewModel() { Source = src, Timestamp = mts, Count = 0 });
                    }
                }

                loanApllicationsCount.AddRange(missingentries);
                loanApllicationsCount.ForEach(l => l.MA = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Average(l => l.Count));
                loanApllicationsCount.ForEach(l => l.Total = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Sum(l => l.Count));

                loanApllicationsCount = loanApllicationsCount.OrderBy(g => g.Timestamp).ThenBy(g => g.Source).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return loanApllicationsCount;

        }
        public List<LoanApplicationsCountViewModel> GetLoanApplicationsPerMinutePerSource(IEnumerable<LoanApplication> cache, int minutes)
        {
            List<LoanApplicationsCountViewModel> loanApllicationsCount = new List<LoanApplicationsCountViewModel>();
            try
            {
                loanApllicationsCount = cache
                                        .Where(la => la.CreatedAt >= DateTime.Now.AddMinutes(-minutes).AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond)
                                                     && la.CreatedAt < DateTime.Now.AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond)
                                                     && !String.IsNullOrEmpty(la.Source))

                                        .GroupBy(la => new
                                        {
                                            Minute = new DateTime(
                                             la.CreatedAt.Year,
                                             la.CreatedAt.Month,
                                             la.CreatedAt.Day,
                                             la.CreatedAt.Hour,
                                             la.CreatedAt.Minute,
                                             0),
                                            Source = la.Source.Split('_', StringSplitOptions.None).First()
                                        })

                                        //.OrderBy(g => g.Key.Minute).ThenBy(g => g.Key.Source)

                                        .Select(g =>
                                            new LoanApplicationsCountViewModel
                                            {
                                                Timestamp = g.Key.Minute,
                                                Source = g.Key.Source,
                                                Count = g.Count(),


                                            })

                                        .ToList();


                var timestamps = loanApllicationsCount.Select(l => l.Timestamp).Distinct().ToList();
                var sources = loanApllicationsCount.Select(l => l.Source).Distinct().ToList();
                var missingentries = new List<LoanApplicationsCountViewModel>();

                foreach (string src in sources)
                {
                    var scrTimestamps = loanApllicationsCount.Where(l => l.Source == src).Select(l => l.Timestamp).Distinct();
                    var missingTimestamps = timestamps.Except(scrTimestamps);
                    foreach (var mts in missingTimestamps)
                    {
                        missingentries.Add(new LoanApplicationsCountViewModel() { Source = src, Timestamp = mts, Count = 0 });
                    }
                }

                loanApllicationsCount.AddRange(missingentries);
                loanApllicationsCount.ForEach(l => l.MA = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Average(l => l.Count));
                loanApllicationsCount.ForEach(l => l.Total = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Sum(l => l.Count));

                loanApllicationsCount = loanApllicationsCount.OrderBy(g => g.Timestamp).ThenBy(g => g.Source).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return loanApllicationsCount;
        }
        public void UpdateLoanApplicationsPerMinutePerSource(IEnumerable<LoanApplication> cache, int minutes, ref List<LoanApplicationsCountViewModel> currentData)
        {

            DateTime minTime = DateTime.Now.AddMinutes(-minutes).AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond);

            try
            {
                if (currentData.Any())
                    minTime = currentData.Max(l => l.Timestamp);

                var loanApllicationsCount = cache
                                        .Where(la => la.CreatedAt > minTime
                                                     && la.CreatedAt < DateTime.Now.AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond)
                                                     && !String.IsNullOrEmpty(la.Source))

                                        .GroupBy(la => new
                                        {
                                            Minute = new DateTime(
                                             la.CreatedAt.Year,
                                             la.CreatedAt.Month,
                                             la.CreatedAt.Day,
                                             la.CreatedAt.Hour,
                                             la.CreatedAt.Minute,
                                             0),
                                            Source = la.Source.Split('_', StringSplitOptions.None).First()
                                        })

                                        //.OrderBy(g => g.Key.Minute).ThenBy(g => g.Key.Source)

                                        .Select(g =>
                                            new LoanApplicationsCountViewModel
                                            {
                                                Timestamp = g.Key.Minute,
                                                Source = g.Key.Source,
                                                Count = g.Count(),


                                            })

                                        .ToList();


                var timestamps = loanApllicationsCount.Select(l => l.Timestamp).Distinct().ToList();
                var sources = loanApllicationsCount.Select(l => l.Source).Distinct().ToList();
                var missingentries = new List<LoanApplicationsCountViewModel>();

                foreach (string src in sources)
                {
                    var scrTimestamps = loanApllicationsCount.Where(l => l.Source == src).Select(l => l.Timestamp).Distinct();
                    var missingTimestamps = timestamps.Except(scrTimestamps);
                    foreach (var mts in missingTimestamps)
                    {
                        missingentries.Add(new LoanApplicationsCountViewModel() { Source = src, Timestamp = mts, Count = 0 });
                    }
                }

                loanApllicationsCount.AddRange(missingentries);
                loanApllicationsCount.ForEach(l => l.MA = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Average(l => l.Count));
                loanApllicationsCount.ForEach(l => l.Total = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(60).Sum(l => l.Count));

                loanApllicationsCount = loanApllicationsCount.OrderBy(g => g.Timestamp).ThenBy(g => g.Source).ToList();

                currentData.RemoveAll(l => l.Timestamp < DateTime.Now.AddMinutes(-minutes).AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond));
                currentData.AddRange(loanApllicationsCount);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public List<LoanApplicationsCountViewModel> GetLoanApplicationsPerHour(IEnumerable<LoanApplication> cache, int days)
        {
            List<LoanApplicationsCountViewModel> loanApllicationsCount = new List<LoanApplicationsCountViewModel>();
            try
            {
                loanApllicationsCount = cache
                                        .Where(la => la.CreatedAt >= DateTime.Now
                                            .AddHours(-days * 24 - 24)
                                            .AddMinutes(-DateTime.Now.Minute)
                                            .AddSeconds(-DateTime.Now.Second)
                                            .AddMilliseconds(-DateTime.Now.Millisecond))

                                        .GroupBy(la => new DateTime(
                                            la.CreatedAt.Year,
                                            la.CreatedAt.Month,
                                            la.CreatedAt.Day,
                                            la.CreatedAt.Hour,
                                            0,
                                            0))

                                        .OrderBy(g => g.Key)

                                        .Select(g =>
                                            new LoanApplicationsCountViewModel
                                            {
                                                Timestamp = g.Key,
                                                Count = g.Count()

                                            })
                                        .ToList();

                loanApllicationsCount.ForEach(l => l.MA = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(12).Average(l => l.Count));
                loanApllicationsCount.ForEach(l => l.Total = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(12).Sum(l => l.Count));


                loanApllicationsCount.RemoveAll(s => s.Timestamp < DateTime.Now.AddDays(-days));


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return loanApllicationsCount;
        }
        public List<LoanApplicationsCountViewModel> GetLoanApplicationsPerHourPerSource(IEnumerable<LoanApplication> cache, int days)
        {
            List<LoanApplicationsCountViewModel> loanApllicationsCount = new List<LoanApplicationsCountViewModel>();
            try
            {
                loanApllicationsCount = cache
                                        .Where(la => la.CreatedAt >= DateTime.Now
                                            .AddHours(-days * 24)
                                            .AddMinutes(-DateTime.Now.Minute)
                                            .AddSeconds(-DateTime.Now.Second)
                                            .AddMilliseconds(-DateTime.Now.Millisecond)
                                            && !String.IsNullOrEmpty(la.Source))

                                        .GroupBy(la => new
                                        {
                                            Hour = new DateTime(
                                             la.CreatedAt.Year,
                                             la.CreatedAt.Month,
                                             la.CreatedAt.Day,
                                             la.CreatedAt.Hour,
                                             0,
                                             0),
                                            Source = la.Source.Split('_', StringSplitOptions.None).First()
                                        })

                                        //.OrderBy(g => g.Key.Hour).ThenBy(g => g.Key.Source)

                                        .Select(g =>
                                            new LoanApplicationsCountViewModel
                                            {
                                                Timestamp = g.Key.Hour,
                                                Source = g.Key.Source,
                                                Count = g.Count(),


                                            })

                                        .ToList();

                var timestamps = loanApllicationsCount.Select(l => l.Timestamp).Distinct().ToList();
                var sources = loanApllicationsCount.Select(l => l.Source).Distinct().ToList();
                var missingentries = new List<LoanApplicationsCountViewModel>();

                foreach (string src in sources)
                {
                    var scrTimestamps = loanApllicationsCount.Where(l => l.Source == src).Select(l => l.Timestamp).Distinct();
                    var missingTimestamps = timestamps.Except(scrTimestamps);
                    foreach (var mts in missingTimestamps)
                    {
                        missingentries.Add(new LoanApplicationsCountViewModel() { Source = src, Timestamp = mts, Count = 0 });
                    }
                }

                loanApllicationsCount.AddRange(missingentries);
                loanApllicationsCount.ForEach(l => l.MA = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(12).Average(l => l.Count));
                loanApllicationsCount.ForEach(l => l.Total = loanApllicationsCount.Where(x => x.Timestamp <= l.Timestamp).TakeLast(12).Sum(l => l.Count));

                loanApllicationsCount = loanApllicationsCount.OrderBy(g => g.Timestamp).ThenBy(g => g.Source).ToList();




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return loanApllicationsCount;
        }
        public List<LoanStatesViewModel> GetLoanApplicationsPerSource(IEnumerable<LoanApplication> cache, DateTime startTime)
        {
            List<LoanStatesViewModel> loanApllicationsSourceCount = new List<LoanStatesViewModel>();
            try
            {


                loanApllicationsSourceCount = cache

                                            .Where(la => la.CreatedAt > startTime &&
                                                   la.CreatedAt < DateTime.Now.AddSeconds(-DateTime.Now.Second).AddMilliseconds(-DateTime.Now.Millisecond))


                                            .GroupBy(l => l.Source.Split('_', StringSplitOptions.None).First())

                                            //.OrderBy(g => g.Key)

                                            .Select(g => new LoanStatesViewModel()
                                            {
                                                State = 0,
                                                Source = g.Key,
                                                Count = g.Count(),
                                                CountPct = 0m
                                            })
                                            .OrderByDescending(a => a.Count)
                                            .ToList();

                foreach (var item in loanApllicationsSourceCount)
                    item.CountPct = Math.Round(Convert.ToDecimal(item.Count) / Convert.ToDecimal(loanApllicationsSourceCount.Sum(x => x.Count)) * 100m, 1);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return loanApllicationsSourceCount;
        }


        public async Task<List<LoanApplicationsCountViewModel>> GetLoanApplicationsSuccessRate(DateTime startDate, int state)
        {
            List<LoanApplicationsCountViewModel> loanApllicationsCount = new List<LoanApplicationsCountViewModel>();
            try
            {
                loanApllicationsCount = await MyObjects.Cache.GetObjectsAsync<LoanApplicationsCountViewModel>(s => s.State == state,
                    async () =>
                    {
                        try
                        {
                            var dbquery = await (from la in _context.LoanApplication
                                                 join las in _context.LoanApplicationSource on la.Id equals las.LoanApplicationId

                                                 where la.CreatedAt >= startDate && la.Dob != null && las.Index == 1
                                                 //group la by new { date = la.CreatedAt.Date, source = las.Name } into g
                                                 group la by new
                                                 {
                                                     date = new DateTime(
                                                                         la.CreatedAt.Year,
                                                                         la.CreatedAt.Month,
                                                                         la.CreatedAt.Day,
                                                                         0,
                                                                         0,
                                                                         0),
                                                     source = las.Name
                                                 } into g



                                                 select new
                                                 {
                                                     Date = g.Key.date,
                                                     Source = g.Key.source,
                                                     State = state,
                                                     LoanAppState = g.Sum(l => l.State == state ? 1 : 0),
                                                     TotalApps = g.Count()
                                                 })
                                            .AsNoTracking()
                                            .ToListAsync();

                            var loanAppsinState = dbquery
                                        .GroupBy(la => new { date = la.Date, source = la.Source.Split('_', StringSplitOptions.None).First() })

                                        .Where(g => g.Sum(l => l.LoanAppState) > 0 && g.Sum(l => l.TotalApps) > 0)

                                        .Select(g => new LoanApplicationsCountViewModel
                                        {
                                            Timestamp = g.Key.date,
                                            Source = String.IsNullOrEmpty(g.Key.source) ? "Unknown" : g.Key.source,
                                            State = state,
                                            StateRate = Convert.ToDecimal(g.Sum(l => l.LoanAppState)) / Convert.ToDecimal(g.Sum(l => l.TotalApps)) * 100m,
                                            Count = g.Sum(l => l.LoanAppState)
                                        })
                                        .OrderBy(s => s.Timestamp)
                                        .ToList();


                            var timestamps = loanAppsinState.Select(l => l.Timestamp).Distinct().ToList();
                            var sources = loanAppsinState.Select(l => l.Source).Distinct().ToList();
                            var missingentries = new List<LoanApplicationsCountViewModel>();

                            foreach (string src in sources)
                            {
                                var scrTimestamps = loanAppsinState.Where(l => l.Source == src).Select(l => l.Timestamp).Distinct();
                                var missingTimestamps = timestamps.Except(scrTimestamps);
                                foreach (var mts in missingTimestamps)
                                {
                                    missingentries.Add(new LoanApplicationsCountViewModel() { Source = src, Timestamp = mts, Count = 1, StateRate = 0, State = state });
                                }
                            }

                            loanAppsinState.AddRange(missingentries);

                            loanAppsinState = loanAppsinState.OrderBy(g => g.Timestamp).ThenBy(g => g.Source).ToList();

                            MyObjects.Cache.AddObjects(loanAppsinState, DateTime.Now.AddMinutes(5));
                        }
                        catch (Exception ex)
                        {
                            MyObjects.Cache.AddObject(new EventLogItem(EventLevelType.Error, MethodBase.GetCurrentMethod(), ex.Message));
                        }
                    });

            }
            catch (Exception ex)
            {
                MyObjects.Cache.AddObject(new EventLogItem(EventLevelType.Error, MethodBase.GetCurrentMethod(), ex.Message));
            }

            return loanApllicationsCount;
        }

        public static int zRound(int x, int y)
        {
            var r = Convert.ToInt32(Math.Floor(Convert.ToDouble(x) / y) * y);
            return r;
        }

        public static object DeepClone(object obj)
        {
            object objResult = null;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);

                ms.Position = 0;
                objResult = bf.Deserialize(ms);
            }
            return objResult;
        }


    }


}
