﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace B1.Data.Services
{
    public static class Extensions
    {
        public static string RemoveWhitespace(this string str)
        {
            return string.Join("", str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
        }

        public static string Left(this string str, int length, string ellipsis = ".. ")
        {
            if (str.Length <= length)
                return str.Trim();
            else
                return str.Substring(0, length).Trim() + ellipsis;
        }

    }
}
