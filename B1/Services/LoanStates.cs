﻿using System;

namespace B1.Data.ViewModels
{

    public enum LoanApplicationStateType
    {
        Rejected = 0,
        Referred = 1,
        Accepted = 2,
        Reviewed = 5,
        Active = 9,
        Invalid = 88,
        NonPurchased = 95,
        Pending = 99,
        //NonPurchasedx100 = 950,
    }

    public class LoanApplicationsCountViewModel
    {
        public string Source;
        
        public int State;
        public DateTime Timestamp { get; set; }
        public int Count { get; set; }
        public double MA { get; set; }
        public int Total { get; internal set; }

        public decimal StateRate { get; set; }
    }

    public class LoanStatesViewModel
    {
        public LoanApplicationStateType State { get; set; }
        public string Source { get; set; }
        public int Count { get; set; }

        public decimal CountPct { get; set; }

    }


}