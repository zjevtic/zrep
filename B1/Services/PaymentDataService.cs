using B1.Data.ViewModels;
using B1.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using B1.Data.RePay;

namespace B1.Data.Services

{
    public class PaymentDataService
    {
        private LeadDbContext _context;

        public IEnumerable<LoanViewModel> Loans;


        public PaymentDataService(LeadDbContext context)
        {
            _context = context;

        }

        public List<PaymentAgentViewModel> GetPaymentsPerAgent(IEnumerable<Payment> cache, DateTime startTime)
        {
            var paymentsPerAgent = cache

                                .Where(p => p.Date > startTime && p.User != "system")

                                .GroupBy(p => p.User)

                                //.OrderBy(g => g.Key)

                                .Select(g => new PaymentAgentViewModel()
                                {
                                    TimeStamp = DateTime.Today,
                                    Agent = g.Key.ToLower(),
                                    Count = g.Count(),
                                    Sum = g.Sum(p => p.Amount)
                                })
                                .OrderByDescending(a => a.Sum)
                                .ToList();


            return paymentsPerAgent;
        }
        
        public List<PaymentAgentViewModel> GetPaymentsForSystemPerHour(IEnumerable<Payment> cache, DateTime startTime)
        {
            var paymentsforSystemPerHour = cache

                                .Where(p => p.Date > startTime && p.User == "system")

                                .GroupBy(p => new DateTime(p.Date.Year, p.Date.Month, p.Date.Day, p.Date.Hour, 0, 0))

                                .Select(g => new PaymentAgentViewModel()
                                {
                                    TimeStamp = g.Key,
                                    Agent = "system",
                                    Count = g.Count(),
                                    Sum = g.Sum(p => p.Amount)

                                }).ToList();

            return paymentsforSystemPerHour;
        }
        public List<PaymentAgentViewModel> GetPaymentsPerAgentPerHour(IEnumerable<Payment> cache, DateTime startTime)
        {
            var paymentsPerAgent = cache

                                .Where(p => p.Date > startTime && p.User != "system")

                                .GroupBy(p => new { agent = p.User, hour = new DateTime(p.Date.Year, p.Date.Month, p.Date.Day, p.Date.Hour, 0, 0) })

                                //.OrderBy(g => g.Key)

                                .Select(g => new PaymentAgentViewModel()
                                {
                                    TimeStamp = g.Key.hour,
                                    Agent = g.Key.agent.ToLower(),
                                    Count = g.Count(),
                                    Sum = g.Sum(p => p.Amount)
                                })
                                .OrderByDescending(a => a.TimeStamp)
                                
                                .ToList();


            return paymentsPerAgent;
        }


        public IEnumerable<LoanChartViewModel> GetPaymentsPerMonth(DateTime startDate)
        {
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
            var paymentsTemp = _context.Payments
                .Where(p => p.PaymentDate >= new DateTime(startDate.Year, startDate.Month, 1))
                .GroupBy(p => new DateTime((p.PaymentDate ?? DateTime.Now).Year, (p.PaymentDate ?? DateTime.Now).Month, 1))

                .Select(p => new LoanChartViewModel()
                {
                    TimeStamp = p.Key,
                    LoansAmountIn = p.Sum(py => py.Amount ?? 0m)
                })

                .AsNoTracking()
                .AsEnumerable()

                .OrderBy(p => p.TimeStamp)
                .ToList();

            return paymentsTemp;
        }
        public IEnumerable<LoanChartViewModel> GetPaymentsPerDay(DateTime startDate)
        {
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
            var paymentsTemp = _context.Payments
                .Where(p => p.PaymentDate > new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0))
                .GroupBy(p => new DateTime((p.PaymentDate ?? DateTime.Now).Year, (p.PaymentDate ?? DateTime.Now).Month, (p.PaymentDate ?? DateTime.Now).Day))

                .Select(p => new LoanChartViewModel()
                {
                    TimeStamp = p.Key,
                    LoansAmountIn = p.Sum(py => py.Amount ?? 0m)
                })

                .AsNoTracking()
                .AsEnumerable()

                .OrderBy(p => p.TimeStamp)
                .ToList();

            return paymentsTemp;
        }
        public IEnumerable<LoanChartViewModel> GetPaymentsPerHour(DateTime startDate)
        {
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
            var paymentsTemp = _context.Payments
                .Where(p => p.PaymentDate > startDate)
                .GroupBy(p => new DateTime((p.PaymentDate ?? DateTime.Now).Year, (p.PaymentDate ?? DateTime.Now).Month, (p.PaymentDate ?? DateTime.Now).Day, (p.PaymentDate ?? DateTime.Now).Hour, 0, 0))

                .Select(p => new LoanChartViewModel()
                {
                    TimeStamp = p.Key,
                    LoansAmountIn = p.Sum(py => py.Amount ?? 0m)
                })

                .AsNoTracking()
                .AsEnumerable()

                .OrderBy(p => p.TimeStamp)
                .ToList();

            return paymentsTemp;
        }


    }
}
