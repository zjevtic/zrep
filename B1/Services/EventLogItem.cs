﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace B1.Data.Services
{
    public class EventLogItem
    {
        public EventLogItem() { }


        public EventLogItem(EventLevelType level, MethodBase source, string message)
        {
            this.Timestamp = DateTime.Now;
            this.Level = level;
            this.Source = source.Name + " of " + source.ReflectedType.Name;
            this.Message = message;
        }
        public EventLogItem(EventLevelType level, string source, string message)
        {
            this.Timestamp = DateTime.Now;
            this.Level = level;
            this.Source = source;
            this.Message = message;
        }

        public DateTime Timestamp { get; set; }
        
        public EventLevelType Level { get; set; }

        public string Source { get; set; }

        public string Message { get; set; }
    }

    public enum EventLevelType
    { 
        Information,
        Warning, 
        Error,
        Request
    }
}
